! ****************************************************************************************
!   Parallel I/O subroutines

module netcdf_stuff
contains

    subroutine check(status,message)
    use netcdf
    use mpi
        integer, intent (in) :: status
        character(len=*) message
        if (status .NE. NF90_NOERR) then
            write(*,*) "Aborting: ",trim(message), trim(nf90_strerror(status))
        end if
    end subroutine check

    !> Output 3D array with hdf5
    !> Two-phase level set input and output subroutines.
    !!
    !! @author Prashant Valluri, Lennon O Naraigh, Iain Bethune,
    !! Toni Collis, David Scott, Peter Spelt, Mike Jackson.
    !! @version $Revision: 328 $
    !! @copyright (c) 2013-2015, Prashant Valluri, Lennon O Naraigh,
    !! Iain Bethune, David Scott, Toni Collis, Peter Spelt,
    !! The University of Edinburgh, all rights reserved.
    !! This program is distributed under the BSD License See LICENSE.txt
    !! for details.



    subroutine output_3D_hdf5(dataname_loc,sx,ex,sy,ey,sz,ez,data_array,nprocs_x,nprocs_y,nprocs_z,iteration)
    use netcdf
    use mpi
        character(len=*), intent(in) :: dataname_loc !< File name.
        integer,          intent(in) :: sx !< x dimension lower bound for local array
        integer,          intent(in) :: ex !< x dimension upper bound for local array
        integer,          intent(in) :: sy !< y dimension lower bound for local array
        integer,          intent(in) :: ey !< y dimension upper bound for local array
        integer,          intent(in) :: sz !< z dimension lower bound for local array
        integer,          intent(in) :: ez !< z dimension upper bound for local array
        double precision, intent(in) :: data_array(1:(ex-sx+1),1:(ey-sy+1),1:(ez-sz+1)) !< Local array to be
        !printed out to global data file
        ! integer,          intent(in) :: rank !< process ID
        integer,          intent(in) :: nprocs_x !< number of procs in x dim
        integer,          intent(in) :: nprocs_y !< number of procs in y dim
        integer,          intent(in) :: nprocs_z !< number of procs in z dim
        integer,          intent(in) :: iteration !< iteration number
        !Netcdf variables
        integer cmode, ncid, varid, dimid(3)
        integer psizes(3), gsizes(3), start(3), count(3)
        character(len=60) :: filename
        character(len=256) ::strg

        write(strg,*) iteration
        filename = trim(dataname_loc)//'_'//trim(strg)//'.nc'
        ! indicate to use PnetCDF to carry out I/O
        cmode = IOR(NF90_NETCDF4, NF90_MPIIO)
        ! Create output file
        call check(nf90_create(filename, cmode, ncid, comm=MPI_COMM_WORLD, info=MPI_INFO_NULL), 'creating file: ')
        gsizes(1) = (ex-sx+1)*nprocs_x
        gsizes(2) = (ey-sy+1)*nprocs_y
        gsizes(3) = (ez-sz+1)
        psizes(1) = nprocs_x
        psizes(2) = nprocs_y
        psizes(3) = nprocs_z

        ! Printing out a 3D array of phi array (phi_local(x,y,z))
        ! define dimensions x and y
        call check(nf90_def_dim(ncid, "x", gsizes(1), dimid(1)), 'In nf_def_dim X: ')
        call check(nf90_def_dim(ncid, "y", gsizes(2), dimid(2)), 'In nf_def_dim Y: ')
        call check(nf90_def_dim(ncid, "z", gsizes(3), dimid(3)), 'In nf_def_dim Z: ')
        ! define a 3D variable of type double
        call check(nf90_def_var(ncid, dataname_loc, NF90_DOUBLE, dimid, varid), 'In nf_def_var: ')
        ! exit define mode
        call check(nf90_enddef(ncid), 'In nf_enddef: ')
        !Now in NETCDF Data Mode
        ! set to use MPI/PnetCDF collective I/O
        call check(nf90_var_par_access(ncid, varid, NF90_COLLECTIVE),&
        'In nf_var_par_access: ')
        !Set up start and end points for each process' data
        start(1) = sx
        start(2) = sy
        start(3) = sz+1
        count(1) = (ex-sx)+1
        count(2) = (ey-sy)+1
        count(3) = ez-sz+1
        call check(nf90_put_var(ncid, varid, data_array, start, count),&
        'In nf_put_vara_int: ')
        ! close the file
        call check(nf90_close(ncid), 'In nf_close: ')
    end subroutine output_3D_hdf5


    ! *****************************************************************************************
    ! This  is the exact inverse of output_3d_hdf5

    !> Output 3D array with hdf5
    !> Two-phase level set input and output subroutines.
    !> Creator: Lennon O Naraigh (December 2016)

    subroutine input_3D_hdf5(input_filename,sx,ex,sy,ey,sz,ez,data_input,nprocs_x,nprocs_y,nprocs_z,iteration)
    use netcdf
    use mpi
        character(len=*), intent(in) :: input_filename !< File name.
        integer,          intent(in) :: sx !< x dimension lower bound for local array
        integer,          intent(in) :: ex !< x dimension upper bound for local array
        integer,          intent(in) :: sy !< y dimension lower bound for local array
        integer,          intent(in) :: ey !< y dimension upper bound for local array
        integer,          intent(in) :: sz !< z dimension lower bound for local array
        integer,          intent(in) :: ez !< z dimension upper bound for local array
        double precision, intent(out) :: data_input(1:(ex-sx+1),1:(ey-sy+1),1:(ez-sz+1)) !< Local array to be
        !read in from global data file
        ! integer,          intent(in) :: rank !< process ID
        integer,          intent(in) :: nprocs_x !< number of procs in x dim
        integer,          intent(in) :: nprocs_y !< number of procs in y dim
        integer,          intent(in) :: nprocs_z !< number of procs in z dim
        integer,          intent(in) :: iteration !< iteration number
        !Netcdf variables
        integer cmode, ncid, varid, dimid(3)
        integer psizes(3), gsizes(3), start(3), count(3)
        character(len=60) :: filename
        character(len=256) ::strg

        write(strg,*) iteration

        filename = trim(input_filename)//'_'//trim(strg)//'.nc'
        write(*,*) filename
        ! indicate to use PnetCDF to carry out I/O
        ! cmode = IOR(NF90_NETCDF4, NF90_NOWRITE,NF90_MPIIO)
        cmode = IOR(NF90_NOWRITE,NF90_MPIIO)


        ! Open the file.
        call check( nf90_open(filename, cmode, ncid, comm=MPI_COMM_WORLD, info=MPI_INFO_NULL), 'opening file: ')

        ! Get the varid of the data variable, based on its name.
        call check( nf90_inq_varid(ncid, input_filename, varid),  'checking varid of file: ' )

        ! Old debugging steps
        ! write(*,*) input_filename
        ! write(*,*) varid

        ! Read the data.
        ! Set up start and end points for each process' data
        start(1) = sx
        start(2) = sy
        start(3) = sz+1
        count(1) = (ex-sx)+1
        count(2) = (ey-sy)+1
        count(3) = ez-sz+1
        call check(nf90_get_var(ncid, varid, data_input, start, count),&
        'In nf_get_vara_int: ')
        ! close the file
        call check(nf90_close(ncid), 'In nf_close: ')
    end subroutine input_3D_hdf5
end module netcdf_stuff
