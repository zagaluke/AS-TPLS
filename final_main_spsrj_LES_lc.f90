!! @author Prashant Valluri, Lennon O Naraigh, Jean-Christophe Loiseau,
!! James Fannon, Peter Spelt
!! @copyright (c) 2013-2015, Prashant Valluri, Lennon O Naraigh, Jean-Christophe Loiseau,
!! James Fannon, Peter Spelt
!! This program is distributed under the BSD License See LICENSE.txt
!! for details.

program mainprogram
!        use netcdf
!        use netcdf_stuff
        use, intrinsic :: iso_fortran_env
        implicit none
        include 'mpif.h'

	integer :: maxl,maxm,maxn,iteration,iteration_sor, &
               max_iteration_mom_u,max_iteration_mom_v,max_iteration_mom_w, &
               max_iteration_pres,n_timesteps
        parameter (max_iteration_mom_u=20,max_iteration_mom_v=20,max_iteration_mom_w=20)
        double precision :: pi
        parameter (pi=4.D0*DATAN(1.D0))

        integer :: master_id,Ndim,num_procs_x,num_procs_y,num_procs_z,etag,stag
        ! NUMBER OF MPI PROCESSES
        parameter (master_id=0,Ndim=3)
        parameter (etag=1,stag=2)
                
        integer :: status(MPI_STATUS_SIZE), ierr,dims(Ndim),coords(Ndim), comm2d_periodic
        integer :: n_local_x,n_local_y,n_local_z_uv,n_local_z_w,n_local_z_p
        integer :: sx,sy,ex,ey,sz_uv,ez_uv,sz_w,ez_w,sz_p,ez_p,neighbours(6),my_id,sender_id,num_procs
        integer :: sx_o,ex_o,sy_o,ey_o,sx_f,ex_f,sy_f,ey_f
        integer :: px,py,pz
        integer :: stride_uv_xz, stride_uv_yz,stride_w_xz,stride_w_yz, stride_p_xz,stride_p_yz
        integer :: stride_uv_aug1_xz,stride_uv_aug1_yz,stride_uv_aug2_xz,stride_uv_aug2_yz,           &
               stride_w_aug1_xz,stride_w_aug1_yz,stride_w_aug2_xz,stride_w_aug2_yz,                   &
               stride_p_aug1_xz,stride_p_aug1_yz,stride_p_aug2_xz,stride_p_aug2_yz
        integer :: stride_p_augaug1_xz,stride_p_augaug1_yz,stride_p_augaug2_xz,stride_p_augaug2_yz,   &
               stride_p_augaug3_xz,stride_p_augaug3_yz
        logical :: periodic(Ndim), reorder, parallel_io, dynam_smag

        double precision :: dx,dy,dz,x_val,y_val,z_val
        
        double precision :: err1,err2,t1,t2,t_temp
        double precision, allocatable, dimension(:,:,:) :: u_global, &
               v_global,                                             &
               w_global,                                             &
               pres_global,                                          &
               pres_hydro_global,                                    &
               visc_global

        double precision, allocatable, dimension(:,:,:) :: delta_u,delta_v,delta_w
	      ! Conv - Convective step in the projection method
        double precision, allocatable, dimension(:,:,:) :: u2,u3,u3_old,conv0_u,conv1_u,conv2_u, &
               diffusion_u,RHS_u,tempr_u,                                       &
               v2,v3,v3_old,conv0_v,conv1_v,conv2_v,diffusion_v,RHS_v, tempr_v, &
               w2,w3,w3_old,conv0_w,conv1_w,conv2_w,diffusion_w,RHS_w, tempr_w, &
               pres,pres_old,pres_temp,pres_hydro,tempr_p,RHS_p,                &
               output_u,output_w,output_p,u_m,u_o,v_m,v_o,w_m,w_o,p_o,visc_o,   &
               conv0_u_o, conv1_u_o, conv0_v_o, conv1_v_o, conv0_w_o,           &
               conv1_w_o, pres_old_o

        double precision, allocatable, dimension(:,:,:) :: u2_aug,v2_aug,w2_aug,viscosity1,viscosity2,viscosity2_aug, &
               tempr_visc

	!dynamic smagorinski arrays, "_f" denotes doubly filtered field
	double precision, allocatable, dimension(:,:,:) :: abs_s, abs_s_f
	double precision, allocatable, dimension(:,:,:) :: s11, s22, s33, s12, s13, s23
	double precision, allocatable, dimension(:,:,:) :: s11_f, s22_f, s33_f, s12_f, s13_f, s23_f
	double precision, allocatable, dimension(:,:,:) :: u1u1, u2u2, u3u3, u1u2, u1u3, u2u3
	double precision, allocatable, dimension(:,:,:) :: u1u1_f, u2u2_f, u3u3_f, u1u2_f, u1u3_f, u2u3_f
	double precision, allocatable, dimension(:,:,:) :: abs_s_s11,abs_s_s22,abs_s_s33,abs_s_s12,abs_s_s13,abs_s_s23
	double precision, allocatable, dimension(:,:,:) :: abs_s_s11_f,abs_s_s22_f,abs_s_s33_f,abs_s_s12_f,abs_s_s13_f,abs_s_s23_f
	double precision, allocatable, dimension(:,:,:) :: u_c, u_c_f, v_c, v_c_f, w_c, w_c_f

        double precision, allocatable, dimension(:,:,:) :: conv0_u_global,  & !I changed these variables to "allocatable"
                conv1_u_global,                                             & !because they depend on maxl, maxm, man, which
                conv0_v_global, conv1_v_global,                             & !haven't been read from the input file yet. CMC.
                conv0_w_global, conv1_w_global,                             &
                pres_old_global
                      
        integer :: i,j,k

        double precision :: Re,dt,cfl,maxu,dpdl,max_time
        double precision :: rho_ugrid,rho_vgrid,rho_wgrid

 	! SRJ related variables
        
        double precision :: poisson_tolerance
        integer :: srj_count, max_srj
        double precision, allocatable, dimension(:) :: relax_vals

	! Input parameter related variables

        integer ::                          RetCode, RetCode2
        character (len=200) ::              line
        integer, dimension(:) ::            input(0:5)
        double precision, dimension(:) ::   input2(0:2)
        logical, dimension(:) ::            input3(0:1)
        double precision ::                 input4


        ! dpdl=-2 in the dimensionless units used in the thesis
        ! this will make the base-state profile equal to
        ! u=Re*z*(1-z*z)
        !parameter (Re=360.d0,dpdl=-2.d0) commented out CMC.
        
        character*60 filename0,filename1

        integer :: backup_counter, backup_frequency, backup
        character(len=256) :: dataname        
        integer :: file_output_period,t_restart,fieldbackup_ctr
        character(len=256) :: filename

! ****************************************************************************************
! Parameters

! Read input parameters. Added a text file containing the "input" array.

        open(unit=14, file='input.txt') !integer inputs
        read(14,*) input
        read(14,*) input2
        read(14,*) input3
        read(14,*) input4

        num_procs_x = input(0)
        num_procs_y = input(1)
        num_procs_z = input(2)
        maxl        = input(3)
        maxm        = input(4)
        maxn        = input(5)

        Re          = input2(0)
        dt          = input2(1)
        max_time    = input2(2)

        parallel_io = input3(0)
        dynam_smag  = input3(1)

        dpdl        = input4

        close(14)


    !   I/O paramaters

        backup_counter=0
        backup_frequency=1000

        dx=8.d0/dble(maxl-1)
        dy=4.d0/dble(maxm-1)
        dz=1.d0/dble(maxn-1)
        maxu=Re/4
        !dt=1d-4
        cfl=maxu*dt/dx
        ! Max time of 1, gives 10000 timesteps (small-ish scale test)        
        !n_timesteps=floor(1.d0/dt)
        n_timesteps=floor(max_time/dt)

	! Poisson tolerance for SRJ
        poisson_tolerance = 1.d-4

        fieldbackup_ctr=1

        write(filename0,'(A,A)')'fieldbackup0','.dat'
        write(filename1,'(A,A)')'fieldbackup1','.dat'

        
! ****************************************************************************************
! Get relaxation values for SRJ scheme

        open(unit=10,file='srj_relaxation_factors_P5_N32.dat')
        read(10,*) max_srj
        allocate(relax_vals(max_srj))
        do i = 1,max_srj
          read(10,*) relax_vals(i)
        enddo
        close(10)

! ****************************************************************************************

        allocate(u_global(0:maxl,0:maxm,0:maxn-2),         &
            v_global(0:maxl,0:maxm,0:maxn-2),              &
            w_global(0:maxl,0:maxm,0:maxn-1),              &
            pres_global(0:maxl,0:maxm,0:maxn),             &
            pres_hydro_global(0:maxl,0:maxm,0:maxn),       &
            visc_global(0:maxl,0:maxm,0:maxn),             &
            delta_u(0:maxl,0:maxm,0:maxn-2),               &
            delta_v(0:maxl,0:maxm,0:maxn-2),               &
            delta_w(0:maxl,0:maxm,0:maxn-1),               &
            conv0_u_global(0:maxl,0:maxm,0:maxn-2),        &
            conv1_u_global(0:maxl,0:maxm,0:maxn-2),        &
            conv0_v_global(0:maxl,0:maxm,0:maxn-2),        &
            conv1_v_global(0:maxl,0:maxm,0:maxn-2),        &
            conv0_w_global(0:maxl,0:maxm,0:maxn-1),        &
            conv1_w_global(0:maxl,0:maxm,0:maxn-1),        &
            pres_old_global(0:maxl,0:maxm,0:maxn))

    
               
! ****************************************************************************************
! MPI stuff                
	
	! ============================================================================================
	! ============================================================================================
        Call mpi_init(ierr)
        Call mpi_comm_rank(mpi_comm_world,my_id,ierr)
        Call mpi_comm_size(mpi_comm_world,num_procs,ierr)

	! Code will exit if the number of grid points modulo the number of processors is
	! not an integer.
                
        if(my_id==0)then
          write(*,*) 'num_procs=', num_procs
          
          if(num_procs_x*num_procs_y*num_procs_z/=num_procs)then
            write(*,*) 'Error 1: domain decomposition inconsistent with number of processors, exiting'
            stop
          end if
          
           if(num_procs_z/=1)then
            write(*,*) 'Error 2: domain decomposition inconsistent with number of processors, exiting'
            stop
          end if
          
          if((mod(maxl-1,num_procs_x)/=0).or.(mod(maxm-1,num_procs_y)/=0))then
            write(*,*) 'Error 3: number of processors must evenly divide (grid dimension-1), exiting'
            stop
          end if
          
        end if

        dims(1) = num_procs_x
        dims(2) = num_procs_y
        dims(3) = num_procs_z
                
        periodic(1) = .true.
        periodic(2) = .true.
        periodic(3) = .false.
        reorder = .false.

        Call mpi_cart_create(mpi_comm_world,Ndim,dims,periodic,reorder,comm2d_periodic,ierr)
        Call mpi_comm_rank(  comm2d_periodic,my_id,ierr)
        Call mpi_cart_coords(comm2d_periodic,my_id,Ndim,coords,ierr)
        Call get_mpi_neighbours(neighbours,comm2d_periodic)

        call mpi_barrier(mpi_comm_world,ierr)
        
        call mpi_decomp_2d(sx,ex,sy,ey,n_local_x,n_local_y,maxl,maxm,coords,dims,Ndim)
        
        sz_uv=0
        ez_uv=maxn-2
        n_local_z_uv=ez_uv-sz_uv+1        
        
        sz_w =0
        ez_w =maxn-1
        n_local_z_w =ez_w -sz_w +1 
        
        sz_p= 0
        ez_p= maxn
        n_local_z_p =ez_p- sz_p +1 
                
        call mpi_barrier(mpi_comm_world,ierr)
        
        ! First set of strides - unchanged

        call get_stride_p(stride_uv_yz,stride_uv_xz,sx,ex,sy,ey,sz_uv,ez_uv)
        call get_stride_p(stride_w_yz, stride_w_xz, sx,ex,sy,ey,sz_w, ez_w )
        call get_stride_p(                     stride_p_yz,stride_p_xz,        sx,ex,sy,ey,sz_p, ez_p)
        
        ! Second set of strides: first- and second-order halos for augmented arrays
        
        call get_stride_p_aug1(stride_uv_aug1_yz,stride_uv_aug1_xz,sx,ex,sy,ey,sz_uv,ez_uv)
        call get_stride_p_aug2(stride_uv_aug2_yz,stride_uv_aug2_xz,sx,ex,sy,ey,sz_uv,ez_uv)
        
        call get_stride_p_aug1(stride_w_aug1_yz,stride_w_aug1_xz,sx,ex,sy,ey,sz_w,ez_w)
        call get_stride_p_aug2(stride_w_aug2_yz,stride_w_aug2_xz,sx,ex,sy,ey,sz_w,ez_w)
        
        call get_stride_p_aug1(stride_p_aug1_yz,stride_p_aug1_xz,sx,ex,sy,ey,sz_p,ez_p)
        call get_stride_p_aug2(stride_p_aug2_yz,stride_p_aug2_xz,sx,ex,sy,ey,sz_p,ez_p)
        
        ! Third set of strides: first-, second-, and third-order halos for augmented arrays
                
        call get_stride_p_augaug1(stride_p_augaug1_yz,stride_p_augaug1_xz,sx,ex,sy,ey,sz_p,ez_p)
        call get_stride_p_augaug2(stride_p_augaug2_yz,stride_p_augaug2_xz,sx,ex,sy,ey,sz_p,ez_p)
        call get_stride_p_augaug2(stride_p_augaug3_yz,stride_p_augaug3_xz,sx,ex,sy,ey,sz_p,ez_p)
        
        call mpi_barrier(mpi_comm_world,ierr)

	! ============================================================================================
	! ============================================================================================
                              
! ******************************************************************************************
! Compute initial value of all variables

        if (my_id==master_id) then
          write(*,*) 'dt=',dt
          write(*,*) 'cfl=',cfl
          write(*,*) 'n_timesteps=', n_timesteps
          write(*,*) '(dx,dy,dz)=',dx,dy,dz
        end if
        
        if (my_id==master_id) then
          write (*,*) 'working on master to get initial values'          

          call get_all_initial_LES(u_global,v_global,w_global, &
              pres_global,pres_hydro_global, &
               maxl,maxm,maxn,dx,dy,dz,Re,dpdl,delta_u,delta_v,delta_w)
               
           !write(filename,'(A,A)')'AA_initial_vals','.dat'
            
	   !call channel_output_3d(u_global,v_global,w_global,pres_global,visc_global, &
           !         maxl,maxm,maxn,dx,dy,dz,filename)
           !call channel_output_2dslice_initial(u_global,v_global,w_global,delta_u,delta_w,pres_global,visc_global, &
           !    maxl,maxm,maxn,dx,dy,dz,filename)
	   !call channel_output_2dslice(u_global,v_global,w_global,delta_u,delta_w,pres_global,visc_global, &
           !    maxl,maxm,maxn,dx,dy,dz,filename)

        end if
                
        call mpi_bcast(u_global, (maxl+1)*(maxm+1)*(maxn-1),mpi_double_precision,0,mpi_comm_world,ierr)
        call mpi_bcast(v_global, (maxl+1)*(maxm+1)*(maxn-1),mpi_double_precision,0,mpi_comm_world,ierr)
        call mpi_bcast(w_global, (maxl+1)*(maxm+1)*(maxn  ),mpi_double_precision,0,mpi_comm_world,ierr)
        call mpi_bcast(pres_global,(maxl+1)*(maxm+1)*(maxn+1),mpi_double_precision,0,mpi_comm_world,ierr)
        call mpi_bcast(pres_hydro_global,(maxl+1)*(maxm+1)*(maxn+1),mpi_double_precision,0,mpi_comm_world,ierr)
        call mpi_barrier(mpi_comm_world,ierr)

! ********************* Initial conditions for pressure 
       
        allocate(      pres(sx-1:ex+1,sy-1:ey+1,sz_p:ez_p),       &
                   pres_old(sx-1:ex+1,sy-1:ey+1,sz_p:ez_p),       &
                  pres_temp(sx-1:ex+1,sy-1:ey+1,sz_p:ez_p),       &
                  pres_hydro(sx-1:ex+1,sy-1:ey+1,sz_p:ez_p),      &
                      RHS_p(sx-1:ex+1,sy-1:ey+1,sz_p:ez_p),       &
                  tempr_p(1:n_local_x,1:n_local_y,1:n_local_z_p), &
                  output_p(sx-1:ex+1,sy-1:ey+1,sz_p:ez_p),        &
                  p_o(1:(ex-sx+1),1:(ey-sy+1),1:(ez_p-sz_p+1)),             &
                  pres_old_o(1:(ex-sx+1),1:(ey-sy+1),1:(ez_p-sz_p+1))  )
                  
        pres(sx-1:ex+1,sy-1:ey+1,sz_p:ez_p)         =pres_global(sx-1:ex+1,sy-1:ey+1,sz_p:ez_p)
        pres_hydro(sx-1:ex+1,sy-1:ey+1,sz_p:ez_p)    =pres_hydro_global(sx-1:ex+1,sy-1:ey+1,sz_p:ez_p)
        pres_old=0.d0*pres
        
        
! ********************* Initial conditions for viscosity and various other things
               
        allocate(     viscosity1(sx-1:ex+1,sy-1:ey+1,sz_p:ez_p),   &
                      viscosity2(sx-1:ex+1,sy-1:ey+1,sz_p:ez_p),   &
                  viscosity2_aug(sx-2:ex+2,sy-2:ey+2,sz_p:ez_p),   &
                  tempr_visc(1:n_local_x,1:n_local_y,1:n_local_z_p), &
                  visc_o(1:(ex-sx+1),1:(ey-sy+1),1:(ez_p-sz_p+1)) )
        
	if(dynam_smag .eqv. .true.)then
	!allocate arrays used in dynamic smagorinsky in each mpi process
	allocate( abs_s(sx-1:ex+1,sy-1:ey+1,sz_p:ez_p), abs_s_f(sx-1:ex+1,sy-1:ey+1,sz_p:ez_p), &
		  s11(sx-1:ex+1,sy-1:ey+1,sz_p:ez_p), s11_f(sx-1:ex+1,sy-1:ey+1,sz_p:ez_p),     &
		  s22(sx-1:ex+1,sy-1:ey+1,sz_p:ez_p), s22_f(sx-1:ex+1,sy-1:ey+1,sz_p:ez_p),     &
		  s33(sx-1:ex+1,sy-1:ey+1,sz_p:ez_p), s33_f(sx-1:ex+1,sy-1:ey+1,sz_p:ez_p),     &
		  s12(sx-1:ex+1,sy-1:ey+1,sz_p:ez_p), s12_f(sx-1:ex+1,sy-1:ey+1,sz_p:ez_p),     &
		  s13(sx-1:ex+1,sy-1:ey+1,sz_p:ez_p), s13_f(sx-1:ex+1,sy-1:ey+1,sz_p:ez_p),     &
		  s23(sx-1:ex+1,sy-1:ey+1,sz_p:ez_p), s23_f(sx-1:ex+1,sy-1:ey+1,sz_p:ez_p),     &
		  u1u1(sx-1:ex+1,sy-1:ey+1,sz_p:ez_p), u1u1_f(sx-1:ex+1,sy-1:ey+1,sz_p:ez_p),   &
		  u2u2(sx-1:ex+1,sy-1:ey+1,sz_p:ez_p), u2u2_f(sx-1:ex+1,sy-1:ey+1,sz_p:ez_p),   &
		  u3u3(sx-1:ex+1,sy-1:ey+1,sz_p:ez_p), u3u3_f(sx-1:ex+1,sy-1:ey+1,sz_p:ez_p),   &
		  u1u2(sx-1:ex+1,sy-1:ey+1,sz_p:ez_p), u1u2_f(sx-1:ex+1,sy-1:ey+1,sz_p:ez_p),   &
		  u1u3(sx-1:ex+1,sy-1:ey+1,sz_p:ez_p), u1u3_f(sx-1:ex+1,sy-1:ey+1,sz_p:ez_p),   &
		  u2u3(sx-1:ex+1,sy-1:ey+1,sz_p:ez_p), u2u3_f(sx-1:ex+1,sy-1:ey+1,sz_p:ez_p),   &
		  u_c(sx-1:ex+1,sy-1:ey+1,sz_p:ez_p), u_c_f(sx-1:ex+1,sy-1:ey+1,sz_p:ez_p),     &	
		  v_c(sx-1:ex+1,sy-1:ey+1,sz_p:ez_p), v_c_f(sx-1:ex+1,sy-1:ey+1,sz_p:ez_p),     &
	 	  w_c(sx-1:ex+1,sy-1:ey+1,sz_p:ez_p), w_c_f(sx-1:ex+1,sy-1:ey+1,sz_p:ez_p),     &
		  abs_s_s11(sx-1:ex+1,sy-1:ey+1,sz_p:ez_p), abs_s_s11_f(sx-1:ex+1,sy-1:ey+1,sz_p:ez_p), &
		  abs_s_s22(sx-1:ex+1,sy-1:ey+1,sz_p:ez_p), abs_s_s22_f(sx-1:ex+1,sy-1:ey+1,sz_p:ez_p), &
		  abs_s_s33(sx-1:ex+1,sy-1:ey+1,sz_p:ez_p), abs_s_s33_f(sx-1:ex+1,sy-1:ey+1,sz_p:ez_p), &
		  abs_s_s12(sx-1:ex+1,sy-1:ey+1,sz_p:ez_p), abs_s_s12_f(sx-1:ex+1,sy-1:ey+1,sz_p:ez_p), &
		  abs_s_s13(sx-1:ex+1,sy-1:ey+1,sz_p:ez_p), abs_s_s13_f(sx-1:ex+1,sy-1:ey+1,sz_p:ez_p), &
		  abs_s_s23(sx-1:ex+1,sy-1:ey+1,sz_p:ez_p), abs_s_s23_f(sx-1:ex+1,sy-1:ey+1,sz_p:ez_p) )
	
	endif          
        viscosity1=0.d0
        viscosity2=0.d0
        viscosity2_aug=0.d0
        
        call mpi_barrier(mpi_comm_world,ierr)
                              
! ********************* Initial conditions in x(u) direction 
       
        allocate(          u2(sx-1:ex+1,sy-1:ey+1,sz_uv:ez_uv),       u3(sx-1:ex+1,sy-1:ey+1,sz_uv:ez_uv),    &
                       u3_old(sx-1:ex+1,sy-1:ey+1,sz_uv:ez_uv),                                               &
                      conv0_u(sx-1:ex+1,sy-1:ey+1,sz_uv:ez_uv),  conv1_u(sx-1:ex+1,sy-1:ey+1,sz_uv:ez_uv),    &
                      conv2_u(sx-1:ex+1,sy-1:ey+1,sz_uv:ez_uv),                                               &
                  diffusion_u(sx-1:ex+1,sy-1:ey+1,sz_uv:ez_uv),    RHS_u(sx-1:ex+1,sy-1:ey+1,sz_uv:ez_uv),    &
                  tempr_u(1:n_local_x,1:n_local_y,1:n_local_z_uv),                                            &
                  output_u(sx-1:ex+1,sy-1:ey+1,sz_uv:ez_uv),         u_o(1:(ex-sx+1),1:(ey-sy+1),1:(ez_uv-sz_uv+1)),  &
                    u_m(sx-1:ex+1,sy-1:ey+1,sz_uv:ez_uv),         conv0_u_o(1:(ex-sx+1),1:(ey-sy+1),1:(ez_uv-sz_uv+1)), &
                    conv1_u_o(1:(ex-sx+1),1:(ey-sy+1),1:(ez_uv-sz_uv+1))                                                   )
        
        ! Augmented array          
        allocate(u2_aug(sx-2:ex+2,sy-2:ey+2,sz_uv:ez_uv) )

        u2(sx-1:ex+1,sy-1:ey+1,sz_uv:ez_uv)    =u_global(sx-1:ex+1,sy-1:ey+1,sz_uv:ez_uv)
        u3(sx-1:ex+1,sy-1:ey+1,sz_uv:ez_uv)    =0.d0*u2
        u3_old(sx-1:ex+1,sy-1:ey+1,sz_uv:ez_uv)=0.d0*u2
        
        conv0_u=0.d0
        conv1_u=0.d0
        conv2_u=0.d0
        
        u2_aug(sx-1:ex+1,sy-1:ey+1,sz_uv:ez_uv)=u_global(sx-1:ex+1,sy-1:ey+1,sz_uv:ez_uv)
        
        diffusion_u=0.d0
        RHS_u=   0.d0
        
        call mpi_barrier(mpi_comm_world,ierr)
        
! ********************* Initial conditions in y(v) direction 
       
        allocate(          v2(sx-1:ex+1,sy-1:ey+1,sz_uv:ez_uv),     v3(sx-1:ex+1,sy-1:ey+1,sz_uv:ez_uv),      &
                       v3_old(sx-1:ex+1,sy-1:ey+1,sz_uv:ez_uv),                                               &
                      conv0_v(sx-1:ex+1,sy-1:ey+1,sz_uv:ez_uv), conv1_v(sx-1:ex+1,sy-1:ey+1,sz_uv:ez_uv),     &
                      conv2_v(sx-1:ex+1,sy-1:ey+1,sz_uv:ez_uv),                                               &
                  diffusion_v(sx-1:ex+1,sy-1:ey+1,sz_uv:ez_uv),   RHS_v(sx-1:ex+1,sy-1:ey+1,sz_uv:ez_uv),     &
                  tempr_v(1:n_local_x,1:n_local_y,1:n_local_z_uv),   v_o(1:(ex-sx+1),1:(ey-sy+1),1:(ez_uv-sz_uv+1)),  &
                    v_m(sx-1:ex+1,sy-1:ey+1,sz_uv:ez_uv),         conv0_v_o(1:(ex-sx+1),1:(ey-sy+1),1:(ez_uv-sz_uv+1)), &
                    conv1_v_o(1:(ex-sx+1),1:(ey-sy+1),1:(ez_uv-sz_uv+1))		)
        
        ! Augmented array            
        allocate(v2_aug(sx-2:ex+2,sy-2:ey+2,sz_uv:ez_uv) )

        v2(sx-1:ex+1,sy-1:ey+1,sz_uv:ez_uv)    =v_global(sx-1:ex+1,sy-1:ey+1,sz_uv:ez_uv)
        v3(sx-1:ex+1,sy-1:ey+1,sz_uv:ez_uv)    =0.d0*v2
        v3_old(sx-1:ex+1,sy-1:ey+1,sz_uv:ez_uv)=0.d0*v2
        
        conv0_v=0.d0
        conv1_v=0.d0
        conv2_v=0.d0
        
        v2_aug(sx-1:ex+1,sy-1:ey+1,sz_uv:ez_uv)=v_global(sx-1:ex+1,sy-1:ey+1,sz_uv:ez_uv)
        
        diffusion_v=0.d0
        RHS_v=   0.d0
        
        call mpi_barrier(mpi_comm_world,ierr)

! ********************* Initial conditions in z(w) direction 
       
        allocate(          w2(sx-1:ex+1,sy-1:ey+1,sz_w:ez_w),      w3(sx-1:ex+1,sy-1:ey+1,sz_w:ez_w),       &
                       w3_old(sx-1:ex+1,sy-1:ey+1,sz_w:ez_w),                                               &
                      conv0_w(sx-1:ex+1,sy-1:ey+1,sz_w:ez_w), conv1_w(sx-1:ex+1,sy-1:ey+1,sz_w:ez_w),       &
                      conv2_w(sx-1:ex+1,sy-1:ey+1,sz_w:ez_w),                                               &
                  diffusion_w(sx-1:ex+1,sy-1:ey+1,sz_w:ez_w),   RHS_w(sx-1:ex+1,sy-1:ey+1,sz_w:ez_w),       &
                  tempr_w(1:n_local_x,1:n_local_y,1:n_local_z_w),                                           &
                  output_w(sx-1:ex+1,sy-1:ey+1,sz_w:ez_w),          w_o(1:(ex-sx+1),1:(ey-sy+1),1:(ez_w-sz_w+1)),   &
                    w_m(sx-1:ex+1,sy-1:ey+1,sz_w:ez_w),       conv0_w_o(1:(ex-sx+1),1:(ey-sy+1),1:(ez_w-sz_w+1)),     &
                    conv1_w_o(1:(ex-sx+1),1:(ey-sy+1),1:(ez_w-sz_w+1))                                                   )
        
        ! Augmented array             
        allocate(w2_aug(sx-2:ex+2,sy-2:ey+2,sz_w:ez_w) )

        w2(sx-1:ex+1,sy-1:ey+1,sz_w:ez_w)    =w_global(sx-1:ex+1,sy-1:ey+1,sz_w:ez_w)
        w3(sx-1:ex+1,sy-1:ey+1,sz_w:ez_w)    =0.d0*w2
        w3_old(sx-1:ex+1,sy-1:ey+1,sz_w:ez_w)=0.d0*w2
        
        conv0_w=0.d0
        conv1_w=0.d0
        conv2_w=0.d0
        
        w2_aug=0.d0
        
        diffusion_w=0.d0
        RHS_w=   0.d0
        
        call mpi_barrier(mpi_comm_world,ierr)     
        
! ****************************************************************************************  
! Getting coords of base node in Cartesian topology.
        
        If (my_id==master_id) then
          pz=0
          do py=0,dims(2)-1
            do px=0,dims(1)-1
          
              if((pz==0).and.(py==0).and.(px==0))then
                sx_o=sx
                ex_o=ex
                sy_o=sy
                ey_o=ey 
              end if
              
            end do
          end do
        end if
        
        call mpi_barrier(mpi_comm_world,ierr) 
           
! *************************************************************************************************
!       Time loop
! *************************************************************************************************

        file_output_period=1000

        t1 = mpi_wtime()
        do iteration=1,n_timesteps
        
          if(iteration.lt.100)then
            max_iteration_pres=1000
          else
            max_iteration_pres=200
          end if

          ! if(iteration.lt.(n_timesteps/2))then
          !   file_output_period=1000
          ! else
          !   file_output_period=200
          ! endif
        
          call mpi_comm_rank(comm2d_periodic,my_id,ierr) 

	  ! ***************        LES business      *******************************************
                    
          ! Initialise augmented arrays
          u2_aug(sx:ex,sy:ey,sz_uv:ez_uv)=u2(sx:ex,sy:ey,sz_uv:ez_uv)
          v2_aug(sx:ex,sy:ey,sz_uv:ez_uv)=v2(sx:ex,sy:ey,sz_uv:ez_uv)
          w2_aug(sx:ex,sy:ey,sz_w:ez_w)  =w2(sx:ex,sy:ey,sz_w:ez_w)
          
          call mpi_barrier(comm2d_periodic,ierr)
          call mpi_barrier(mpi_comm_world,ierr)  
          
          ! Exchange first-order halos
          call exchange2d_aug1(u2_aug,stride_uv_aug1_xz,stride_uv_aug1_yz, &
               neighbours,ex,ey,ez_uv,sx,sy,sz_uv, comm2d_periodic)
          call exchange2d_aug1(v2_aug,stride_uv_aug1_xz,stride_uv_aug1_yz, &
               neighbours,ex,ey,ez_uv,sx,sy,sz_uv, comm2d_periodic)
          call exchange2d_aug1(w2_aug,stride_w_aug1_xz,stride_w_aug1_yz, &
               neighbours,ex,ey,ez_w,sx,sy,sz_w, comm2d_periodic)
       
          call mpi_barrier(comm2d_periodic,ierr)
          call mpi_barrier(mpi_comm_world,ierr)  
          
          ! Exchange second-order halos
          call exchange2d_aug2(u2_aug,stride_uv_aug2_xz,stride_uv_aug2_yz, &
               neighbours,ex,ey,ez_uv,sx,sy,sz_uv, comm2d_periodic)
          call exchange2d_aug2(v2_aug,stride_uv_aug2_xz,stride_uv_aug2_yz, &
               neighbours,ex,ey,ez_uv,sx,sy,sz_uv, comm2d_periodic)
          call exchange2d_aug2(w2_aug,stride_w_aug2_xz,stride_w_aug2_yz, &
               neighbours,ex,ey,ez_w,sx,sy,sz_w, comm2d_periodic)
               
          call mpi_barrier(comm2d_periodic,ierr)
          call mpi_barrier(mpi_comm_world,ierr)  
               
          ! *************       viscosity business      ******************************************
                    
          ! We need to use the augmented fields (i.e. u2_aug, etc.) when calculating the eddy 
	  ! viscosity. 
	  if(dynam_smag .eqv. .true.)then	

	  !Halo swap u2,v2,w2 before finding cell-centred velocities
	  call exchange2d(u2,stride_uv_xz,stride_uv_yz,neighbours,ex,ey,ez_uv,sx,sy,sz_uv, comm2d_periodic)
          call exchange2d(v2,stride_uv_xz,stride_uv_yz,neighbours,ex,ey,ez_uv,sx,sy,sz_uv, comm2d_periodic)
          call exchange2d(w2, stride_w_xz, stride_w_yz,neighbours,ex,ey,ez_w, sx,sy,sz_w,  comm2d_periodic)

	  call mpi_barrier(comm2d_periodic,ierr)
          call mpi_barrier(mpi_comm_world,ierr) 

	  !retrieve cell-centred velocities
	  call get_cell_cent_vel(u2,v2,w2,u_c,v_c,w_c,ex,ey,sx,sy,maxn)

	  !retrieve fields used in finding C_smag
	  call get_dynam_smag_fields(s11,s22,s33,s12,s13,s23,u1u1,u2u2,u3u3,u1u2,u1u3,u2u3,abs_s_s11, &
	       abs_s_s22,abs_s_s33,abs_s_s12,abs_s_s13,abs_s_s23,abs_s,u2_aug,v2_aug,w2_aug,u_c,v_c,w_c, &
	       ex,ey,sx,sy,maxn,dx,dy,dz)

	  ! Exchange first-order halos of these fields before averaging
	  call exchange2d(s11,stride_p_xz,stride_p_yz, neighbours,ex,ey,ez_p,sx,sy,sz_p, comm2d_periodic)
	  call exchange2d(s22,stride_p_xz,stride_p_yz, neighbours,ex,ey,ez_p,sx,sy,sz_p, comm2d_periodic)
	  call exchange2d(s33,stride_p_xz,stride_p_yz, neighbours,ex,ey,ez_p,sx,sy,sz_p, comm2d_periodic)
	  call exchange2d(s12,stride_p_xz,stride_p_yz, neighbours,ex,ey,ez_p,sx,sy,sz_p, comm2d_periodic)
	  call exchange2d(s13,stride_p_xz,stride_p_yz, neighbours,ex,ey,ez_p,sx,sy,sz_p, comm2d_periodic)
	  call exchange2d(s23,stride_p_xz,stride_p_yz, neighbours,ex,ey,ez_p,sx,sy,sz_p, comm2d_periodic)

          call mpi_barrier(mpi_comm_world,ierr)  
          call mpi_barrier(comm2d_periodic,ierr)

	  call exchange2d(u1u1,stride_p_xz,stride_p_yz, neighbours,ex,ey,ez_p,sx,sy,sz_p, comm2d_periodic)
	  call exchange2d(u2u2,stride_p_xz,stride_p_yz, neighbours,ex,ey,ez_p,sx,sy,sz_p, comm2d_periodic)
	  call exchange2d(u3u3,stride_p_xz,stride_p_yz, neighbours,ex,ey,ez_p,sx,sy,sz_p, comm2d_periodic)
	  call exchange2d(u1u2,stride_p_xz,stride_p_yz, neighbours,ex,ey,ez_p,sx,sy,sz_p, comm2d_periodic)
	  call exchange2d(u1u3,stride_p_xz,stride_p_yz, neighbours,ex,ey,ez_p,sx,sy,sz_p, comm2d_periodic)
	  call exchange2d(u2u3,stride_p_xz,stride_p_yz, neighbours,ex,ey,ez_p,sx,sy,sz_p, comm2d_periodic)

          call mpi_barrier(mpi_comm_world,ierr)  
          call mpi_barrier(comm2d_periodic,ierr)
	
	  call exchange2d(abs_s_s11,stride_p_xz,stride_p_yz, neighbours,ex,ey,ez_p,sx,sy,sz_p, comm2d_periodic)
	  call exchange2d(abs_s_s22,stride_p_xz,stride_p_yz, neighbours,ex,ey,ez_p,sx,sy,sz_p, comm2d_periodic)
	  call exchange2d(abs_s_s33,stride_p_xz,stride_p_yz, neighbours,ex,ey,ez_p,sx,sy,sz_p, comm2d_periodic)
	  call exchange2d(abs_s_s12,stride_p_xz,stride_p_yz, neighbours,ex,ey,ez_p,sx,sy,sz_p, comm2d_periodic)
	  call exchange2d(abs_s_s13,stride_p_xz,stride_p_yz, neighbours,ex,ey,ez_p,sx,sy,sz_p, comm2d_periodic)
	  call exchange2d(abs_s_s23,stride_p_xz,stride_p_yz, neighbours,ex,ey,ez_p,sx,sy,sz_p, comm2d_periodic)

          call mpi_barrier(mpi_comm_world,ierr)  
          call mpi_barrier(comm2d_periodic,ierr)

	  call exchange2d(u_c,stride_p_xz,stride_p_yz, neighbours,ex,ey,ez_p,sx,sy,sz_p, comm2d_periodic)
	  call exchange2d(v_c,stride_p_xz,stride_p_yz, neighbours,ex,ey,ez_p,sx,sy,sz_p, comm2d_periodic)
	  call exchange2d(w_c,stride_p_xz,stride_p_yz, neighbours,ex,ey,ez_p,sx,sy,sz_p, comm2d_periodic)

          call mpi_barrier(mpi_comm_world,ierr)  
          call mpi_barrier(comm2d_periodic,ierr)

	  call exchange2d(abs_s,stride_p_xz,stride_p_yz, neighbours,ex,ey,ez_p,sx,sy,sz_p, comm2d_periodic)

          call mpi_barrier(mpi_comm_world,ierr)  
          call mpi_barrier(comm2d_periodic,ierr)

	  !apply test filter to fields
	  call filter_field(s11,s11_f,ex,ey,sx,sy,maxn)
	  call filter_field(s22,s22_f,ex,ey,sx,sy,maxn)
	  call filter_field(s33,s33_f,ex,ey,sx,sy,maxn)
	  call filter_field(s12,s12_f,ex,ey,sx,sy,maxn)
	  call filter_field(s13,s13_f,ex,ey,sx,sy,maxn)
	  call filter_field(s23,s23_f,ex,ey,sx,sy,maxn)

	  call filter_field(u1u1,u1u1_f,ex,ey,sx,sy,maxn)
	  call filter_field(u2u2,u2u2_f,ex,ey,sx,sy,maxn)
	  call filter_field(u3u3,u3u3_f,ex,ey,sx,sy,maxn)
	  call filter_field(u1u2,u1u2_f,ex,ey,sx,sy,maxn)
	  call filter_field(u1u3,u1u3_f,ex,ey,sx,sy,maxn)
	  call filter_field(u2u3,u2u3_f,ex,ey,sx,sy,maxn)

	  call filter_field(abs_s_s11,abs_s_s11_f,ex,ey,sx,sy,maxn)
	  call filter_field(abs_s_s22,abs_s_s22_f,ex,ey,sx,sy,maxn)
	  call filter_field(abs_s_s33,abs_s_s33_f,ex,ey,sx,sy,maxn)
	  call filter_field(abs_s_s12,abs_s_s12_f,ex,ey,sx,sy,maxn)
	  call filter_field(abs_s_s13,abs_s_s13_f,ex,ey,sx,sy,maxn)
	  call filter_field(abs_s_s23,abs_s_s23_f,ex,ey,sx,sy,maxn)

	  call filter_field(abs_s,abs_s_f,ex,ey,sx,sy,maxn)

	  call filter_field(u_c,u_c_f,ex,ey,sx,sy,maxn)
	  call filter_field(v_c,v_c_f,ex,ey,sx,sy,maxn)
	  call filter_field(w_c,w_c_f,ex,ey,sx,sy,maxn)

          call mpi_barrier(mpi_comm_world,ierr)  
          call mpi_barrier(comm2d_periodic,ierr)

	  !calculate C_smag and retrieve viscosity
	  call get_dynam_visc(viscosity2,s11_f,s22_f,s33_f,s12_f,s13_f,s23_f,u1u1_f,u2u2_f,u3u3_f,u1u2_f,u1u3_f, &
	       u2u3_f,abs_s_s11_f,abs_s_s22_f,abs_s_s33_f,abs_s_s12_f,abs_s_s13_f,abs_s_s23_f,abs_s_f,abs_s,u_c_f,v_c_f, &
	       w_c_f,ex,ey,sx,sy,maxn,dx,dy,dz,Re)
	  
	  else


      call get_viscosity_les(viscosity2,u2_aug,v2_aug,w2_aug,ex,ey,sx,sy,maxn,dx,dy,dz,Re)

      endif
          ! Initialise augmented viscosity
          viscosity2_aug(sx:ex,sy:ey,sz_p:ez_p)=viscosity2(sx:ex,sy:ey,sz_p:ez_p)
          
          call mpi_barrier(mpi_comm_world,ierr)  
          call mpi_barrier(comm2d_periodic,ierr)  
          
          ! Exchange first-order halos
          call exchange2d_aug1(viscosity2_aug,stride_p_aug1_xz,stride_p_aug1_yz, &
               neighbours,ex,ey,ez_p,sx,sy,sz_p, comm2d_periodic)
               
          call mpi_barrier(comm2d_periodic,ierr) 
          call mpi_barrier(mpi_comm_world,ierr)  
              
          ! Exchange second-order halos 
          call exchange2d_aug2(viscosity2_aug,stride_p_aug2_xz,stride_p_aug2_yz, &
               neighbours,ex,ey,ez_p,sx,sy,sz_p, comm2d_periodic)  
          
          ! Exchange halos for non-augmented viscosity arrays
          call exchange2d(viscosity2,stride_p_xz,stride_p_yz, &
               neighbours,ex,ey,ez_p,sx,sy,sz_p, comm2d_periodic)
          call exchange2d(viscosity1,stride_p_xz,stride_p_yz, &
               neighbours,ex,ey,ez_p,sx,sy,sz_p, comm2d_periodic)
               
          call mpi_barrier(mpi_comm_world,ierr)  
          call mpi_barrier(comm2d_periodic,ierr)  
          
          ! Exchange first-order halos

          ! ***** Convective and diffusive terms      *********************************************
                          
          call exchange2d(u2,stride_uv_xz,stride_uv_yz,neighbours,ex,ey,ez_uv,sx,sy,sz_uv, comm2d_periodic)
          call exchange2d(v2,stride_uv_xz,stride_uv_yz,neighbours,ex,ey,ez_uv,sx,sy,sz_uv, comm2d_periodic)
          call exchange2d(w2, stride_w_xz, stride_w_yz,neighbours,ex,ey,ez_w, sx,sy,sz_w,  comm2d_periodic)

          ! convective terms - uses naive differencing.

          call get_conv_all(conv2_u,conv2_v,conv2_w,viscosity2_aug,u2,v2,w2,sx,sy,ex,ey,maxn,dx,dy,dz)
          
          ! diffusive term - uses flux-conservative differencing.
          call get_diffusion_all(diffusion_u,diffusion_v,diffusion_w,viscosity2_aug,u2,v2,w2,sx,sy,ex,ey,maxn,dx,dy,dz)
                    
          !! AB2:
          !! RHS_u=u2-dt*((3.d0/2.d0)*conv2_u-(1.d0/2.d0)*conv1_u)+(dt/2.d0)*diffusion_u+dt*((3.d0/2.d0)*csf_u2-(1.d0/2.d0)*csf_u1)
          !! RHS_v=v2-dt*((3.d0/2.d0)*conv2_v-(1.d0/2.d0)*conv1_v)+(dt/2.d0)*diffusion_v+dt*((3.d0/2.d0)*csf_v2-(1.d0/2.d0)*csf_v1)
          !! RHS_w=w2-dt*((3.d0/2.d0)*conv2_w-(1.d0/2.d0)*conv1_w)+(dt/2.d0)*diffusion_w+dt*((3.d0/2.d0)*csf_w2-(1.d0/2.d0)*csf_w1)
          
          !! AB3:

          RHS_u=u2-dt*((23.d0/12.d0)*conv2_u-(4.d0/3.d0)*conv1_u+(5.d0/12.d0)*conv0_u)+(dt/2.d0)*diffusion_u
          RHS_v=v2-dt*((23.d0/12.d0)*conv2_v-(4.d0/3.d0)*conv1_v+(5.d0/12.d0)*conv0_v)+(dt/2.d0)*diffusion_v
          RHS_w=w2-dt*((23.d0/12.d0)*conv2_w-(4.d0/3.d0)*conv1_w+(5.d0/12.d0)*conv0_w)+(dt/2.d0)*diffusion_w
              
          ! Solving helmholtz operator at level n+1 for u
          ! Initial guess for u3:
          
          u3=0.d0
          err1=0.d0
          do iteration_sor=1,(max_iteration_mom_u/2)
            u3_old=u3
            call exchange2d(u3_old,stride_uv_xz,stride_uv_yz,neighbours,ex,ey,ez_uv,sx,sy,sz_uv,comm2d_periodic)
            call do_jacobi_u(u3,u3_old,RHS_u,viscosity2_aug,       dx,dy,dz,dt,ex,ey,sx,sy,maxn)
            
            call exchange2d(u3,stride_uv_xz,stride_uv_yz,neighbours,ex,ey,ez_uv,sx,sy,sz_uv,comm2d_periodic)
            call do_sor_u(u3,RHS_u,viscosity2_aug,      dx,dy,dz,dt,ex,ey,sx,sy,maxn,1,iteration_sor)
            call exchange2d(u3,stride_uv_xz,stride_uv_yz,neighbours,ex,ey,ez_uv,sx,sy,sz_uv,comm2d_periodic)
            call do_sor_u(u3,RHS_u,viscosity2_aug,      dx,dy,dz,dt,ex,ey,sx,sy,maxn,0,iteration_sor)
          end do
        
          call get_difference(u3,u3_old,ex,ey,ez_uv,sx,sy,sz_uv,err2)
          CALL MPI_ALLREDUCE(err1, err2, 1, MPI_REAL, MPI_MAX,comm2d_periodic, ierr)
          err1 = err2
          if (my_id==master_id) then
            Write(*,*)'time iteration=',iteration, 'u---residual is ', err1
          end if
          
          v3=0.d0
          err1=0.d0
          do iteration_sor=1,(max_iteration_mom_v/2)
            v3_old=v3
            call exchange2d(v3_old,stride_uv_xz,stride_uv_yz,neighbours,ex,ey,ez_uv,sx,sy,sz_uv,comm2d_periodic)
            call do_jacobi_v(v3,v3_old,RHS_v,viscosity2_aug,       dx,dy,dz,dt,ex,ey,sx,sy,maxn)         
            call exchange2d(v3,stride_uv_xz,stride_uv_yz,neighbours,ex,ey,ez_uv,sx,sy,sz_uv,comm2d_periodic)
            call do_sor_v(v3,RHS_v,viscosity2_aug,       dx,dy,dz,dt,ex,ey,sx,sy,maxn,1,iteration_sor)
            call exchange2d(v3,stride_uv_xz,stride_uv_yz,neighbours,ex,ey,ez_uv,sx,sy,sz_uv,comm2d_periodic)
            call do_sor_v(v3,RHS_v,viscosity2_aug,       dx,dy,dz,dt,ex,ey,sx,sy,maxn,0,iteration_sor)
          end do
        
          call get_difference(v3,v3_old,ex,ey,ez_uv,sx,sy,sz_uv,err2)
          CALL MPI_ALLREDUCE(err1, err2, 1, MPI_REAL, MPI_MAX,comm2d_periodic, ierr)
          err1 = err2
          if (my_id==master_id) then
            Write(*,*)'time iteration=',iteration, 'v---residual is ', err1
          end if
          
          w3=0.d0
          err1=0.d0
          do iteration_sor=1,(max_iteration_mom_w/2)
            w3_old=w3
            call exchange2d(w3_old,stride_w_xz,stride_w_yz,neighbours,ex,ey,ez_w,sx,sy,sz_w,comm2d_periodic)
            call do_jacobi_w(w3,w3_old,RHS_w,viscosity2_aug,       dx,dy,dz,dt,ex,ey,sx,sy,maxn)
            
            call exchange2d(w3,stride_w_xz,stride_w_yz,neighbours,ex,ey,ez_w,sx,sy,sz_w,comm2d_periodic)
            call do_sor_w(w3,RHS_w,viscosity2_aug,       dx,dy,dz,dt,ex,ey,sx,sy,maxn,1,iteration_sor)
            call exchange2d(w3,stride_w_xz,stride_w_yz,neighbours,ex,ey,ez_w,sx,sy,sz_w,comm2d_periodic)
            call do_sor_w(w3,RHS_w,viscosity2_aug,       dx,dy,dz,dt,ex,ey,sx,sy,maxn,0,iteration_sor)
          end do
        
          call get_difference(w3,w3_old,ex,ey,ez_w,sx,sy,sz_w,err2)
          CALL MPI_ALLREDUCE(err1, err2, 1, MPI_REAL, MPI_MAX,comm2d_periodic, ierr)
          err1 = err2
          if (my_id==master_id) then
            Write(*,*)'time iteration=',iteration, 'w---residual is ', err1
          end if
          
          call exchange2d(u3,   stride_uv_xz, stride_uv_yz, neighbours,ex,ey,ez_uv,sx,sy,sz_uv, comm2d_periodic)
          call exchange2d(v3,   stride_uv_xz, stride_uv_yz, neighbours,ex,ey,ez_uv,sx,sy,sz_uv, comm2d_periodic)
          call exchange2d(w3,    stride_w_xz, stride_w_yz,  neighbours,ex,ey, ez_w,sx,sy, sz_w, comm2d_periodic)

          !----- Compute the RHS for the Poisson equation -----!

	  call get_source_pres(RHS_p,u3,v3,w3,dpdl,ex,ey,sx,sy,maxn,dx,dy,dz,dt)

	  call exchange2d(RHS_p,stride_p_xz,stride_p_yz,neighbours, &
		    ex,ey,ez_p,sx,sy,sz_p,comm2d_periodic,iteration)

	  !----- Begining of the Poisson solver -----!

	  pres = pres_old
	  err1 = 1.d0
	  iteration_sor = 0

	  Outer_iteration:  do iteration_sor = 1,max_iteration_pres
		  
	     Inner_iteration: do srj_count = 1,max_srj
		     
		pres_temp = pres
		     
		!----- Scheduled Relaxation Jacobi -----!
		     
		call exchange2d(pres,stride_p_xz,stride_p_yz,neighbours, &
		          ex,ey,ez_p,sx,sy,sz_p,comm2d_periodic,iteration)

		call scheduled_relaxation_jacobi(pres,RHS_p,dx,dy,dz,ex,ey,sx,sy,maxn,relax_vals(srj_count))
		     
		!----- Boundary conditions on the upper and lower walls? -----!
		     
		do j = sy,ey
		   do i = sx,ex
		      k = 0
		      rho_wgrid = 1.d0
		      pres(i,j,k) = pres(i,j,k+1)-rho_wgrid*dz*(w3(i-1,j-1,k)/dt)
		           
		      k = maxn-1
		      rho_wgrid = 1.d0
		      pres(i,j,k+1) = pres(i,j,k)+rho_wgrid*dz*(w3(i-1,j-1,k)/dt)
		   end do
		end do

		!----- Check for the residual -----!
		     
		call get_difference(pres,pres_temp,ex,ey,ez_p,sx,sy,sz_p,err2)
		call mpi_allreduce(err2,err1,1,MPI_DOUBLE_PRECISION,MPI_SUM,comm2d_periodic,ierr)
		err1 = sqrt(err1)

		if(err1.LT.poisson_tolerance) EXIT Inner_iteration
		if(isnan(err1)) STOP 'The Poisson solver blew up!'
		     
	     end do Inner_iteration

             if(err1.LT.poisson_tolerance) EXIT Outer_iteration
		  
	  end do Outer_iteration
	  
	  if (my_id==master_id) then
		  Write(*,*)'time iteration=',iteration, 'p---residual is ', err1, '(',((iteration_sor-1)*max_srj)+srj_count,')'
		  write(*,*)
	  end if
          
          call exchange2d(pres,stride_p_xz,stride_p_yz,neighbours, &
               ex,ey,ez_p,sx,sy,sz_p,comm2d_periodic,iteration)
          call get_uvw_pres(u3,v3,w3,pres,dpdl,ex,ey,sx,sy,maxn,dx,dy,dz,dt)

          !! Update steps to be done AFTER writing to files
                    
          call mpi_barrier(mpi_comm_world,ierr)
          
! *************************************************************************************************
!   parallel I/O

      if(parallel_io.eqv. .true.)then

        if(mod(iteration,file_output_period).eq.0)then
            if(my_id==master_id)then
                write(*,*) 'Iteration=',iteration,'Writing to file on master node'
            endif

            t_temp = mpi_wtime()-t1

            do i=sx,ex
                do j=sy,ey
                    do k=sz_uv,ez_uv
                        u_m(i,j,k)=(u2(i,j,k)+u2(i+1,j,k))/2.d0
                        v_m(i,j,k)=(v2(i,j,k)+v2(i,j+1,k))/2.d0
                    enddo
                    do k=sz_w,ez_w
                            w_m(i,j,k)=(w2(i,j,k)+w2(i,j,k+1))/2.d0
                    enddo
                enddo
            enddo

            dataname = 'vel_u'

            u_o(1:(ex-sx+1),1:(ey-sy+1),1:(ez_uv-sz_uv+1))=u_m(sx:ex,sy:ey,sz_uv:ez_uv)
            call output_3D_hdf5(dataname,sx,ex,sy,ey,sz_uv,ez_uv,u_o,num_procs_x,num_procs_y,num_procs_z,&
            iteration)

            dataname = 'vel_v'

            v_o(1:(ex-sx+1),1:(ey-sy+1),1:(ez_uv-sz_uv+1))=v_m(sx:ex,sy:ey,sz_uv:ez_uv)
            call output_3D_hdf5(dataname,sx,ex,sy,ey,sz_uv,ez_uv,v_o,num_procs_x,num_procs_y,num_procs_z,&
            iteration)

            dataname = 'vel_w'

            w_o(1:(ex-sx+1),1:(ey-sy+1),1:(ez_w-sz_w+1))=w_m(sx:ex,sy:ey,sz_w:ez_w)
            call output_3D_hdf5(dataname,sx,ex,sy,ey,sz_w,ez_w,w_o,num_procs_x,num_procs_y,num_procs_z,&
            iteration)

            dataname = 'pres'

            p_o(1:(ex-sx+1),1:(ey-sy+1),1:(ez_p-sz_p+1))=pres(sx:ex,sy:ey,sz_p:ez_p)
            call output_3D_hdf5(dataname,sx,ex,sy,ey,sz_p,ez_p,&
            p_o,num_procs_x,num_procs_y,num_procs_z,iteration)

            dataname = 'visc'

            visc_o(1:(ex-sx+1),1:(ey-sy+1),1:(ez_p-sz_p+1))=viscosity2(sx:ex,sy:ey,sz_p:ez_p)
            call output_3D_hdf5(dataname,sx,ex,sy,ey,sz_p,ez_p,&
            visc_o,num_procs_x,num_procs_y,num_procs_z,iteration)

        endif

        if(mod(iteration,backup_frequency).eq.0)then
            backup_counter=backup_counter+1

            if(mod(backup_counter,2).eq.0)then
                backup=0
            else
                backup=1
            end if

            dataname = "aa_u2_backup"
            u_o(1:(ex-sx+1),1:(ey-sy+1),1:(ez_uv-sz_uv+1))=u_m(sx:ex,sy:ey,sz_uv:ez_uv)
            call output_3D_hdf5(dataname,sx,ex,sy,ey,sz_uv,ez_uv,&
            u_o,num_procs_x,num_procs_y,num_procs_z,backup)

            dataname = "aa_conv0_u_backup"
            conv0_u_o(1:(ex-sx+1),1:(ey-sy+1),1:(ez_uv-sz_uv+1))=conv0_u(sx:ex,sy:ey,sz_uv:ez_uv)
            call output_3D_hdf5(dataname,sx,ex,sy,ey,sz_uv,ez_uv,&
            conv0_u_o,num_procs_x,num_procs_y,num_procs_z,backup)

            dataname = "aa_conv1_u_backup"
            conv1_u_o(1:(ex-sx+1),1:(ey-sy+1),1:(ez_uv-sz_uv+1))=conv1_u(sx:ex,sy:ey,sz_uv:ez_uv)
            call output_3D_hdf5(dataname,sx,ex,sy,ey,sz_uv,ez_uv,&
            conv1_u_o,num_procs_x,num_procs_y,num_procs_z,backup)

            dataname = "aa_v2_backup"
            v_o(1:(ex-sx+1),1:(ey-sy+1),1:(ez_uv-sz_uv+1))=v_m(sx:ex,sy:ey,sz_uv:ez_uv)
            call output_3D_hdf5(dataname,sx,ex,sy,ey,sz_uv,ez_uv,&
            v_o,num_procs_x,num_procs_y,num_procs_z,backup)

            dataname = "aa_conv0_v_backup"
            conv0_v_o(1:(ex-sx+1),1:(ey-sy+1),1:(ez_uv-sz_uv+1))=conv0_v(sx:ex,sy:ey,sz_uv:ez_uv)
            call output_3D_hdf5(dataname,sx,ex,sy,ey,sz_uv,ez_uv,&
            conv0_v_o,num_procs_x,num_procs_y,num_procs_z,backup)

            dataname = "aa_conv1_v_backup"
            conv1_v_o(1:(ex-sx+1),1:(ey-sy+1),1:(ez_uv-sz_uv+1))=conv1_v(sx:ex,sy:ey,sz_uv:ez_uv)
            call output_3D_hdf5(dataname,sx,ex,sy,ey,sz_uv,ez_uv,&
            conv1_v_o,num_procs_x,num_procs_y,num_procs_z,backup)

            dataname = "aa_w2_backup"
            w_o(1:(ex-sx+1),1:(ey-sy+1),1:(ez_w-sz_w+1))=w_m(sx:ex,sy:ey,sz_w:ez_w)
            call output_3D_hdf5(dataname,sx,ex,sy,ey,sz_w,ez_w,&
            w_o,num_procs_x,num_procs_y,num_procs_z,backup)

            dataname = "aa_conv0_w_backup"
            conv0_w_o(1:(ex-sx+1),1:(ey-sy+1),1:(ez_w-sz_w+1))=conv0_w(sx:ex,sy:ey,sz_w:ez_w)
            call output_3D_hdf5(dataname,sx,ex,sy,ey,sz_w,ez_w,&
            conv0_w_o,num_procs_x,num_procs_y,num_procs_z,backup)

            dataname = "aa_conv1_w_backup"
            conv1_w_o(1:(ex-sx+1),1:(ey-sy+1),1:(ez_w-sz_w+1))=conv1_w(sx:ex,sy:ey,sz_w:ez_w)
            call output_3D_hdf5(dataname,sx,ex,sy,ey,sz_w,ez_w,&
            conv1_w_o,num_procs_x,num_procs_y,num_procs_z,backup)

            dataname = "aa_pres_backup"
            p_o(1:(ex-sx+1),1:(ey-sy+1),1:(ez_p-sz_p+1))=pres(sx:ex,sy:ey,sz_p:ez_p)
            call output_3D_hdf5(dataname,sx,ex,sy,ey,sz_p,ez_p,&
            p_o,num_procs_x,num_procs_y,num_procs_z,backup)

            dataname = "aa_pres_old_backup"
            pres_old_o(1:(ex-sx+1),1:(ey-sy+1),1:(ez_p-sz_p+1))=pres_old(sx:ex,sy:ey,sz_p:ez_p)
            call output_3D_hdf5(dataname,sx,ex,sy,ey,sz_p,ez_p,&
            pres_old_o,num_procs_x,num_procs_y,num_procs_z,backup)

        end if

        ! end of parallel I/O
        ! reverts to serial I/O 

      else
                  
! Periodic output to files          
          
          if(mod(iteration,file_output_period).eq.0)then
          
! ***********************************************
! gather local pressures to master node

          call mpi_barrier(comm2d_periodic,ierr)
          
          ! The output pressure shall be the fluctuation part of the pressure field
          ! endowed with periodic boundary conditions
          
          output_p=pres-pres_hydro
              
          If (my_id==master_id) Then
            pres_global=0.d0
            pz=0
	    
            do py=0,dims(2)-1
              do px=0,dims(1)-1
          
              if((pz==0).and.(py==0).and.(px==0))then
                pres_global(sx_o:ex_o,sy_o:ey_o,sz_p:ez_p)=output_p(sx_o:ex_o,sy_o:ey_o,sz_p:ez_p)    
              else
            
              coords(1)=px
              coords(2)=py
              coords(3)=pz
            
              !! Find the sender's rank from the senders virtual Cartesian coords.
              Call mpi_cart_rank(comm2d_periodic,coords,sender_id,ierr)

              Call mpi_recv(sy_f,1,mpi_integer,  sender_id,stag,comm2d_periodic,status,ierr)
              Call mpi_recv(ey_f,1,  mpi_integer,  sender_id,etag,comm2d_periodic,status,ierr)
              Call mpi_recv(sx_f,1,mpi_integer,  sender_id,stag,comm2d_periodic,status,ierr)
              Call mpi_recv(ex_f,1,  mpi_integer,  sender_id,etag,comm2d_periodic,status,ierr)
              Call mpi_recv(n_local_x,1,mpi_integer,  sender_id,stag,comm2d_periodic,status,ierr)
              Call mpi_recv(n_local_y,1,  mpi_integer,  sender_id,etag,comm2d_periodic,status,ierr)
              
              write(*,*) 'pressure:',sx_f,ex_f,sy_f,ey_f
            
              Call mpi_recv(tempr_p,n_local_x*n_local_y*n_local_z_p,mpi_double_precision,sender_id,0,comm2d_periodic,status,ierr)
                      
              do k=1,n_local_z_p
                do j=1,n_local_y
                  do i=1,n_local_x
                    pres_global(sx_f-1+i,sy_f-1+j,sz_p-1+k) =tempr_p(i,j,k)
                    end do
                  end do
                end do
                        
              end if
          
              end do
            end do
          Else
            Call mpi_ssend(sy,1,mpi_integer,master_id,stag,comm2d_periodic,ierr)
            Call mpi_ssend(ey,1,  mpi_integer,master_id,etag,comm2d_periodic,ierr)
            Call mpi_ssend(sx,1,mpi_integer,master_id,stag,comm2d_periodic,ierr)
            Call mpi_ssend(ex,1,  mpi_integer,master_id,etag,comm2d_periodic,ierr)
            Call mpi_ssend(n_local_x,1,mpi_integer,master_id,stag,comm2d_periodic,ierr)
            Call mpi_ssend(n_local_y,1,  mpi_integer,master_id,etag,comm2d_periodic,ierr)
            
            Call mpi_ssend(output_p(sx:ex,sy:ey,sz_p:ez_p),n_local_x*n_local_y*n_local_z_p,mpi_double_precision,   &
                 master_id,0,comm2d_periodic,ierr)
                 
          End If

          ! enforcing periodic boundary conditions on ghost cells
    
          if (my_id==master_id) then
            pres_global(0,:,:)=pres_global(maxl-1,:,:)
            pres_global(maxl,:,:)=pres_global(1,:,:)
            pres_global(:,0,:)=pres_global(:,maxm-1,:)
            pres_global(:,maxm,:)=pres_global(:,1,:)
            pres_global(:,:,maxn)=pres_global(:,:,maxn-1)
            pres_global(:,:,0)=pres_global(:,:,1)
          end if
    
          call mpi_barrier(mpi_comm_world,ierr)
          
! ***********************************************
! gather local pressures (old pressure) to master node

          call mpi_barrier(comm2d_periodic,ierr)
          
          If (my_id==master_id) Then
            pres_old_global=0.d0
            pz=0
            do py=0,dims(2)-1
              do px=0,dims(1)-1
          
              if((pz==0).and.(py==0).and.(px==0))then
                pres_old_global(sx_o:ex_o,sy_o:ey_o,sz_p:ez_p)=pres_old(sx_o:ex_o,sy_o:ey_o,sz_p:ez_p)    
              else
            
              coords(1)=px
              coords(2)=py
              coords(3)=pz
            
              !! Find the sender's rank from the senders virtual Cartesian coords.
              Call mpi_cart_rank(comm2d_periodic,coords,sender_id,ierr)

              Call mpi_recv(sy_f,1,mpi_integer,  sender_id,stag,comm2d_periodic,status,ierr)
              Call mpi_recv(ey_f,1,  mpi_integer,  sender_id,etag,comm2d_periodic,status,ierr)
              Call mpi_recv(sx_f,1,mpi_integer,  sender_id,stag,comm2d_periodic,status,ierr)
              Call mpi_recv(ex_f,1,  mpi_integer,  sender_id,etag,comm2d_periodic,status,ierr)
              Call mpi_recv(n_local_x,1,mpi_integer,  sender_id,stag,comm2d_periodic,status,ierr)
              Call mpi_recv(n_local_y,1,  mpi_integer,  sender_id,etag,comm2d_periodic,status,ierr)
              
              write(*,*) 'old pressure:',sx_f,ex_f,sy_f,ey_f
            
              Call mpi_recv(tempr_p,n_local_x*n_local_y*n_local_z_p,mpi_double_precision,sender_id,0,comm2d_periodic,status,ierr)
                      
              do k=1,n_local_z_p
                do j=1,n_local_y
                  do i=1,n_local_x
                    pres_old_global(sx_f-1+i,sy_f-1+j,sz_p-1+k) =tempr_p(i,j,k)
                    end do
                  end do
                end do
                        
              end if
          
              end do
            end do
          Else
            Call mpi_ssend(sy,1,mpi_integer,master_id,stag,comm2d_periodic,ierr)
            Call mpi_ssend(ey,1,  mpi_integer,master_id,etag,comm2d_periodic,ierr)
            Call mpi_ssend(sx,1,mpi_integer,master_id,stag,comm2d_periodic,ierr)
            Call mpi_ssend(ex,1,  mpi_integer,master_id,etag,comm2d_periodic,ierr)
            Call mpi_ssend(n_local_x,1,mpi_integer,master_id,stag,comm2d_periodic,ierr)
            Call mpi_ssend(n_local_y,1,  mpi_integer,master_id,etag,comm2d_periodic,ierr)
            
            Call mpi_ssend(pres_old(sx:ex,sy:ey,sz_p:ez_p),n_local_x*n_local_y*n_local_z_p,mpi_double_precision,   &
                 master_id,0,comm2d_periodic,ierr)
                 
          End If

          ! enforcing periodic boundary conditions on ghost cells
    
          if (my_id==master_id) then
            pres_old_global(0,:,:)=pres_old_global(maxl-1,:,:)
            pres_old_global(maxl,:,:)=pres_old_global(1,:,:)
            pres_old_global(:,0,:)=pres_old_global(:,maxm-1,:)
            pres_old_global(:,maxm,:)=pres_old_global(:,1,:)
            pres_old_global(:,:,maxn)=pres_old_global(:,:,maxn-1)
            pres_old_global(:,:,0)=pres_old_global(:,:,1)
          end if
    
          call mpi_barrier(mpi_comm_world,ierr)
      
! ***********************************************
! gather local viscosities to master node

          call mpi_barrier(comm2d_periodic,ierr)
          
          If (my_id==master_id) Then
            visc_global=0.d0
            pz=0
            do py=0,dims(2)-1
              do px=0,dims(1)-1
          
              if((pz==0).and.(py==0).and.(px==0))then
                visc_global(sx_o:ex_o,sy_o:ey_o,sz_p:ez_p)=viscosity2(sx_o:ex_o,sy_o:ey_o,sz_p:ez_p)    
              else
            
              coords(1)=px
              coords(2)=py
              coords(3)=pz
            
              !! Find the sender's rank from the senders virtual Cartesian coords.
              Call mpi_cart_rank(comm2d_periodic,coords,sender_id,ierr)

              Call mpi_recv(sy_f,1,mpi_integer,  sender_id,stag,comm2d_periodic,status,ierr)
              Call mpi_recv(ey_f,1,  mpi_integer,  sender_id,etag,comm2d_periodic,status,ierr)
              Call mpi_recv(sx_f,1,mpi_integer,  sender_id,stag,comm2d_periodic,status,ierr)
              Call mpi_recv(ex_f,1,  mpi_integer,  sender_id,etag,comm2d_periodic,status,ierr)
              Call mpi_recv(n_local_x,1,mpi_integer,  sender_id,stag,comm2d_periodic,status,ierr)
              Call mpi_recv(n_local_y,1,  mpi_integer,  sender_id,etag,comm2d_periodic,status,ierr)
                          
              Call mpi_recv(tempr_visc,n_local_x*n_local_y*n_local_z_p,mpi_double_precision,sender_id,0,comm2d_periodic,status,ierr)
                 
              do k=1,n_local_z_p
                do j=1,n_local_y
                  do i=1,n_local_x
                    visc_global(sx_f-1+i,sy_f-1+j,sz_p-1+k) =tempr_visc(i,j,k)
                    end do
                  end do
                end do
                        
              end if
          
              end do
            end do
          Else
            Call mpi_ssend(sy,1,mpi_integer,master_id,stag,comm2d_periodic,ierr)
            Call mpi_ssend(ey,1,  mpi_integer,master_id,etag,comm2d_periodic,ierr)
            Call mpi_ssend(sx,1,mpi_integer,master_id,stag,comm2d_periodic,ierr)
            Call mpi_ssend(ex,1,  mpi_integer,master_id,etag,comm2d_periodic,ierr)
            Call mpi_ssend(n_local_x,1,mpi_integer,master_id,stag,comm2d_periodic,ierr)
            Call mpi_ssend(n_local_y,1,  mpi_integer,master_id,etag,comm2d_periodic,ierr)
            
            Call mpi_ssend(viscosity2(sx:ex,sy:ey,sz_p:ez_p),n_local_x*n_local_y*n_local_z_p,mpi_double_precision,   &
                 master_id,0,comm2d_periodic,ierr)
                 
          End If

          ! enforcing periodic boundary conditions on ghost cells
    
          if (my_id==master_id) then
            visc_global(0,:,:)=visc_global(maxl-1,:,:)
            visc_global(maxl,:,:)=visc_global(1,:,:)
            visc_global(:,0,:)=visc_global(:,maxm-1,:)
            visc_global(:,maxm,:)=visc_global(:,1,:)
            visc_global(:,:,maxn)=visc_global(:,:,maxn-1)
            visc_global(:,:,0)=visc_global(:,:,1)
          end if
    
          call mpi_barrier(mpi_comm_world,ierr)   
                       
! ***********************************************
! gather local u-velocities to master node

          call mpi_barrier(comm2d_periodic,ierr)
          
          output_u=u2
              
          If (my_id==master_id) Then
            u_global=0.d0
            pz=0
            do py=0,dims(2)-1
              do px=0,dims(1)-1
          
              if((pz==0).and.(py==0).and.(px==0))then
                u_global(sx_o:ex_o,sy_o:ey_o,sz_uv:ez_uv)=output_u(sx_o:ex_o,sy_o:ey_o,sz_uv:ez_uv)    
              else
            
              coords(1)=px
              coords(2)=py
              coords(3)=pz
            
              !! Find the sender's rank from the senders virtual Cartesian coords.
              Call mpi_cart_rank(comm2d_periodic,coords,sender_id,ierr)

              Call mpi_recv(sy_f,1,  mpi_integer,  sender_id,stag,comm2d_periodic,status,ierr)
              Call mpi_recv(ey_f,1,  mpi_integer,  sender_id,etag,comm2d_periodic,status,ierr)
              Call mpi_recv(sx_f,1,  mpi_integer,  sender_id,stag,comm2d_periodic,status,ierr)
              Call mpi_recv(ex_f,1,  mpi_integer,  sender_id,etag,comm2d_periodic,status,ierr)
              Call mpi_recv(n_local_x,1,mpi_integer,  sender_id,stag,comm2d_periodic,status,ierr)
              Call mpi_recv(n_local_y,1,  mpi_integer,  sender_id,etag,comm2d_periodic,status,ierr)
            
              Call mpi_recv(tempr_u,n_local_x*n_local_y*n_local_z_uv,mpi_double_precision,sender_id,0,comm2d_periodic,status,ierr)
              
              do k=1,n_local_z_uv
                do j=1,n_local_y
                  do i=1,n_local_x
                    u_global(sx_f-1+i,sy_f-1+j,sz_uv-1+k) =tempr_u(i,j,k)
                  end do
                end do
              end do
                        
              end if
          
              end do
            end do
          Else

            Call mpi_ssend(sy,1,mpi_integer,master_id,stag,comm2d_periodic,ierr)
            Call mpi_ssend(ey,1,  mpi_integer,master_id,etag,comm2d_periodic,ierr)
            Call mpi_ssend(sx,1,mpi_integer,master_id,stag,comm2d_periodic,ierr)
            Call mpi_ssend(ex,1,  mpi_integer,master_id,etag,comm2d_periodic,ierr)
            Call mpi_ssend(n_local_x,1,mpi_integer,master_id,stag,comm2d_periodic,ierr)
            Call mpi_ssend(n_local_y,1,  mpi_integer,master_id,etag,comm2d_periodic,ierr)
      
            Call mpi_ssend(output_u(sx:ex,sy:ey,sz_uv:ez_uv),n_local_x*n_local_y*n_local_z_uv,mpi_double_precision,   &
                 master_id,0,comm2d_periodic,ierr)
          End If

          ! enforcing periodic boundary conditions on ghost cells
    
          if (my_id==master_id) then
            u_global(0,:,:)=   u_global(maxl-1,:,:)
            u_global(maxl,:,:)=u_global(1,:,:)
            u_global(:,0,:)=u_global(:,maxm-1,:)
            u_global(:,maxm,:)=u_global(:,1,:)
          end if
    
          call mpi_barrier(mpi_comm_world,ierr)
          
! ***********************************************
! gather local u-velocity convective derivatives to master node

          call mpi_barrier(comm2d_periodic,ierr)
          
          If (my_id==master_id) Then
            conv0_u_global=0.d0
            pz=0
            do py=0,dims(2)-1
              do px=0,dims(1)-1
          
              if((pz==0).and.(py==0).and.(px==0))then
                conv0_u_global(sx_o:ex_o,sy_o:ey_o,sz_uv:ez_uv)=conv0_u(sx_o:ex_o,sy_o:ey_o,sz_uv:ez_uv)    
              else
            
              coords(1)=px
              coords(2)=py
              coords(3)=pz
            
              !! Find the sender's rank from the senders virtual Cartesian coords.
              Call mpi_cart_rank(comm2d_periodic,coords,sender_id,ierr)

              Call mpi_recv(sy_f,1,  mpi_integer,  sender_id,stag,comm2d_periodic,status,ierr)
              Call mpi_recv(ey_f,1,  mpi_integer,  sender_id,etag,comm2d_periodic,status,ierr)
              Call mpi_recv(sx_f,1,  mpi_integer,  sender_id,stag,comm2d_periodic,status,ierr)
              Call mpi_recv(ex_f,1,  mpi_integer,  sender_id,etag,comm2d_periodic,status,ierr)
              Call mpi_recv(n_local_x,1,mpi_integer,  sender_id,stag,comm2d_periodic,status,ierr)
              Call mpi_recv(n_local_y,1,  mpi_integer,  sender_id,etag,comm2d_periodic,status,ierr)
            
              Call mpi_recv(tempr_u,n_local_x*n_local_y*n_local_z_uv,mpi_double_precision,sender_id,0,comm2d_periodic,status,ierr)
              
              do k=1,n_local_z_uv
                do j=1,n_local_y
                  do i=1,n_local_x
                    conv0_u_global(sx_f-1+i,sy_f-1+j,sz_uv-1+k) =tempr_u(i,j,k)
                  end do
                end do
              end do
                        
              end if
          
              end do
            end do
          Else

            Call mpi_ssend(sy,1,mpi_integer,master_id,stag,comm2d_periodic,ierr)
            Call mpi_ssend(ey,1,  mpi_integer,master_id,etag,comm2d_periodic,ierr)
            Call mpi_ssend(sx,1,mpi_integer,master_id,stag,comm2d_periodic,ierr)
            Call mpi_ssend(ex,1,  mpi_integer,master_id,etag,comm2d_periodic,ierr)
            Call mpi_ssend(n_local_x,1,mpi_integer,master_id,stag,comm2d_periodic,ierr)
            Call mpi_ssend(n_local_y,1,  mpi_integer,master_id,etag,comm2d_periodic,ierr)
      
            Call mpi_ssend(conv0_u(sx:ex,sy:ey,sz_uv:ez_uv),n_local_x*n_local_y*n_local_z_uv,mpi_double_precision,   &
                 master_id,0,comm2d_periodic,ierr)
          End If

          ! enforcing periodic boundary conditions on ghost cells
    
          if (my_id==master_id) then
            conv0_u_global(0,:,:)=   conv0_u_global(maxl-1,:,:)
            conv0_u_global(maxl,:,:)=conv0_u_global(1,:,:)
            conv0_u_global(:,0,:)=conv0_u_global(:,maxm-1,:)
            conv0_u_global(:,maxm,:)=conv0_u_global(:,1,:)
          end if
    
          call mpi_barrier(mpi_comm_world,ierr)
          
! ***********************************************
! gather local u-velocity convective derivatives to master node

          call mpi_barrier(comm2d_periodic,ierr)
          
          If (my_id==master_id) Then
            conv1_u_global=0.d0
            pz=0
            do py=0,dims(2)-1
              do px=0,dims(1)-1
          
              if((pz==0).and.(py==0).and.(px==0))then
                conv1_u_global(sx_o:ex_o,sy_o:ey_o,sz_uv:ez_uv)=conv1_u(sx_o:ex_o,sy_o:ey_o,sz_uv:ez_uv)    
              else
            
              coords(1)=px
              coords(2)=py
              coords(3)=pz
            
              !! Find the sender's rank from the senders virtual Cartesian coords.
              Call mpi_cart_rank(comm2d_periodic,coords,sender_id,ierr)

              Call mpi_recv(sy_f,1,  mpi_integer,  sender_id,stag,comm2d_periodic,status,ierr)
              Call mpi_recv(ey_f,1,  mpi_integer,  sender_id,etag,comm2d_periodic,status,ierr)
              Call mpi_recv(sx_f,1,  mpi_integer,  sender_id,stag,comm2d_periodic,status,ierr)
              Call mpi_recv(ex_f,1,  mpi_integer,  sender_id,etag,comm2d_periodic,status,ierr)
              Call mpi_recv(n_local_x,1,mpi_integer,  sender_id,stag,comm2d_periodic,status,ierr)
              Call mpi_recv(n_local_y,1,  mpi_integer,  sender_id,etag,comm2d_periodic,status,ierr)
            
              Call mpi_recv(tempr_u,n_local_x*n_local_y*n_local_z_uv,mpi_double_precision,sender_id,0,comm2d_periodic,status,ierr)
              
              do k=1,n_local_z_uv
                do j=1,n_local_y
                  do i=1,n_local_x
                    conv1_u_global(sx_f-1+i,sy_f-1+j,sz_uv-1+k) =tempr_u(i,j,k)
                  end do
                end do
              end do
                        
              end if
          
              end do
            end do
          Else

            Call mpi_ssend(sy,1,mpi_integer,master_id,stag,comm2d_periodic,ierr)
            Call mpi_ssend(ey,1,  mpi_integer,master_id,etag,comm2d_periodic,ierr)
            Call mpi_ssend(sx,1,mpi_integer,master_id,stag,comm2d_periodic,ierr)
            Call mpi_ssend(ex,1,  mpi_integer,master_id,etag,comm2d_periodic,ierr)
            Call mpi_ssend(n_local_x,1,mpi_integer,master_id,stag,comm2d_periodic,ierr)
            Call mpi_ssend(n_local_y,1,  mpi_integer,master_id,etag,comm2d_periodic,ierr)
      
            Call mpi_ssend(conv1_u(sx:ex,sy:ey,sz_uv:ez_uv),n_local_x*n_local_y*n_local_z_uv,mpi_double_precision,   &
                 master_id,0,comm2d_periodic,ierr)
          End If

          ! enforcing periodic boundary conditions on ghost cells
    
          if (my_id==master_id) then
            conv1_u_global(0,:,:)=   conv1_u_global(maxl-1,:,:)
            conv1_u_global(maxl,:,:)=conv1_u_global(1,:,:)
            conv1_u_global(:,0,:)=conv1_u_global(:,maxm-1,:)
            conv1_u_global(:,maxm,:)=conv1_u_global(:,1,:)
          end if
    
          call mpi_barrier(mpi_comm_world,ierr)
          
! ***********************************************
! gather local v-velocities to master node

          call mpi_barrier(comm2d_periodic,ierr)
    
          If (my_id==master_id) Then
            v_global=0.d0
            pz=0
            do py=0,dims(2)-1
              do px=0,dims(1)-1
          
              if((pz==0).and.(py==0).and.(px==0))then
                v_global(sx_o:ex_o,sy_o:ey_o,sz_uv:ez_uv)=v2(sx_o:ex_o,sy_o:ey_o,sz_uv:ez_uv)    
              else
            
              coords(1)=px
              coords(2)=py
              coords(3)=pz
            
              !! Find the sender's rank from the senders virtual Cartesian coords.
              Call mpi_cart_rank(comm2d_periodic,coords,sender_id,ierr)

              Call mpi_recv(sy_f,1,mpi_integer,  sender_id,stag,comm2d_periodic,status,ierr)
              Call mpi_recv(ey_f,1,  mpi_integer,  sender_id,etag,comm2d_periodic,status,ierr)
              Call mpi_recv(sx_f,1,mpi_integer,  sender_id,stag,comm2d_periodic,status,ierr)
              Call mpi_recv(ex_f,1,  mpi_integer,  sender_id,etag,comm2d_periodic,status,ierr)
              Call mpi_recv(n_local_x,1,mpi_integer,  sender_id,stag,comm2d_periodic,status,ierr)
              Call mpi_recv(n_local_y,1,  mpi_integer,  sender_id,etag,comm2d_periodic,status,ierr)
            
              Call mpi_recv(tempr_v,n_local_x*n_local_y*n_local_z_uv,mpi_double_precision,sender_id,0,comm2d_periodic,status,ierr)
              
              do k=1,n_local_z_uv
                do j=1,n_local_y
                  do i=1,n_local_x
                    v_global(sx_f-1+i,sy_f-1+j,sz_uv-1+k) =tempr_v(i,j,k)
                  end do
                end do
              end do
                        
              end if
          
              end do
            end do
          Else
            Call mpi_ssend(sy,1,mpi_integer,master_id,stag,comm2d_periodic,ierr)
            Call mpi_ssend(ey,1,  mpi_integer,master_id,etag,comm2d_periodic,ierr)
            Call mpi_ssend(sx,1,mpi_integer,master_id,stag,comm2d_periodic,ierr)
            Call mpi_ssend(ex,1,  mpi_integer,master_id,etag,comm2d_periodic,ierr)
            Call mpi_ssend(n_local_x,1,mpi_integer,master_id,stag,comm2d_periodic,ierr)
            Call mpi_ssend(n_local_y,1,  mpi_integer,master_id,etag,comm2d_periodic,ierr)
      
            Call mpi_ssend(v2(sx:ex,sy:ey,sz_uv:ez_uv),n_local_x*n_local_y*n_local_z_uv,mpi_double_precision,   &
                 master_id,0,comm2d_periodic,ierr)
          End If

          ! enforcing periodic boundary conditions on ghost cells
    
          if (my_id==master_id) then
            v_global(0,:,:)=   v_global(maxl-1,:,:)
            v_global(maxl,:,:)=v_global(1,:,:)
            v_global(:,0,:)=v_global(:,maxm-1,:)
            v_global(:,maxm,:)=v_global(:,1,:)
          end if
    
          call mpi_barrier(mpi_comm_world,ierr)

! ***********************************************
! gather local v-velocity convective derivatives to master node

          call mpi_barrier(comm2d_periodic,ierr)
    
          If (my_id==master_id) Then
            conv0_v_global=0.d0
            pz=0
            do py=0,dims(2)-1
              do px=0,dims(1)-1
          
              if((pz==0).and.(py==0).and.(px==0))then
                conv0_v_global(sx_o:ex_o,sy_o:ey_o,sz_uv:ez_uv)=conv0_v(sx_o:ex_o,sy_o:ey_o,sz_uv:ez_uv)    
              else
            
              coords(1)=px
              coords(2)=py
              coords(3)=pz
            
              !! Find the sender's rank from the senders virtual Cartesian coords.
              Call mpi_cart_rank(comm2d_periodic,coords,sender_id,ierr)

              Call mpi_recv(sy_f,1,mpi_integer,  sender_id,stag,comm2d_periodic,status,ierr)
              Call mpi_recv(ey_f,1,  mpi_integer,  sender_id,etag,comm2d_periodic,status,ierr)
              Call mpi_recv(sx_f,1,mpi_integer,  sender_id,stag,comm2d_periodic,status,ierr)
              Call mpi_recv(ex_f,1,  mpi_integer,  sender_id,etag,comm2d_periodic,status,ierr)
              Call mpi_recv(n_local_x,1,mpi_integer,  sender_id,stag,comm2d_periodic,status,ierr)
              Call mpi_recv(n_local_y,1,  mpi_integer,  sender_id,etag,comm2d_periodic,status,ierr)
            
              Call mpi_recv(tempr_v,n_local_x*n_local_y*n_local_z_uv,mpi_double_precision,sender_id,0,comm2d_periodic,status,ierr)
              
              do k=1,n_local_z_uv
                do j=1,n_local_y
                  do i=1,n_local_x
                    conv0_v_global(sx_f-1+i,sy_f-1+j,sz_uv-1+k) =tempr_v(i,j,k)
                  end do
                end do
              end do
                        
              end if
          
              end do
            end do
          Else
            Call mpi_ssend(sy,1,mpi_integer,master_id,stag,comm2d_periodic,ierr)
            Call mpi_ssend(ey,1,  mpi_integer,master_id,etag,comm2d_periodic,ierr)
            Call mpi_ssend(sx,1,mpi_integer,master_id,stag,comm2d_periodic,ierr)
            Call mpi_ssend(ex,1,  mpi_integer,master_id,etag,comm2d_periodic,ierr)
            Call mpi_ssend(n_local_x,1,mpi_integer,master_id,stag,comm2d_periodic,ierr)
            Call mpi_ssend(n_local_y,1,  mpi_integer,master_id,etag,comm2d_periodic,ierr)
      
            Call mpi_ssend(conv0_v(sx:ex,sy:ey,sz_uv:ez_uv),n_local_x*n_local_y*n_local_z_uv,mpi_double_precision,   &
                 master_id,0,comm2d_periodic,ierr)
          End If

          ! enforcing periodic boundary conditions on ghost cells
    
          if (my_id==master_id) then
            conv0_v_global(0,:,:)=   conv0_v_global(maxl-1,:,:)
            conv0_v_global(maxl,:,:)=conv0_v_global(1,:,:)
            conv0_v_global(:,0,:)=conv0_v_global(:,maxm-1,:)
            conv0_v_global(:,maxm,:)=conv0_v_global(:,1,:)
          end if
    
          call mpi_barrier(mpi_comm_world,ierr)

! ***********************************************
! gather local v-velocity convective derivatives to master node

          call mpi_barrier(comm2d_periodic,ierr)
    
          If (my_id==master_id) Then
            conv1_v_global=0.d0
            pz=0
            do py=0,dims(2)-1
              do px=0,dims(1)-1
          
              if((pz==0).and.(py==0).and.(px==0))then
                conv1_v_global(sx_o:ex_o,sy_o:ey_o,sz_uv:ez_uv)=conv1_v(sx_o:ex_o,sy_o:ey_o,sz_uv:ez_uv)    
              else
            
              coords(1)=px
              coords(2)=py
              coords(3)=pz
            
              !! Find the sender's rank from the senders virtual Cartesian coords.
              Call mpi_cart_rank(comm2d_periodic,coords,sender_id,ierr)

              Call mpi_recv(sy_f,1,mpi_integer,  sender_id,stag,comm2d_periodic,status,ierr)
              Call mpi_recv(ey_f,1,  mpi_integer,  sender_id,etag,comm2d_periodic,status,ierr)
              Call mpi_recv(sx_f,1,mpi_integer,  sender_id,stag,comm2d_periodic,status,ierr)
              Call mpi_recv(ex_f,1,  mpi_integer,  sender_id,etag,comm2d_periodic,status,ierr)
              Call mpi_recv(n_local_x,1,mpi_integer,  sender_id,stag,comm2d_periodic,status,ierr)
              Call mpi_recv(n_local_y,1,  mpi_integer,  sender_id,etag,comm2d_periodic,status,ierr)
            
              Call mpi_recv(tempr_v,n_local_x*n_local_y*n_local_z_uv,mpi_double_precision,sender_id,0,comm2d_periodic,status,ierr)
              
              do k=1,n_local_z_uv
                do j=1,n_local_y
                  do i=1,n_local_x
                    conv1_v_global(sx_f-1+i,sy_f-1+j,sz_uv-1+k) =tempr_v(i,j,k)
                  end do
                end do
              end do
                        
              end if
          
              end do
            end do
          Else
            Call mpi_ssend(sy,1,mpi_integer,master_id,stag,comm2d_periodic,ierr)
            Call mpi_ssend(ey,1,  mpi_integer,master_id,etag,comm2d_periodic,ierr)
            Call mpi_ssend(sx,1,mpi_integer,master_id,stag,comm2d_periodic,ierr)
            Call mpi_ssend(ex,1,  mpi_integer,master_id,etag,comm2d_periodic,ierr)
            Call mpi_ssend(n_local_x,1,mpi_integer,master_id,stag,comm2d_periodic,ierr)
            Call mpi_ssend(n_local_y,1,  mpi_integer,master_id,etag,comm2d_periodic,ierr)
      
            Call mpi_ssend(conv1_v(sx:ex,sy:ey,sz_uv:ez_uv),n_local_x*n_local_y*n_local_z_uv,mpi_double_precision,   &
                 master_id,0,comm2d_periodic,ierr)
          End If

          ! enforcing periodic boundary conditions on ghost cells
    
          if (my_id==master_id) then
            conv1_v_global(0,:,:)=   conv1_v_global(maxl-1,:,:)
            conv1_v_global(maxl,:,:)=conv1_v_global(1,:,:)
            conv1_v_global(:,0,:)=conv1_v_global(:,maxm-1,:)
            conv1_v_global(:,maxm,:)=conv1_v_global(:,1,:)
          end if
    
          call mpi_barrier(mpi_comm_world,ierr)
          
! ***********************************************
! gather local w-velocities to master node

          call mpi_barrier(comm2d_periodic,ierr)
          
          output_w=w2
    
          If (my_id==master_id) Then
            w_global=0.d0
            pz=0
            do py=0,dims(2)-1
              do px=0,dims(1)-1
          
              if((pz==0).and.(py==0).and.(px==0))then
                w_global(sx_o:ex_o,sy_o:ey_o,sz_w:ez_w)=output_w(sx_o:ex_o,sy_o:ey_o,sz_w:ez_w)    
              else
            
              coords(1)=px
              coords(2)=py
              coords(3)=pz
            
              !! Find the sender's rank from the senders virtual Cartesian coords.
              Call mpi_cart_rank(comm2d_periodic,coords,sender_id,ierr)

              Call mpi_recv(sy_f,1,mpi_integer,  sender_id,stag,comm2d_periodic,status,ierr)
              Call mpi_recv(ey_f,1,  mpi_integer,  sender_id,etag,comm2d_periodic,status,ierr)
              Call mpi_recv(sx_f,1,mpi_integer,  sender_id,stag,comm2d_periodic,status,ierr)
              Call mpi_recv(ex_f,1,  mpi_integer,  sender_id,etag,comm2d_periodic,status,ierr)
              Call mpi_recv(n_local_x,1,mpi_integer,  sender_id,stag,comm2d_periodic,status,ierr)
              Call mpi_recv(n_local_y,1,  mpi_integer,  sender_id,etag,comm2d_periodic,status,ierr)
              
              Call mpi_recv(tempr_w,n_local_x*n_local_y*n_local_z_w,mpi_double_precision,sender_id,0,comm2d_periodic,status,ierr)
            
              do k=1,n_local_z_w
                do j=1,n_local_y
                  do i=1,n_local_x
                    w_global(sx_f-1+i,sy_f-1+j,sz_w-1+k) =tempr_w(i,j,k)
                  end do
                end do
              end do
                        
              end if

              end do
            end do
          Else
            Call mpi_ssend(sy,1,mpi_integer,master_id,stag,comm2d_periodic,ierr)
            Call mpi_ssend(ey,1,  mpi_integer,master_id,etag,comm2d_periodic,ierr)
            Call mpi_ssend(sx,1,mpi_integer,master_id,stag,comm2d_periodic,ierr)
            Call mpi_ssend(ex,1,  mpi_integer,master_id,etag,comm2d_periodic,ierr)
            Call mpi_ssend(n_local_x,1,mpi_integer,master_id,stag,comm2d_periodic,ierr)
            Call mpi_ssend(n_local_y,1,  mpi_integer,master_id,etag,comm2d_periodic,ierr)
      
            Call mpi_ssend(output_w(sx:ex,sy:ey,sz_w:ez_w),n_local_x*n_local_y*n_local_z_w,mpi_double_precision,   &
                 master_id,0,comm2d_periodic,ierr)
          End If

          ! enforcing periodic boundary conditions on ghost cells
    
          if (my_id==master_id) then
            w_global(0,:,:)=   w_global(maxl-1,:,:)
            w_global(maxl,:,:)=w_global(1,:,:)
            w_global(:,0,:)=w_global(:,maxm-1,:)
            w_global(:,maxm,:)=w_global(:,1,:)
            w_global(:,:,0)=0.d0
            w_global(:,:,maxn-1)=0.d0
          end if
    
          call mpi_barrier(mpi_comm_world,ierr)  

! ***********************************************
! gather local w-velocity convective derivatives to master node

          call mpi_barrier(comm2d_periodic,ierr)
          
          If (my_id==master_id) Then
            conv0_w_global=0.d0
            pz=0
            do py=0,dims(2)-1
              do px=0,dims(1)-1
          
              if((pz==0).and.(py==0).and.(px==0))then
                conv0_w_global(sx_o:ex_o,sy_o:ey_o,sz_w:ez_w)=conv0_w(sx_o:ex_o,sy_o:ey_o,sz_w:ez_w)    
              else
            
              coords(1)=px
              coords(2)=py
              coords(3)=pz
            
              !! Find the sender's rank from the senders virtual Cartesian coords.
              Call mpi_cart_rank(comm2d_periodic,coords,sender_id,ierr)

              Call mpi_recv(sy_f,1,mpi_integer,  sender_id,stag,comm2d_periodic,status,ierr)
              Call mpi_recv(ey_f,1,  mpi_integer,  sender_id,etag,comm2d_periodic,status,ierr)
              Call mpi_recv(sx_f,1,mpi_integer,  sender_id,stag,comm2d_periodic,status,ierr)
              Call mpi_recv(ex_f,1,  mpi_integer,  sender_id,etag,comm2d_periodic,status,ierr)
              Call mpi_recv(n_local_x,1,mpi_integer,  sender_id,stag,comm2d_periodic,status,ierr)
              Call mpi_recv(n_local_y,1,  mpi_integer,  sender_id,etag,comm2d_periodic,status,ierr)
              
              Call mpi_recv(tempr_w,n_local_x*n_local_y*n_local_z_w,mpi_double_precision,sender_id,0,comm2d_periodic,status,ierr)
            
              do k=1,n_local_z_w
                do j=1,n_local_y
                  do i=1,n_local_x
                    conv0_w_global(sx_f-1+i,sy_f-1+j,sz_w-1+k) =tempr_w(i,j,k)
                  end do
                end do
              end do
                        
              end if

              end do
            end do
          Else
            Call mpi_ssend(sy,1,mpi_integer,master_id,stag,comm2d_periodic,ierr)
            Call mpi_ssend(ey,1,  mpi_integer,master_id,etag,comm2d_periodic,ierr)
            Call mpi_ssend(sx,1,mpi_integer,master_id,stag,comm2d_periodic,ierr)
            Call mpi_ssend(ex,1,  mpi_integer,master_id,etag,comm2d_periodic,ierr)
            Call mpi_ssend(n_local_x,1,mpi_integer,master_id,stag,comm2d_periodic,ierr)
            Call mpi_ssend(n_local_y,1,  mpi_integer,master_id,etag,comm2d_periodic,ierr)
      
            Call mpi_ssend(conv0_w(sx:ex,sy:ey,sz_w:ez_w),n_local_x*n_local_y*n_local_z_w,mpi_double_precision,   &
                 master_id,0,comm2d_periodic,ierr)
          End If

          ! enforcing periodic boundary conditions on ghost cells
    
          if (my_id==master_id) then
            conv0_w_global(0,:,:)=   conv0_w_global(maxl-1,:,:)
            conv0_w_global(maxl,:,:)=conv0_w_global(1,:,:)
            conv0_w_global(:,0,:)=conv0_w_global(:,maxm-1,:)
            conv0_w_global(:,maxm,:)=conv0_w_global(:,1,:)
            conv0_w_global(:,:,0)=0.d0
            conv0_w_global(:,:,maxn-1)=0.d0
          end if
    
          call mpi_barrier(mpi_comm_world,ierr)
          
! ***********************************************
! gather local w-velocity (convective derivative) to master node

          call mpi_barrier(comm2d_periodic,ierr)
          
          If (my_id==master_id) Then
            conv1_w_global=0.d0
            pz=0
            do py=0,dims(2)-1
              do px=0,dims(1)-1
          
              if((pz==0).and.(py==0).and.(px==0))then
                conv1_w_global(sx_o:ex_o,sy_o:ey_o,sz_w:ez_w)=conv1_w(sx_o:ex_o,sy_o:ey_o,sz_w:ez_w)    
              else
            
              coords(1)=px
              coords(2)=py
              coords(3)=pz
            
              !! Find the sender's rank from the senders virtual Cartesian coords.
              Call mpi_cart_rank(comm2d_periodic,coords,sender_id,ierr)

              Call mpi_recv(sy_f,1,mpi_integer,  sender_id,stag,comm2d_periodic,status,ierr)
              Call mpi_recv(ey_f,1,  mpi_integer,  sender_id,etag,comm2d_periodic,status,ierr)
              Call mpi_recv(sx_f,1,mpi_integer,  sender_id,stag,comm2d_periodic,status,ierr)
              Call mpi_recv(ex_f,1,  mpi_integer,  sender_id,etag,comm2d_periodic,status,ierr)
              Call mpi_recv(n_local_x,1,mpi_integer,  sender_id,stag,comm2d_periodic,status,ierr)
              Call mpi_recv(n_local_y,1,  mpi_integer,  sender_id,etag,comm2d_periodic,status,ierr)
              
              Call mpi_recv(tempr_w,n_local_x*n_local_y*n_local_z_w,mpi_double_precision,sender_id,0,comm2d_periodic,status,ierr)
            
              do k=1,n_local_z_w
                do j=1,n_local_y
                  do i=1,n_local_x
                    conv1_w_global(sx_f-1+i,sy_f-1+j,sz_w-1+k) =tempr_w(i,j,k)
                  end do
                end do
              end do
                        
              end if

              end do
            end do
          Else
            Call mpi_ssend(sy,1,mpi_integer,master_id,stag,comm2d_periodic,ierr)
            Call mpi_ssend(ey,1,  mpi_integer,master_id,etag,comm2d_periodic,ierr)
            Call mpi_ssend(sx,1,mpi_integer,master_id,stag,comm2d_periodic,ierr)
            Call mpi_ssend(ex,1,  mpi_integer,master_id,etag,comm2d_periodic,ierr)
            Call mpi_ssend(n_local_x,1,mpi_integer,master_id,stag,comm2d_periodic,ierr)
            Call mpi_ssend(n_local_y,1,  mpi_integer,master_id,etag,comm2d_periodic,ierr)
      
            Call mpi_ssend(conv1_w(sx:ex,sy:ey,sz_w:ez_w),n_local_x*n_local_y*n_local_z_w,mpi_double_precision,   &
                 master_id,0,comm2d_periodic,ierr)
          End If

          ! enforcing periodic boundary conditions on ghost cells
    
          if (my_id==master_id) then
            conv1_w_global(0,:,:)=   conv1_w_global(maxl-1,:,:)
            conv1_w_global(maxl,:,:)=conv1_w_global(1,:,:)
            conv1_w_global(:,0,:)=conv1_w_global(:,maxm-1,:)
            conv1_w_global(:,maxm,:)=conv1_w_global(:,1,:)
            conv1_w_global(:,:,0)=0.d0
            conv1_w_global(:,:,maxn-1)=0.d0
          end if
    
          call mpi_barrier(mpi_comm_world,ierr)  
          
! ***********************************************
! Write to files

          t_temp = mpi_wtime()-t1 
          
          if (my_id==master_id) then

            write(filename,'(A,I9,A)')'sp3dchannel_',iteration,'.dat'

            write(*,*) 'Iteration=',iteration,'Writing to file on master node'
            

	    call channel_output_3d(u_global,v_global,w_global,pres_global,visc_global, &
              maxl,maxm,maxn,dx,dy,dz,filename)

            !call channel_output_2dslice(u_global,v_global,w_global,pres_global,visc_global, &
            !  maxl,maxm,maxn,dx,dy,dz,filename)
 
            ! call channel_output(u_global,v_global,w_global,pres_global,visc_global, &
            !    maxl,maxm,maxn,dx,dy,dz,filename)       

            write(*,*) 'done'  

            write(*,*) 'Iteration=',iteration,'Writing to backup on master node'
           

            if(mod(fieldbackup_ctr,2).eq.0)then

	      call backup_channel(u_global,v_global,w_global, &
              conv0_u_global,conv1_u_global, &
              conv0_v_global,conv1_v_global, &
              conv0_w_global,conv1_w_global, &
              pres_old_global,pres_global,   &
              maxl,maxm,maxn,filename0)

            else
            
	      call backup_channel(u_global,v_global,w_global, &
              conv0_u_global,conv1_u_global, &
              conv0_v_global,conv1_v_global, &
              conv0_w_global,conv1_w_global, &
              pres_old_global,pres_global,   &
              maxl,maxm,maxn,filename1)
            
            end if

            write(*,*) 'done'  
            
            fieldbackup_ctr=fieldbackup_ctr+1

	
            
          end if
          
          end if
                

      endif  
! *************************************************************************************************

          conv0_u=conv1_u
          conv0_v=conv1_v
          conv0_w=conv1_w
    
          conv1_u=conv2_u
          conv1_v=conv2_v
          conv1_w=conv2_w
          
          pres_old=pres
          viscosity1=viscosity2

          u2=u3
          v2=v3
          w2=w3
          
! *************************************************************************************************
! Step out of time iteration  
        end do

! *************************************************************************************************
        
        t2 = mpi_wtime()-t1  

        if(my_id==master_id)then    
          write(*,*) 'runtime,excluding initialisation ',t2
        end if
        
        if(my_id==master_id)then
          write(*,*) 'finalising and deallocating'
        end if


        deallocate(u2,u3,u3_old,conv0_u,conv1_u,conv2_u,diffusion_u,RHS_u,tempr_u,output_u)
        deallocate(v2,v3,v3_old,conv0_v,conv1_v,conv2_v,diffusion_v,RHS_v,tempr_v)
        deallocate(w2,w3,w3_old,conv0_w,conv1_w,conv2_w,diffusion_w,RHS_w,tempr_w,output_w)
        deallocate(pres,pres_old,pres_temp,pres_hydro,RHS_p,tempr_p,output_p)
        deallocate(u2_aug,v2_aug,w2_aug,viscosity1,viscosity2,viscosity2_aug,tempr_visc) 
        
        call mpi_comm_free (comm2d_periodic, ierr)
        call mpi_finalize(ierr)

end program mainprogram
  
! ****************************************************************************************
! ****************************************************************************************

        subroutine channel_output_2dslice(u3,v3,w3,p,visc,l,m,n,dx,dy,dz,filename)
        implicit none
        integer :: l,m,n,kkk,i,j,k,j_cent
        double precision :: dx,dy,dz,x,y,z,u,v,w,du,dw
        double precision :: u3(0:l,0:m,0:n-2),v3(0:l,0:m,0:n-2),w3(0:l,0:m,0:n-1),p(0:l,0:m,0:n), &
               visc(0:l,0:m,0:n)
                           
        character*60    filename

        open(unit=9,file=filename,status='unknown')
        write(9,*)'VARIABLES="X","Z","U","W","P","Viscosity"'
        write(9,*)'ZONE T="Floor", I=',l-1,' K=',n-1,' F=POINT'
        
        j_cent=(m-1)/2
        
        do k=0,n-2
          z=dz/2.d0+dz*k              
            do i=1,l-1
              x=dx/2.d0+dx*i
              u=(u3(i,j_cent,k)+u3(i+1,j_cent,k))/2.d0
              w=(w3(i,j_cent,k)+w3(i,j_cent,k+1))/2.d0
              write(9,*)x,z,u,w,p(i+1,j_cent,k+1),visc(i+1,j_cent,k+1)
            enddo
          enddo
       
        close(9)

        return
        end subroutine channel_output_2dslice

! ****************************************************************************************
! ****************************************************************************************

        subroutine channel_output_2dslice_initial(u3,v3,w3,delta_u,delta_w,p,visc,l,m,n,dx,dy,dz,filename)
        implicit none
        integer :: l,m,n,kkk,i,j,k,j_cent
        double precision :: dx,dy,dz,x,y,z,u,v,w,du,dw
        double precision :: u3(0:l,0:m,0:n-2),v3(0:l,0:m,0:n-2),w3(0:l,0:m,0:n-1),p(0:l,0:m,0:n), &
               visc(0:l,0:m,0:n)
        double precision :: delta_u(0:l,0:m,0:n-2),delta_w(0:l,0:m,0:n-1)
                           
        character*60    filename

        open(unit=9,file=filename,status='unknown')
        write(9,*)'VARIABLES="X","Z","U","W","delta_u","delta_w","P","Viscosity"'
        write(9,*)'ZONE T="Floor", I=',l-1,' K=',n-1,' F=POINT'
        
        j_cent=(m-1)/2
        
        do k=0,n-2
          z=dz/2.d0+dz*k              
            do i=1,l-1
              x=dx/2.d0+dx*i
              u=(u3(i,j_cent,k)+u3(i+1,j_cent,k))/2.d0
              w=(w3(i,j_cent,k)+w3(i,j_cent,k+1))/2.d0
	      du=(delta_u(i,j_cent,k)+delta_u(i+1,j_cent,k))/2.d0
	      dw=(delta_w(i,j_cent,k)+delta_w(i,j_cent,k+1))/2.d0
              write(9,*)x,z,u,w,du,dw,p(i+1,j_cent,k+1),visc(i+1,j_cent,k+1)
            enddo
          enddo
       
        close(9)

        return
        end subroutine channel_output_2dslice_initial

! ****************************************************************************************

         subroutine channel_output_phi(u,l,m,n,dx,dy,dz,filename)
         implicit none
         integer :: l,m,n,i,j,k
         
         double precision :: u(0:l,0:m,0:n)
         
         integer :: iostatus,idum
         character :: q
         ! character :: end_rec
         character(LEN=1), parameter :: newline=char(10)
         CHARACTER(LEN=180) :: s_buffer
        
         integer :: output_unit,input_unit
         parameter (input_unit=8,output_unit=9)
         
         double precision :: dx,dy,dz,x_val,y_val,z_val
         double precision :: xorigin,yorigin,zorigin
         double precision :: xspacing,yspacing,zspacing
                            
         character*60    filename
                 
         xspacing=dx
         yspacing=dx
         zspacing=dx

         xorigin=(dx/2.d0)+dx
         yorigin=(dy/2.d0)
         zorigin=(dz/2.d0)
 
         ! ****************************************************************************************
         ! Writing data to vtu file

         ! double-quote character
         q=char(34)
         ! end-character for binary-record finalize
         ! end_rec=char(10)

         ! open file for mixed binary/ascii writing in VTK legacy format
         OPEN(UNIT=15,FILE=filename,form='UNFORMATTED',ACCESS='STREAM', &
                ACTION='WRITE',CONVERT='BIG_ENDIAN',IOSTAT=iostatus)

         ! writing header of file (INIT)
         WRITE(UNIT=15,IOSTAT=iostatus)  '# vtk DataFile Version 3.0'//newline
         WRITE(UNIT=15,IOSTAT=iostatus)  'test file'//newline
         WRITE(UNIT=15,IOSTAT=iostatus)  'BINARY'//newline
         WRITE(UNIT=15,IOSTAT=iostatus)   newline
         WRITE(UNIT=15,IOSTAT=iostatus)  'DATASET '//'STRUCTURED_POINTS'//newline
        
         write(s_buffer,FMT='(A,3I12)',IOSTAT=iostatus) 'DIMENSIONS',l-1,m-1,n-1
         WRITE(UNIT=15,IOSTAT=iostatus) TRIM(s_buffer)//newline
        
         write(s_buffer,FMT='(A,3E14.6E2)',IOSTAT=iostatus) 'ORIGIN',xorigin,yorigin,zorigin
         WRITE(UNIT=15,IOSTAT=iostatus) TRIM(s_buffer)//newline
        
         write(s_buffer,FMT='(A,3E14.6E2)',IOSTAT=iostatus) 'SPACING',xspacing,yspacing,zspacing
         WRITE(UNIT=15,IOSTAT=iostatus) TRIM(s_buffer)//newline
        
         write(UNIT=15,IOSTAT=iostatus)  newline
         write(s_buffer,FMT='(A,I12)',IOSTAT=iostatus) 'POINT_DATA',(l-1)*(m-1)*(n-1)
         WRITE(UNIT=15,IOSTAT=iostatus)  TRIM(s_buffer)//newline
         write(UNIT=15,IOSTAT=iostatus)  'SCALARS ' // 'u' // ' float '//newline
         write(UNIT=15,IOSTAT=iostatus)  'LOOKUP_TABLE default'//newline
        
         ! LEVEL SET
         do k=0,n-2            
           do j=0,m-2
             do i=1,l-1
	       !WRITE (UNIT=15,IOSTAT=iostatus) (pres(i+1,j,k+1))
               WRITE (UNIT=15,IOSTAT=iostatus) real(u(i+1,j,k+1))
             enddo
           enddo
         enddo
        
         write(UNIT=15,IOSTAT=iostatus) newline
        
         ! close binary file (END)
         CLOSE(15)
         
         ! ****************************************************************************************
 
         return
 
         end subroutine channel_output_phi
      
! ****************************************************************************************
  
	subroutine backup_channel(u2,v2,w2,conv0_u,conv1_u,conv0_v,conv1_v,conv0_w,conv1_w, &
               pres_old,pres,l,m,n,filename)
        implicit none
        integer ::i,j,k,l,m,n

	double precision ::u2(0:l,0:m,0:n-2),conv0_u(0:l,0:m,0:n-2),conv1_u(0:l,0:m,0:n-2), &
               v2(0:l,0:m,0:n-2),conv0_v(0:l,0:m,0:n-2),conv1_v(0:l,0:m,0:n-2), &
               w2(0:l,0:m,0:n-1),conv0_w(0:l,0:m,0:n-1),conv1_w(0:l,0:m,0:n-1), &
               pres(0:l,0:m,0:n),pres_old(0:l,0:m,0:n)
        
        character*60 :: filename

        open(9,file=filename,status='unknown')
        
        do k=0,n-2
          do j=0,m
            do i=0,l
              write(9,*)u2(i,j,k),conv0_u(i,j,k),conv1_u(i,j,k)
            end do
          end do
        end do
        
        do k=0,n-2
          do j=0,m
            do i=0,l
              write(9,*)v2(i,j,k),conv0_v(i,j,k),conv1_v(i,j,k)
            end do
          end do
        end do
        
        do k=0,n-1
          do j=0,m
            do i=0,l
              write(9,*)w2(i,j,k),conv0_w(i,j,k),conv1_w(i,j,k)
            end do
          end do
        end do
        
        do k=0,n
          do j=0,m
            do i=0,l
              write(9,*)pres(i,j,k),pres_old(i,j,k)
            end do
          end do
        end do

        close(9)

        return
        end subroutine backup_channel

! ****************************************************************************************

        subroutine dataload_channel(u2,v2,w2,conv0_u,conv1_u,conv0_v,conv1_v,conv0_w,conv1_w, &
               pres_old,pres,phi,conv0_phi,conv1_phi,l,m,n)
        implicit none
        integer ::i,j,k,l,m,n
        
        double precision ::u2(0:l,0:m,0:n-2),conv0_u(0:l,0:m,0:n-2),conv1_u(0:l,0:m,0:n-2), &
               v2(0:l,0:m,0:n-2),conv0_v(0:l,0:m,0:n-2),conv1_v(0:l,0:m,0:n-2), &
               w2(0:l,0:m,0:n-1),conv0_w(0:l,0:m,0:n-1),conv1_w(0:l,0:m,0:n-1), &
               phi(0:l,0:m,0:n),conv0_phi(0:l,0:m,0:n),conv1_phi(0:l,0:m,0:n),  &
               pres(0:l,0:m,0:n),pres_old(0:l,0:m,0:n)
        
        open(9,file='fieldbackup.dat',status='old')
        
        do k=0,n-2
          do j=0,m
            do i=0,l
              read(9,*)u2(i,j,k),conv0_u(i,j,k),conv1_u(i,j,k)
            end do
          end do
        end do
        
        do k=0,n-2
          do j=0,m
            do i=0,l
              read(9,*)v2(i,j,k),conv0_v(i,j,k),conv1_v(i,j,k)
            end do
          end do
        end do
        
        do k=0,n-1
          do j=0,m
            do i=0,l
              read(9,*)w2(i,j,k),conv0_w(i,j,k),conv1_w(i,j,k)
            end do
          end do
        end do
        
        do k=0,n
          do j=0,m
            do i=0,l
              read(9,*)phi(i,j,k),conv0_phi(i,j,k),conv1_phi(i,j,k)
            end do
          end do
        end do
        
        do k=0,n
          do j=0,m
            do i=0,l
              read(9,*)pres(i,j,k),pres_old(i,j,k)
            end do
          end do
        end do

        close(9)

        return
        end subroutine dataload_channel

! ****************************************************************************************

        subroutine channel_output_vz(u3,v3,w3,p,visc,l,m,n,dx,dy,dz,filename)
        implicit none
        integer :: l,m,n,kkk,i,j,k,i_cent
        double precision :: dx,dy,dz,x,y,z,u,v,w
        double precision :: u3(0:l,0:m,0:n-2),v3(0:l,0:m,0:n-2),w3(0:l,0:m,0:n-1),p(0:l,0:m,0:n), &
               visc(0:l,0:m,0:n)
                           
        character*60    filename

        open(unit=9,file=filename,status='unknown')
        write(9,*)'VARIABaug="X ","Y ","Z ","U","V","W","P"'
        write(9,*)'ZONE T="Floor", I=',l-1,' J=',m-1,' K=',n-1,' F=POINT'
        
        i_cent=(l-1)/2
        
        do k=0,n-2
          z=dz/2.d0+dz*k              
            do j=1,m-1
              y=dy/2.d0+dy*j
              v=(v3(i_cent,j,k)+u3(i_cent,j+1,k))/2.d0
              w=(w3(i_cent,j,k)+w3(i_cent,j,k+1))/2.d0
              write(9,*)y,z,v,w,p(i_cent,j+1,k+1),visc(i_cent,j+1,k+1)
            enddo
        enddo
        
               
        close(9)

        return
        end subroutine channel_output_vz

! ****************************************************************************************

        subroutine channel_output_3d(u3,v3,w3,p,visc,l,m,n,dx,dy,dz,filename)
        implicit none
        integer :: l,m,n,kkk,i,j,k
        double precision :: dx,dy,dz,x,y,z,u,v,w
        double precision :: u3(0:l,0:m,0:n-2),v3(0:l,0:m,0:n-2),w3(0:l,0:m,0:n-1),p(0:l,0:m,0:n), &
               visc(0:l,0:m,0:n)
                           
        character*60    filename

        open(unit=9,file=filename,status='unknown')
        !write(9,*)'VARIABaug="X ","Y ","Z ","U","V","W","P","Visc"'
        !write(9,*)'ZONE T="Floor", I=',l-1,' J=',m-1,' K=',n-1,' F=POINT'
        
        do k=0,n-2
          z=dz/2.d0+dz*k              
          do j=0,m-2
            y=dy/2.d0+dy*j
            do i=0,l-2
              x=dx/2.d0+dx*i
              u=(u3(i,j,k)+u3(i+1,j,  k  ))/2.d0
              v=(v3(i,j,k)+v3(i,  j+1,k  ))/2.d0
              w=(w3(i,j,k)+w3(i,  j,  k+1))/2.d0
              write(9,*)x,y,z,u,v,w,p(i+1,j,k+1),visc(i+1,j,k+1)
            enddo
          enddo
        enddo
        
        ! Question: why is it p(i+1,j,k+1) and not p(i+1,j+1,k+1) ??
               
        close(9)

        return
        end subroutine channel_output_3d

! ****************************************************************************************



