!! @author Prashant Valluri, Lennon O Naraigh, Jean-Christophe Loiseau,
!! James Fannon, Peter Spelt
!! @copyright (c) 2013-2015, Prashant Valluri, Lennon O Naraigh, Jean-Christophe Loiseau,
!! James Fannon, Peter Spelt
!! This program is distributed under the BSD License See LICENSE.txt
!! for details.

        subroutine get_source_pres(rhs,u3,v3,w3,dpdl,ex,ey,sx,sy,n,dx,dy,dz,dt)
        implicit none
        
        integer :: ex,ey,sx,sy,n
        integer :: i,j,k
        double precision :: dx,dy,dz,dt,scalar_plusx,scalar_minusx,dpdl
        double precision ::  rhs(sx-1:ex+1,sy-1:ey+1,0:n)
        
        double precision :: u3(sx-1:ex+1,sy-1:ey+1,0:n-2), v3(sx-1:ex+1,sy-1:ey+1,0:n-2), &
               w3(sx-1:ex+1,sy-1:ey+1,0:n-1)
               
        ! double precision :: rhs0(sx-1:ex+1,sy-1:ey+1,0:n),rhs1(sx-1:ex+1,sy-1:ey+1,0:n)
               
        rhs=0.d0
                        
        do k=1,n-1
          do j=sy,ey
            do i=sx,ex
              rhs(i,j,k)=((u3(i,j-1,k-1)-u3(i-1,j-1,k-1))/dx)+ ((v3(i-1,j,k-1)-v3(i-1,j-1,k-1))/dy)+ &
               ((w3(i-1,j-1,k)-w3(i-1,j-1,k-1))/dz)
            end do
          end do
        end do
        
        rhs=rhs/dt
        
        ! do k=1,n-1
        !   do j=sy,ey
        !     do i=sx,ex
        !       scalar_plusx= ((1.d0/density(i+1,j,k))+(1.d0/density(i,j,k)))/2.d0
        !       scalar_minusx=((1.d0/density(i-1,j,k))+(1.d0/density(i,j,k)))/2.d0
        !     
        !       rhs1(i,j,k)=dabs(dpdl)*(1.d0/dx)*(scalar_plusx-scalar_minusx)
        !     end do
        !   end do
        ! end do
        ! 
        ! rhs=rhs0+rhs1
        
        
        
        return
        end subroutine get_source_pres
        
! ****************************************************************************************
        
         subroutine  get_uvw_pres(u3,v3,w3,p,dpdl,ex,ey,sx,sy,n,dx,dy,dz,dt)
         implicit none
         
         integer :: sx,ex,sy,ey,n
         integer :: i,j,k
         double precision :: p(sx-1:ex+1,sy-1:ey+1,0:n),u3(sx-1:ex+1,sy-1:ey+1,0:n-2), &
                v3(sx-1:ex+1,sy-1:ey+1,0:n-2), w3(sx-1:ex+1,sy-1:ey+1,0:n-1), &
                density(sx-1:ex+1,sy-1:ey+1,0:n)
         double precision :: dx,dy,dz,dt
         
         double precision :: gz,Grav,rho_ugrid,rho_vgrid,rho_wgrid,dpdl
         
         do k=0,n-2
           do j=sy,ey
         	  do i=sx,ex
         	    rho_ugrid=1.d0
         	    u3(i,j,k)=u3(i,j,k)-(dt/dx)*(1.d0/rho_ugrid)*(p(i+1,j+1,k+1)-p(i,j+1,k+1)) &
         	     +dt*dabs(dpdl)*(1.d0/rho_ugrid)
         	  end do
         	end do
         end do
         
         do k=0,n-2
           do j=sy,ey
         	  do i=sx,ex
         	    rho_vgrid=1.d0
         	    v3(i,j,k)=v3(i,j,k)-(dt/dy)*(1.d0/rho_vgrid)*(p(i+1,j+1,k+1)-p(i+1,j,k+1))
         	  end do
         	end do
         end do
        
         do k=0,n-1
           do j=sy,ey
             do i=sx,ex
              rho_wgrid=1.d0
         	    w3(i,j,k)=w3(i,j,k)-(dt/dz)*(1.d0/rho_wgrid)*(p(i+1,j+1,k+1)-p(i+1,j+1,k))
         	  end do
           end do
         end do
          
         w3(:,:,0)=0.d0
         w3(:,:,n-1)=0.d0
         
         end subroutine get_uvw_pres
