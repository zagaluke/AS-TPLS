!Empty subroutines in order to define the NetCDF functions in the main code.

subroutine check(status,message)
integer, intent (in) :: status
character(len=*) message
end subroutine check

subroutine output_3D_hdf5(dataname_loc,sx,ex,sy,ey,sz,ez,data_array,nprocs_x,nprocs_y,nprocs_z,iteration)
character(len=*), intent(in) :: dataname_loc !< File name.
integer,          intent(in) :: sx !< x dimension lower bound for local array
integer,          intent(in) :: ex !< x dimension upper bound for local array
integer,          intent(in) :: sy !< y dimension lower bound for local array
integer,          intent(in) :: ey !< y dimension upper bound for local array
integer,          intent(in) :: sz !< z dimension lower bound for local array
integer,          intent(in) :: ez !< z dimension upper bound for local array
double precision, intent(in) :: data_array(1:(ex-sx+1),1:(ey-sy+1),1:(ez-sz+1)) !< Local array to be
!printed out to global data file
! integer,          intent(in) :: rank !< process ID
integer,          intent(in) :: nprocs_x !< number of procs in x dim
integer,          intent(in) :: nprocs_y !< number of procs in y dim
integer,          intent(in) :: nprocs_z !< number of procs in z dim
integer,          intent(in) :: iteration !< iteration number
!Netcdf variables
integer cmode, ncid, varid, dimid(3)
integer psizes(3), gsizes(3), start(3), count(3)
character(len=60) :: filename
character(len=256) ::strg

end subroutine output_3D_hdf5

subroutine input_3D_hdf5(input_filename,sx,ex,sy,ey,sz,ez,data_input,nprocs_x,nprocs_y,nprocs_z,iteration)
character(len=*), intent(in) :: input_filename !< File name.
integer,          intent(in) :: sx !< x dimension lower bound for local array
integer,          intent(in) :: ex !< x dimension upper bound for local array
integer,          intent(in) :: sy !< y dimension lower bound for local array
integer,          intent(in) :: ey !< y dimension upper bound for local array
integer,          intent(in) :: sz !< z dimension lower bound for local array
integer,          intent(in) :: ez !< z dimension upper bound for local array
double precision, intent(out) :: data_input(1:(ex-sx+1),1:(ey-sy+1),1:(ez-sz+1)) !< Local array to be
!read in from global data file
! integer,          intent(in) :: rank !< process ID
integer,          intent(in) :: nprocs_x !< number of procs in x dim
integer,          intent(in) :: nprocs_y !< number of procs in y dim
integer,          intent(in) :: nprocs_z !< number of procs in z dim
integer,          intent(in) :: iteration !< iteration number
!Netcdf variables
integer cmode, ncid, varid, dimid(3)
integer psizes(3), gsizes(3), start(3), count(3)
character(len=60) :: filename
character(len=256) ::strg

end subroutine input_3D_hdf5

