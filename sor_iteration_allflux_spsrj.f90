!! @author Prashant Valluri, Lennon O Naraigh, Jean-Christophe Loiseau,
!! James Fannon, Peter Spelt
!! @copyright (c) 2013-2015, Prashant Valluri, Lennon O Naraigh, Jean-Christophe Loiseau,
!! James Fannon, Peter Spelt
!! This program is distributed under the BSD License See LICENSE.txt
!! for details.

!**********************************************************************************************
! Subroutines for SOR iteration for the Crank--Nicholson step.
!
! These subroutines use flux-conservative differencing, and the cell-averaged viscosities are 
! copied from the master subroutine.
!
!**********************************************************************************************

        subroutine do_sor_u(u3,RHS,viscosity,dx,dy,dz,dt,ex,ey,sx,sy,n,flg,iteration_time)
        implicit none
        
        integer :: sx,sy,ex,ey,flg
        integer :: i,j,k,n,iteration_time
        
        double precision :: u3(sx-1:ex+1,sy-1:ey+1,0:n-2), RHS(sx-1:ex+1,sy-1:ey+1,0:n-2)
        double precision :: viscosity(sx-2:ex+2,sy-2:ey+2,0:n)
        double precision :: dx,dy,dz,dt,diag_val,relax,residual,ax,ay,az
        double precision :: mu_plushalf_x_val,mu_minushalf_x_val,mu_plushalf_y_val,mu_minushalf_y_val, &
               mu_plushalf_z_val,mu_minushalf_z_val
        
        double precision :: u_minusz,u_plusz,rho_ugrid
              
        relax=1.2d0
        
        ax=dt/(dx*dx)
        ay=dt/(dy*dy)
        az=dt/(dz*dz)
           
        if(mod(iteration_time,2).eq.0)then   
        
        do k=0,n-2
          do j=sy,ey
            do i=mod(k+j+flg,2)+sx,ex,2
            
              if(k.eq.0)then
                u_minusz=-2.d0*u3(i,j,0)+u3(i,j,1)/3.d0
              else
                u_minusz=u3(i,j,k-1)
              end if
        
              if(k.eq.(n-2))then
                u_plusz=-2.d0*u3(i,j,n-2)+u3(i,j,n-3)/3.d0
              else
                u_plusz=u3(i,j,k+1)
              end if
              
              mu_plushalf_x_val =viscosity(i+1,j+1,k+1)
              mu_minushalf_x_val=viscosity(i,  j+1,k+1)
              
              mu_plushalf_y_val =(viscosity(i,j+1,k+1)+viscosity(i+1,j+1,k+1)+viscosity(i,j+2,k+1)+viscosity(i+1,j+2,k+1))/4.d0
              mu_minushalf_y_val=(viscosity(i,j+1,k+1)+viscosity(i+1,j+1,k+1)+viscosity(i,j  ,k+1)+viscosity(i+1,j  ,k+1))/4.d0
              
              mu_plushalf_z_val =(viscosity(i,j+1,k+1)+viscosity(i+1,j+1,k+1)+viscosity(i,j+1,k+2)+viscosity(i+1,j+1,k+2))/4.d0
              mu_minushalf_z_val=(viscosity(i,j+1,k+1)+viscosity(i+1,j+1,k+1)+viscosity(i,j+1,k  )+viscosity(i+1,j+1,k  ))/4.d0
              
              rho_ugrid=1.d0
                          
              diag_val=1.d0+(ax/2.d0)*(1.d0/rho_ugrid)*(mu_plushalf_x_val+mu_minushalf_x_val)+&
                            (ay/2.d0)*(1.d0/rho_ugrid)*(mu_plushalf_y_val+mu_minushalf_y_val)+&
                            (az/2.d0)*(1.d0/rho_ugrid)*(mu_plushalf_z_val+mu_minushalf_z_val)
                           
              residual = (1.d0/diag_val) * ( &
                (ax/2.d0)*(1.d0/rho_ugrid)*(mu_plushalf_x_val*u3(i+1,j,k)+mu_minushalf_x_val*u3(i-1,j,k)) + &
                (ay/2.d0)*(1.d0/rho_ugrid)*(mu_plushalf_y_val*u3(i,j+1,k)+mu_minushalf_y_val*u3(i,j-1,k)) + &
                (az/2.d0)*(1.d0/rho_ugrid)*(mu_plushalf_z_val*u_plusz    +mu_minushalf_z_val*u_minusz   ) + RHS(i,j,k))

              
              u3(i,j,k) = u3(i,j,k)+relax*(residual-u3(i,j,k))

            end do
          end do
        end do
        
        else
                
        do k=0,n-2
          do j=sy,ey
            do i=mod(j+flg,2)+sx,ex,2
            
              if(k.eq.0)then
                u_minusz=-2.d0*u3(i,j,0)+u3(i,j,1)/3.d0
              else
                u_minusz=u3(i,j,k-1)
              end if
        
              if(k.eq.(n-2))then
                u_plusz=-2.d0*u3(i,j,n-2)+u3(i,j,n-3)/3.d0
              else
                u_plusz=u3(i,j,k+1)
              end if
              
              mu_plushalf_x_val =viscosity(i+1,j+1,k+1)
              mu_minushalf_x_val=viscosity(i,  j+1,k+1)
              
              mu_plushalf_y_val =(viscosity(i,j+1,k+1)+viscosity(i+1,j+1,k+1)+viscosity(i,j+2,k+1)+viscosity(i+1,j+2,k+1))/4.d0
              mu_minushalf_y_val=(viscosity(i,j+1,k+1)+viscosity(i+1,j+1,k+1)+viscosity(i,j  ,k+1)+viscosity(i+1,j  ,k+1))/4.d0
              
              mu_plushalf_z_val =(viscosity(i,j+1,k+1)+viscosity(i+1,j+1,k+1)+viscosity(i,j+1,k+2)+viscosity(i+1,j+1,k+2))/4.d0
              mu_minushalf_z_val=(viscosity(i,j+1,k+1)+viscosity(i+1,j+1,k+1)+viscosity(i,j+1,k  )+viscosity(i+1,j+1,k  ))/4.d0
              
              rho_ugrid=1.d0
                          
              diag_val=1.d0+(ax/2.d0)*(1.d0/rho_ugrid)*(mu_plushalf_x_val+mu_minushalf_x_val)+&
                            (ay/2.d0)*(1.d0/rho_ugrid)*(mu_plushalf_y_val+mu_minushalf_y_val)+&
                            (az/2.d0)*(1.d0/rho_ugrid)*(mu_plushalf_z_val+mu_minushalf_z_val)
                           
              residual = (1.d0/diag_val) * ( &
                (ax/2.d0)*(1.d0/rho_ugrid)*(mu_plushalf_x_val*u3(i+1,j,k)+mu_minushalf_x_val*u3(i-1,j,k)) + &
                (ay/2.d0)*(1.d0/rho_ugrid)*(mu_plushalf_y_val*u3(i,j+1,k)+mu_minushalf_y_val*u3(i,j-1,k)) + &
                (az/2.d0)*(1.d0/rho_ugrid)*(mu_plushalf_z_val*u_plusz    +mu_minushalf_z_val*u_minusz   ) + RHS(i,j,k))

              
              u3(i,j,k) = u3(i,j,k)+relax*(residual-u3(i,j,k))

            end do
          end do
        end do
        
        end if
         
        return
        end subroutine do_sor_u
        
!**********************************************************************************************

        subroutine do_jacobi_u(u3,u3_old,RHS,viscosity,dx,dy,dz,dt,ex,ey,sx,sy,n)
        implicit none
        
        integer :: sx,sy,ex,ey,flg
        integer :: i,j,k,n,iteration_time
        
        double precision :: u3(sx-1:ex+1,sy-1:ey+1,0:n-2), u3_old(sx-1:ex+1,sy-1:ey+1,0:n-2), &
               RHS(sx-1:ex+1,sy-1:ey+1,0:n-2)
        double precision :: viscosity(sx-2:ex+2,sy-2:ey+2,0:n)
        double precision :: dx,dy,dz,dt,diag_val,residual,ax,ay,az
        double precision :: mu_plushalf_x_val,mu_minushalf_x_val,mu_plushalf_y_val,mu_minushalf_y_val, &
               mu_plushalf_z_val,mu_minushalf_z_val
        
        double precision :: u_minusz,u_plusz,rho_ugrid
        
        ax=dt/(dx*dx)
        ay=dt/(dy*dy)
        az=dt/(dz*dz)
           
        do k=0,n-2
          do j=sy,ey
            do i=sx,ex
            
              if(k.eq.0)then
                u_minusz=-2.d0*u3_old(i,j,0)+u3_old(i,j,1)/3.d0
              else
                u_minusz=u3_old(i,j,k-1)
              end if
        
              if(k.eq.(n-2))then
                u_plusz=-2.d0*u3_old(i,j,n-2)+u3_old(i,j,n-3)/3.d0
              else
                u_plusz=u3_old(i,j,k+1)
              end if
              
              mu_plushalf_x_val =viscosity(i+1,j+1,k+1)
              mu_minushalf_x_val=viscosity(i,  j+1,k+1)
              
              mu_plushalf_y_val =(viscosity(i,j+1,k+1)+viscosity(i+1,j+1,k+1)+viscosity(i,j+2,k+1)+viscosity(i+1,j+2,k+1))/4.d0
              mu_minushalf_y_val=(viscosity(i,j+1,k+1)+viscosity(i+1,j+1,k+1)+viscosity(i,j  ,k+1)+viscosity(i+1,j  ,k+1))/4.d0
              
              mu_plushalf_z_val =(viscosity(i,j+1,k+1)+viscosity(i+1,j+1,k+1)+viscosity(i,j+1,k+2)+viscosity(i+1,j+1,k+2))/4.d0
              mu_minushalf_z_val=(viscosity(i,j+1,k+1)+viscosity(i+1,j+1,k+1)+viscosity(i,j+1,k  )+viscosity(i+1,j+1,k  ))/4.d0
              
              rho_ugrid=1.d0
                          
              diag_val=1.d0+(ax/2.d0)*(1.d0/rho_ugrid)*(mu_plushalf_x_val+mu_minushalf_x_val)+&
                            (ay/2.d0)*(1.d0/rho_ugrid)*(mu_plushalf_y_val+mu_minushalf_y_val)+&
                            (az/2.d0)*(1.d0/rho_ugrid)*(mu_plushalf_z_val+mu_minushalf_z_val)
                           
              residual = (1.d0/diag_val) * ( &
                (ax/2.d0)*(1.d0/rho_ugrid)*(mu_plushalf_x_val*u3_old(i+1,j,k)+mu_minushalf_x_val*u3_old(i-1,j,k)) + &
                (ay/2.d0)*(1.d0/rho_ugrid)*(mu_plushalf_y_val*u3_old(i,j+1,k)+mu_minushalf_y_val*u3_old(i,j-1,k)) + &
                (az/2.d0)*(1.d0/rho_ugrid)*(mu_plushalf_z_val*u_plusz    +mu_minushalf_z_val*u_minusz   ) + RHS(i,j,k))

              
              u3(i,j,k) = residual

            end do
          end do
        end do
        
        return
        end subroutine do_jacobi_u

! **********************************************************************************************

        subroutine do_sor_v(v3,RHS,viscosity,dx,dy,dz,dt,ex,ey,sx,sy,n,flg,iteration_time)
        implicit none
        
        integer :: sx,sy,ex,ey,flg
        integer :: i,j,k,n,iteration_time
        
        double precision :: v3(sx-1:ex+1,sy-1:ey+1,0:n-2), RHS(sx-1:ex+1,sy-1:ey+1,0:n-2)
        double precision :: viscosity(sx-2:ex+2,sy-2:ey+2,0:n)
        double precision :: dx,dy,dz,dt,diag_val,relax,residual,ax,ay,az
        double precision :: mu_plushalf_x_val,mu_minushalf_x_val,mu_plushalf_y_val,mu_minushalf_y_val, &
               mu_plushalf_z_val,mu_minushalf_z_val
               
        double precision :: v_minusz,v_plusz,rho_vgrid
              
        relax=1.6d0
        ax=dt/(dx*dx)
        ay=dt/(dy*dy)
        az=dt/(dz*dz)
        
        if(mod(iteration_time,2).eq.0)then
              
        do k=0,n-2
          do j=sy,ey
            do i=mod(k+j+flg,2)+sx,ex,2
            
              if(k.eq.0)then
                v_minusz=-2.d0*v3(i,j,0)+v3(i,j,1)/3.d0
              else
                v_minusz=v3(i,j,k-1)
              end if
        
              if(k.eq.(n-2))then
                v_plusz=-2.d0*v3(i,j,n-2)+v3(i,j,n-3)/3.d0
              else
                v_plusz=v3(i,j,k+1)
              end if
              
              mu_plushalf_x_val= (viscosity(i+1,j+1,k+1)+viscosity(i+1,j,k+1)+viscosity(i+2,j+1,k+1)+viscosity(i+2,j,k+1))/4.d0
              mu_minushalf_x_val=(viscosity(i+1,j+1,k+1)+viscosity(i+1,j,k+1)+viscosity(i,  j+1,k+1)+viscosity(i,  j,k+1))/4.d0
                            
              mu_plushalf_y_val= viscosity(i+1,j+1,k+1)
              mu_minushalf_y_val=viscosity(i+1,j,  k+1)
              
              mu_plushalf_z_val= (viscosity(i+1,j+1,k+1)+viscosity(i+1,j,k+1)+viscosity(i+1,j+1,k+2)+viscosity(i+1,j,k+2))/4.d0
              mu_minushalf_z_val=(viscosity(i+1,j+1,k+1)+viscosity(i+1,j,k+1)+viscosity(i+1,j+1,k  )+viscosity(i+1,j,k  ))/4.d0
              
              rho_vgrid=1.d0
                          
              diag_val=1.d0+(ax/2.d0)*(1.d0/rho_vgrid)*(mu_plushalf_x_val+mu_minushalf_x_val)+&
                            (ay/2.d0)*(1.d0/rho_vgrid)*(mu_plushalf_y_val+mu_minushalf_y_val)+&
                            (az/2.d0)*(1.d0/rho_vgrid)*(mu_plushalf_z_val+mu_minushalf_z_val)
              
              
              residual = (1.d0/diag_val) * ( &
                (ax/2.d0)*(1.d0/rho_vgrid)*(mu_plushalf_x_val*v3(i+1,j,k)+mu_minushalf_x_val*v3(i-1,j,k)) + &
                (ay/2.d0)*(1.d0/rho_vgrid)*(mu_plushalf_y_val*v3(i,j+1,k)+mu_minushalf_y_val*v3(i,j-1,k)) + &
                (az/2.d0)*(1.d0/rho_vgrid)*(mu_plushalf_z_val*v_plusz    +mu_minushalf_z_val*v_minusz   ) + RHS(i,j,k))
              
              v3(i,j,k) = v3(i,j,k)+relax*(residual-v3(i,j,k))

            end do
          end do
        end do
        
        else
        
        do k=0,n-2
          do j=sy,ey
            do i=mod(j+flg,2)+sx,ex,2
            
              if(k.eq.0)then
                v_minusz=-2.d0*v3(i,j,0)+v3(i,j,1)/3.d0
              else
                v_minusz=v3(i,j,k-1)
              end if
        
              if(k.eq.(n-2))then
                v_plusz=-2.d0*v3(i,j,n-2)+v3(i,j,n-3)/3.d0
              else
                v_plusz=v3(i,j,k+1)
              end if
              
              mu_plushalf_x_val= (viscosity(i+1,j+1,k+1)+viscosity(i+1,j,k+1)+viscosity(i+2,j+1,k+1)+viscosity(i+2,j,k+1))/4.d0
              mu_minushalf_x_val=(viscosity(i+1,j+1,k+1)+viscosity(i+1,j,k+1)+viscosity(i,  j+1,k+1)+viscosity(i,  j,k+1))/4.d0
                            
              mu_plushalf_y_val= viscosity(i+1,j+1,k+1)
              mu_minushalf_y_val=viscosity(i+1,j,  k+1)
              
              mu_plushalf_z_val= (viscosity(i+1,j+1,k+1)+viscosity(i+1,j,k+1)+viscosity(i+1,j+1,k+2)+viscosity(i+1,j,k+2))/4.d0
              mu_minushalf_z_val=(viscosity(i+1,j+1,k+1)+viscosity(i+1,j,k+1)+viscosity(i+1,j+1,k  )+viscosity(i+1,j,k  ))/4.d0
              
              rho_vgrid=1.d0
                          
              diag_val=1.d0+(ax/2.d0)*(1.d0/rho_vgrid)*(mu_plushalf_x_val+mu_minushalf_x_val)+&
                            (ay/2.d0)*(1.d0/rho_vgrid)*(mu_plushalf_y_val+mu_minushalf_y_val)+&
                            (az/2.d0)*(1.d0/rho_vgrid)*(mu_plushalf_z_val+mu_minushalf_z_val)
              
              
              residual = (1.d0/diag_val) * ( &
                (ax/2.d0)*(1.d0/rho_vgrid)*(mu_plushalf_x_val*v3(i+1,j,k)+mu_minushalf_x_val*v3(i-1,j,k)) + &
                (ay/2.d0)*(1.d0/rho_vgrid)*(mu_plushalf_y_val*v3(i,j+1,k)+mu_minushalf_y_val*v3(i,j-1,k)) + &
                (az/2.d0)*(1.d0/rho_vgrid)*(mu_plushalf_z_val*v_plusz    +mu_minushalf_z_val*v_minusz   ) + RHS(i,j,k))
              
              v3(i,j,k) = v3(i,j,k)+relax*(residual-v3(i,j,k))

            end do
          end do
        end do
        
        end if
        
        return
        end subroutine do_sor_v
        
! **********************************************************************************************

        subroutine do_jacobi_v(v3,v3_old,RHS,viscosity,dx,dy,dz,dt,ex,ey,sx,sy,n)
        implicit none
        
        integer :: sx,sy,ex,ey
        integer :: i,j,k,n
        
        double precision :: v3(sx-1:ex+1,sy-1:ey+1,0:n-2), v3_old(sx-1:ex+1,sy-1:ey+1,0:n-2), &
               RHS(sx-1:ex+1,sy-1:ey+1,0:n-2)
        double precision :: viscosity(sx-2:ex+2,sy-2:ey+2,0:n)
        double precision :: dx,dy,dz,dt,diag_val,residual,ax,ay,az
        double precision :: mu_plushalf_x_val,mu_minushalf_x_val,mu_plushalf_y_val,mu_minushalf_y_val, &
               mu_plushalf_z_val,mu_minushalf_z_val
               
        double precision :: v_minusz,v_plusz,rho_vgrid
              
        ax=dt/(dx*dx)
        ay=dt/(dy*dy)
        az=dt/(dz*dz)
        
        do k=0,n-2
          do j=sy,ey
            do i=sx,ex
            
              if(k.eq.0)then
                v_minusz=-2.d0*v3_old(i,j,0)+v3_old(i,j,1)/3.d0
              else
                v_minusz=v3_old(i,j,k-1)
              end if
        
              if(k.eq.(n-2))then
                v_plusz=-2.d0*v3_old(i,j,n-2)+v3_old(i,j,n-3)/3.d0
              else
                v_plusz=v3_old(i,j,k+1)
              end if
              
              mu_plushalf_x_val= (viscosity(i+1,j+1,k+1)+viscosity(i+1,j,k+1)+viscosity(i+2,j+1,k+1)+viscosity(i+2,j,k+1))/4.d0
              mu_minushalf_x_val=(viscosity(i+1,j+1,k+1)+viscosity(i+1,j,k+1)+viscosity(i,  j+1,k+1)+viscosity(i,  j,k+1))/4.d0
                            
              mu_plushalf_y_val= viscosity(i+1,j+1,k+1)
              mu_minushalf_y_val=viscosity(i+1,j,  k+1)
              
              mu_plushalf_z_val= (viscosity(i+1,j+1,k+1)+viscosity(i+1,j,k+1)+viscosity(i+1,j+1,k+2)+viscosity(i+1,j,k+2))/4.d0
              mu_minushalf_z_val=(viscosity(i+1,j+1,k+1)+viscosity(i+1,j,k+1)+viscosity(i+1,j+1,k  )+viscosity(i+1,j,k  ))/4.d0
              
              rho_vgrid=1.d0
                          
              diag_val=1.d0+(ax/2.d0)*(1.d0/rho_vgrid)*(mu_plushalf_x_val+mu_minushalf_x_val)+&
                            (ay/2.d0)*(1.d0/rho_vgrid)*(mu_plushalf_y_val+mu_minushalf_y_val)+&
                            (az/2.d0)*(1.d0/rho_vgrid)*(mu_plushalf_z_val+mu_minushalf_z_val)
              
              
              residual = (1.d0/diag_val) * ( &
                (ax/2.d0)*(1.d0/rho_vgrid)*(mu_plushalf_x_val*v3_old(i+1,j,k)+mu_minushalf_x_val*v3_old(i-1,j,k)) + &
                (ay/2.d0)*(1.d0/rho_vgrid)*(mu_plushalf_y_val*v3_old(i,j+1,k)+mu_minushalf_y_val*v3_old(i,j-1,k)) + &
                (az/2.d0)*(1.d0/rho_vgrid)*(mu_plushalf_z_val*v_plusz    +mu_minushalf_z_val*v_minusz   ) + RHS(i,j,k))
              
              v3(i,j,k) = residual

            end do
          end do
        end do
        
        return
        end subroutine do_jacobi_v

! **********************************************************************************************

        subroutine do_sor_w(w3,RHS,viscosity,dx,dy,dz,dt,ex,ey,sx,sy,n,flg,iteration_time)
        implicit none
        
        integer :: sx,sy,ex,ey,flg
        integer :: i,j,k,n,iteration_time
        
        double precision :: w3(sx-1:ex+1,sy-1:ey+1,0:n-1), RHS(sx-1:ex+1,sy-1:ey+1,0:n-1)
        double precision :: viscosity(sx-2:ex+2,sy-2:ey+2,0:n)
        double precision :: dx,dy,dz,dt,diag_val,relax,residual,ax,ay,az, &
               mu_plushalf_x_val,mu_minushalf_x_val,mu_plushalf_y_val,mu_minushalf_y_val, &
               mu_plushalf_z_val,mu_minushalf_z_val
               
        double precision :: rho_wgrid
              
        relax=1.2d0
        ax=dt/(dx*dx)
        ay=dt/(dy*dy)
        az=dt/(dz*dz)
        
        if(mod(iteration_time,2).eq.0)then
              
        do k=1,n-2
          do j=sy,ey
            do i=mod(k+j+flg,2)+sx,ex,2
            
              mu_plushalf_x_val= (viscosity(i+1,j+1,k)+viscosity(i+1,j+1,k+1)+viscosity(i+2,j+1,k)+viscosity(i+2,j+1,k+1))/4.d0
              mu_minushalf_x_val=(viscosity(i+1,j+1,k)+viscosity(i+1,j+1,k+1)+viscosity(i  ,j+1,k)+viscosity(i  ,j+1,k+1))/4.d0
                            
              mu_plushalf_y_val= (viscosity(i+1,j+1,k)+viscosity(i+1,j+1,k+1)+viscosity(i+1,j+2,k)+viscosity(i+1,j+2,k+1))/4.d0
              mu_minushalf_y_val=(viscosity(i+1,j+1,k)+viscosity(i+1,j+1,k+1)+viscosity(i+1,j  ,k)+viscosity(i+1,j  ,k+1))/4.d0
              
              mu_plushalf_z_val=  viscosity(i+1,j+1,k+1)
              mu_minushalf_z_val= viscosity(i+1,j+1,k  )
              
              rho_wgrid=1.d0
              
              diag_val=1.d0+(ax/2.d0)*(1.d0/rho_wgrid)*(mu_plushalf_x_val+mu_minushalf_x_val)+&
                            (ay/2.d0)*(1.d0/rho_wgrid)*(mu_plushalf_y_val+mu_minushalf_y_val)+&
                            (az/2.d0)*(1.d0/rho_wgrid)*(mu_plushalf_z_val+mu_minushalf_z_val)

              residual = (1.d0/diag_val) * ( &
                (ax/2.d0)*(1.d0/rho_wgrid)*(mu_plushalf_x_val*w3(i+1,j,k)+mu_minushalf_x_val*w3(i-1,j,k)) + &
                (ay/2.d0)*(1.d0/rho_wgrid)*(mu_plushalf_y_val*w3(i,j+1,k)+mu_minushalf_y_val*w3(i,j-1,k)) + &
                (az/2.d0)*(1.d0/rho_wgrid)*(mu_plushalf_z_val*w3(i,j,k+1)+mu_minushalf_z_val*w3(i,j,k-1)) + RHS(i,j,k))
              
              w3(i,j,k) = w3(i,j,k)+relax*(residual-w3(i,j,k))
            end do
          end do
        end do
        
        w3(:,:,0)=0.d0
        w3(:,:,n-1)=0.d0
        
        else
        
        do k=1,n-2
          do j=sy,ey
            do i=mod(j+flg,2)+sx,ex,2
            
              mu_plushalf_x_val= (viscosity(i+1,j+1,k)+viscosity(i+1,j+1,k+1)+viscosity(i+2,j+1,k)+viscosity(i+2,j+1,k+1))/4.d0
              mu_minushalf_x_val=(viscosity(i+1,j+1,k)+viscosity(i+1,j+1,k+1)+viscosity(i  ,j+1,k)+viscosity(i  ,j+1,k+1))/4.d0
                            
              mu_plushalf_y_val= (viscosity(i+1,j+1,k)+viscosity(i+1,j+1,k+1)+viscosity(i+1,j+2,k)+viscosity(i+1,j+2,k+1))/4.d0
              mu_minushalf_y_val=(viscosity(i+1,j+1,k)+viscosity(i+1,j+1,k+1)+viscosity(i+1,j  ,k)+viscosity(i+1,j  ,k+1))/4.d0
              
              mu_plushalf_z_val=  viscosity(i+1,j+1,k+1)
              mu_minushalf_z_val= viscosity(i+1,j+1,k  )
              
              rho_wgrid=1.d0
              
              diag_val=1.d0+(ax/2.d0)*(1.d0/rho_wgrid)*(mu_plushalf_x_val+mu_minushalf_x_val)+&
                            (ay/2.d0)*(1.d0/rho_wgrid)*(mu_plushalf_y_val+mu_minushalf_y_val)+&
                            (az/2.d0)*(1.d0/rho_wgrid)*(mu_plushalf_z_val+mu_minushalf_z_val)

              residual = (1.d0/diag_val) * ( &
                (ax/2.d0)*(1.d0/rho_wgrid)*(mu_plushalf_x_val*w3(i+1,j,k)+mu_minushalf_x_val*w3(i-1,j,k)) + &
                (ay/2.d0)*(1.d0/rho_wgrid)*(mu_plushalf_y_val*w3(i,j+1,k)+mu_minushalf_y_val*w3(i,j-1,k)) + &
                (az/2.d0)*(1.d0/rho_wgrid)*(mu_plushalf_z_val*w3(i,j,k+1)+mu_minushalf_z_val*w3(i,j,k-1)) + RHS(i,j,k))
              
              w3(i,j,k) = w3(i,j,k)+relax*(residual-w3(i,j,k))
            end do
          end do
        end do
        
        w3(:,:,0)=0.d0
        w3(:,:,n-1)=0.d0
        
        end if
        
        return
        end subroutine do_sor_w
        
! **********************************************************************************************
        
        subroutine do_jacobi_w(w3,w3_old,RHS,viscosity,dx,dy,dz,dt,ex,ey,sx,sy,n)
        implicit none
        
        integer :: sx,sy,ex,ey,flg
        integer :: i,j,k,n,iteration_time
        
        double precision :: w3(sx-1:ex+1,sy-1:ey+1,0:n-1), w3_old(sx-1:ex+1,sy-1:ey+1,0:n-1), &
               RHS(sx-1:ex+1,sy-1:ey+1,0:n-1)
        double precision :: viscosity(sx-2:ex+2,sy-2:ey+2,0:n)
        double precision :: dx,dy,dz,dt,diag_val,residual,ax,ay,az, &
               mu_plushalf_x_val,mu_minushalf_x_val,mu_plushalf_y_val,mu_minushalf_y_val, &
               mu_plushalf_z_val,mu_minushalf_z_val
               
        double precision :: rho_wgrid
              
        ax=dt/(dx*dx)
        ay=dt/(dy*dy)
        az=dt/(dz*dz)
        
        do k=1,n-2
          do j=sy,ey
            do i=sx,ex
            
              mu_plushalf_x_val= (viscosity(i+1,j+1,k)+viscosity(i+1,j+1,k+1)+viscosity(i+2,j+1,k)+viscosity(i+2,j+1,k+1))/4.d0
              mu_minushalf_x_val=(viscosity(i+1,j+1,k)+viscosity(i+1,j+1,k+1)+viscosity(i  ,j+1,k)+viscosity(i  ,j+1,k+1))/4.d0
                            
              mu_plushalf_y_val= (viscosity(i+1,j+1,k)+viscosity(i+1,j+1,k+1)+viscosity(i+1,j+2,k)+viscosity(i+1,j+2,k+1))/4.d0
              mu_minushalf_y_val=(viscosity(i+1,j+1,k)+viscosity(i+1,j+1,k+1)+viscosity(i+1,j  ,k)+viscosity(i+1,j  ,k+1))/4.d0
              
              mu_plushalf_z_val=  viscosity(i+1,j+1,k+1)
              mu_minushalf_z_val= viscosity(i+1,j+1,k  )
              
              rho_wgrid=1.d0
              
              diag_val=1.d0+(ax/2.d0)*(1.d0/rho_wgrid)*(mu_plushalf_x_val+mu_minushalf_x_val)+&
                            (ay/2.d0)*(1.d0/rho_wgrid)*(mu_plushalf_y_val+mu_minushalf_y_val)+&
                            (az/2.d0)*(1.d0/rho_wgrid)*(mu_plushalf_z_val+mu_minushalf_z_val)

              residual=(1.d0/diag_val)*( &
                (ax/2.d0)*(1.d0/rho_wgrid)*(mu_plushalf_x_val*w3_old(i+1,j,k)+mu_minushalf_x_val*w3_old(i-1,j,k)) + &
                (ay/2.d0)*(1.d0/rho_wgrid)*(mu_plushalf_y_val*w3_old(i,j+1,k)+mu_minushalf_y_val*w3_old(i,j-1,k)) + &
                (az/2.d0)*(1.d0/rho_wgrid)*(mu_plushalf_z_val*w3_old(i,j,k+1)+mu_minushalf_z_val*w3_old(i,j,k-1)) + RHS(i,j,k))
              
              w3(i,j,k) = residual
            end do
          end do
        end do
        
        w3(:,:,0)=0.d0
        w3(:,:,n-1)=0.d0
        
        return
        end subroutine do_jacobi_w
        
! **********************************************************************************************

        subroutine scheduled_relaxation_jacobi(pres,RHS,dx,dy,dz,ex,ey,sx,sy,n,relax)
        implicit none
        
        integer :: sx,sy,ex,ey,flg
        integer :: i,j,k,n,iteration_time
        
        double precision :: pres(sx-1:ex+1,sy-1:ey+1,0:n), RHS(sx-1:ex+1,sy-1:ey+1,0:n), &
					residual(sx-1:ex+1,sy-1:ey+1,0:n)
        double precision :: dx,dy,dz,dt,diag_val,relax,ax,ay,az
        double precision :: rho_plusx,rho_minusx,rho_plusy,rho_minusy,rho_plusz,rho_minusz

              
        ax=1.d0/(dx*dx)
        ay=1.d0/(dy*dy)
        az=1.d0/(dz*dz)
                      
        do k=1,n-1
          do j=sy,ey
            do i=sx,ex
            
              rho_plusx= 1.d0
              rho_minusx=1.d0
              
              rho_plusy= 1.d0
              rho_minusy=1.d0
              
              rho_plusz= 1.d0
              rho_minusz=1.d0
              
              diag_val=ax*( (1.d0/rho_plusx)+(1.d0/rho_minusx)) + &
                  ay*( (1.d0/rho_plusy)+(1.d0/rho_minusy)) + &
                  az*( (1.d0/rho_plusz)+(1.d0/rho_minusz))

              residual(i,j,k) = (1.d0/diag_val) * ( &
                  ay*( (1.d0/rho_minusy)*pres(i,j-1,k) + (1.d0/rho_plusy)*pres(i,j+1,k))  + &
                  ax*( (1.d0/rho_minusx)*pres(i-1,j,k) + (1.d0/rho_plusx)*pres(i+1,j,k))  + &
                  az*( (1.d0/rho_minusz)*pres(i,j,k-1) + (1.d0/rho_plusz)*pres(i,j,k+1))  - RHS(i,j,k))
              !pres(i,j,k) = (1.d0-relax)*pres(i,j,k)+relax*residual
            end do
          end do
        end do

 		  pres = (1.d0-relax)*pres + relax*residual
        
        return
        end subroutine scheduled_relaxation_jacobi
        
! **********************************************************************************************

		  subroutine get_difference(ua,ub,ex,ey,ez,sx,sy,sz,diff)
		  implicit none

		  double precision :: diff,sum
		  integer :: ex,ey,ez,sx,sy,sz,i,j,k
		  double precision, dimension(sx-1:ex+1,sy-1:ey+1,sz:ez) :: ua, ub

		  sum = 0.0D0
		  do k=sz,ez
		    do j = sy,ey
			   do i = sx,ex
			     sum = sum + (ua(i,j,k)-ub(i,j,k))**2
			   end do
		    end do
		  end do
		  diff = sum

		  Return
		  End subroutine get_difference

! **********************************************************************************************
