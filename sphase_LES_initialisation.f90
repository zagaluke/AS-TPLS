!! @author Prashant Valluri, Lennon O Naraigh, Jean-Christophe Loiseau,
!! James Fannon, Peter Spelt
!! @copyright (c) 2013-2015, Prashant Valluri, Lennon O Naraigh, Jean-Christophe Loiseau,
!! James Fannon, Peter Spelt
!! This program is distributed under the BSD License See LICENSE.txt
!! for details.

      SUBROUTINE get_all_initial_LES(u,v,w,p,p_hydro,l,m,n,dx,dy,dz,Reynolds,dpdl,delta_u,delta_v,delta_w)
      IMPLICIT NONE

      integer :: l,m,n,i,j,k,k_max
      ! Declare the number of wavenumbers to sum over in the LES initialisation.
      parameter (k_max=5)
      double precision :: u(0:l,0:m,0:n-2),v(0:l,0:m,0:n-2),w(0:l,0:m,0:n-1), &
               p(0:l,0:m,0:n),p_hydro(0:l,0:m,0:n)	
      double precision :: dx,dy,dz,Reynolds,dpdl,Lx,Ly,Lz,pi,x_val,y_val,z_val
      double precision :: rho_minus,rho_plus,gz,Grav,rho_wgrid
      double precision :: p_oned(0:n)

      ! Pre allocate certain values which are used in the LES initialisation.
      double precision :: eps_val,k_x,k_y,k_z,f,f_z,k_dot,kappa,beta,u_max
      integer :: ck1,ck2,ck3,ip1,ip2,jp1,jp2
      double precision :: Ax(0:l,0:m,0:n),Ay(0:l,0:m,0:n),Az(0:l,0:m,0:n)
      double precision :: Az_y,Ay_z,Ax_z,Az_x,Ay_x,Ax_y,Ay_mid,Ax_mid,sum_Ax,sum_Ay,sum_Az
      double precision :: Az_plusy,Az_minusy,Ay_plusz,Ay_minusz,Ax_plusz,Ax_minusz,Az_plusx,Az_minusx, &
				Ay_plusx,Ay_minusx,Ax_plusy,Ax_minusy
      double precision :: S_rand,C_rand
      double precision :: Sx(1:l,1:m,1:n),Sy(1:l,1:m,1:n),Sz(1:l,1:m,1:n)
      double precision :: Cx(1:l,1:m,1:n),Cy(1:l,1:m,1:n),Cz(1:l,1:m,1:n)
      double precision :: delta_u(0:l,0:m,0:n-2),delta_v(0:l,0:m,0:n-2),delta_w(0:l,0:m,0:n-1)
      
      double precision :: max_u,max_v,max_w

! ****************************************************************************************      
      ! Parameters
      
      pi=4.D0*DATAN(1.D0)
      Lz=1.d0
      Lx=dx*(l-1)
      Ly=dy*(m-1)

! ****************************************************************************************

! LES Initialisation
! We proceed as outlined in the thesis.

! ****************************************************************************************      
      ! Define the relevant constants.
      eps_val=5.d0
      k_x=(2*pi)/Lx
      k_y=(2*pi)/Ly
      k_z=(2*pi)/Lz
      kappa=0.4d0
      beta=3.7
   
      ! We need some random numbers (between 0 and 1) to create our A vector.
      ! First initialise the random number generator to default state.      

      CALL random_seed() 

      do ck3=1,k_max
	do ck2=1,k_max
	  do ck1=1,k_max

		call RANDOM_NUMBER(S_rand)
		call RANDOM_NUMBER(C_rand)
		Sx(ck1,ck2,ck3)=S_rand
		Cx(ck1,ck2,ck3)=C_rand

		call RANDOM_NUMBER(S_rand)
		call RANDOM_NUMBER(C_rand)
		Sy(ck1,ck2,ck3)=S_rand
		Cy(ck1,ck2,ck3)=C_rand

		call RANDOM_NUMBER(S_rand)
		call RANDOM_NUMBER(C_rand)
		Sz(ck1,ck2,ck3)=S_rand
		Cz(ck1,ck2,ck3)=C_rand

	  end do
	end do
      end do


      ! Now we create the vector potentital A at every point on the pressure grid.
      ! We must also ensure that we implement the periodic boundary conditions
      ! in th x and y direction. The no slip boundary conditions in the z direction
      ! are taken care of by the scaling fucntion f.

      ! We loop over all space.
      do k=0,n
	do j=0,m
	  do i=0,l
		
           	x_val= i*dx-(dx/2.d0)
            	y_val= j*dy-(dy/2.d0)
		z_val= k*dz-(dz/2.d0)

                ! Dummy counters
		sum_Ax=0.d0
		sum_Ay=0.d0
		sum_Az=0.d0

		! Loop over our wavenumbers in each direction.
		do ck3=1,k_max
		  do ck2=1,k_max
	   	    do ck1=1,k_max
			
			! Dot product of k and x vectors
			k_dot=ck1*k_x*x_val+ck2*k_y*y_val+ck3*k_z*z_val

			sum_Ax=sum_Ax+(Sx(ck1,ck2,ck3))*sin(k_dot)+(Cx(ck1,ck2,ck3))*cos(k_dot)
			sum_Ay=sum_Ay+(Sy(ck1,ck2,ck3))*sin(k_dot)+(Cy(ck1,ck2,ck3))*cos(k_dot)
			sum_Az=sum_Az+(Sz(ck1,ck2,ck3))*sin(k_dot)+(Cz(ck1,ck2,ck3))*cos(k_dot)

	            end do
		  end do
		end do
		
		! Populate the vector field
		Ax(i,j,k)=sum_Ax
		Ay(i,j,k)=sum_Ay
		Az(i,j,k)=sum_Az

	  end do
	end do
      end do


      ! Now that the vector potential has been defined at all points,
      ! enforce the boundary conditions in the x and y direction.

      Ax(0,:,:)=Ax(l-1,:,:)
      Ax(l,:,:)=Ax(1,:,:)
      Ax(:,0,:)=Ax(:,m-1,:)
      Ax(:,m,:)=Ax(:,1,:)

      Ay(0,:,:)=Ay(l-1,:,:)
      Ay(l,:,:)=Ay(1,:,:)
      Ay(:,0,:)=Ay(:,m-1,:)
      Ay(:,m,:)=Ay(:,1,:)

      Az(0,:,:)=Az(l-1,:,:)
      Az(l,:,:)=Az(1,:,:)
      Az(:,0,:)=Az(:,m-1,:)
      Az(:,m,:)=Az(:,1,:)

      ! Now we need to compute delta_u,delta_v on their respective grids.
      ! This involves taking derivatives of the vector potential.

      ! Loop over the u/v grid.
      do k=0,n-2
	do j=0,m
	  do i=0,l

		! Need to take care at the boundary of our domain when taking
		! the derivative of the vector potential.

		if(i.eq.l)then
			! Use periodic boundary conditions.
			ip1=2
			ip2=3
		elseif(i.eq.(l-1))then
			ip1=i+1
			ip2=2
		else
			ip1=i+1
			ip2=i+2
		endif

		if(j.eq.m)then
			! Use periodic boundary conditions.
			jp1=2
			jp2=3
		elseif(j.eq.(m-1))then
			jp1=j+1
			jp2=2
		else
			jp1=j+1
			jp2=j+2
		endif

		! Compute the f scaling function.
         	z_val=k*dz+(dz/2.d0)
           	f=(0.5d0)+(0.5d0)*sin(((2.d0*pi*z_val)/Lz)-(pi/2.d0))
		f_z=(pi/Lz)*cos(((2.d0*pi*z_val)/Lz)-(pi/2.d0))

		! Compute the derivatives of Az,Ay on the u grid.
		Az_plusy =(0.5d0)*(Az(ip1,jp2,k+1)+Az(i,jp2,k+1))
		Az_minusy=(0.5d0)*(Az(ip1,j  ,k+1)+Az(i,j  ,k+1))
		Ay_plusz =(0.5d0)*(Ay(ip1,jp1,k+2)+Ay(i,jp1,k+2))
		Ay_minusz=(0.5d0)*(Ay(ip1,jp1,k  )+Ay(i,jp1,k  ))

		Az_y=(Az_plusy-Az_minusy)/(2.d0*dy)
		Ay_z=(Ay_plusz-Ay_minusz)/(2.d0*dz)
		
		! Value of Ay on the u grid (linear interpolation).
		Ay_mid=(0.5d0)*(Ay(ip1,jp1,k+1)+Ay(i,jp1,k+1))

		! Now do the same for the derivatives of Ax,Az on the v grid.
		Ax_plusz =(0.5d0)*(Ax(ip1,jp1,k+2)+Ax(ip1,j,k+2))
		Ax_minusz=(0.5d0)*(Ax(ip1,jp1,k  )+Ax(ip1,j,k  ))
		Az_plusx =(0.5d0)*(Az(ip2,jp1,k+1)+Az(ip2,j,k+1))
		Az_minusx=(0.5d0)*(Az(i  ,jp1,k+1)+Az(i  ,j,k+1))

		Ax_z=(Ax_plusz-Ax_minusz)/(2.d0*dz)
		Az_x=(Az_plusx-Az_minusx)/(2.d0*dx)
		
		! Value of Ax on the v grid (linear interpolation).
		Ax_mid=(0.5d0)*(Ax(ip1,jp1,k+1)+Ax(ip1,j,k+1))

		! Now compute delta u and delta v
		delta_u(i,j,k)=f*(Az_y-Ay_z)-(Ay_mid)*f_z
		delta_v(i,j,k)=f*(Ax_z-Az_x)+(Ax_mid)*f_z

	  end do
	end do
      end do
 
      ! Enforce the periodic boundary conditions in the x and y direction.
      delta_u(0,:,:)=delta_u(l-1,:,:)
      delta_u(l,:,:)=delta_u(1,:,:)
      delta_u(:,0,:)=delta_u(:,m-1,:)
      delta_u(:,m,:)=delta_u(:,1,:)

      delta_v(0,:,:)=delta_v(l-1,:,:)
      delta_v(l,:,:)=delta_v(1,:,:)
      delta_v(:,0,:)=delta_v(:,m-1,:)
      delta_v(:,m,:)=delta_v(:,1,:)

      ! We now do the same for the w grid.
      do k=0,n-1
	do j=0,m
	  do i=0,l
		
		! Need to take care at the boundary of our domain when taking
		! the derivative of the vector potential.

		if(i.eq.l)then
			! Use periodic boundary conditions.
			ip1=2
			ip2=3
		elseif(i.eq.(l-1))then
			ip1=i+1
			ip2=2
		else
			ip1=i+1
			ip2=i+2
		endif

		if(j.eq.m)then
			! Use periodic boundary conditions.
			jp1=2
			jp2=3
		elseif(j.eq.(m-1))then
			jp1=j+1
			jp2=2
		else
			jp1=j+1
			jp2=j+2
		endif
	
		! Compute the f scaling function.
		z_val=k*dz
		f=(0.5d0)+(0.5d0)*sin(((2.d0*pi*z_val)/Lz)-(pi/2.d0))

		! Compute the derivatives of Ax,Ay on the w grid.
		Ay_plusx =(0.5d0)*(Ay(ip2,jp1,k+1)+Ay(ip2,jp1,k))
		Ay_minusx=(0.5d0)*(Ay(i  ,jp1,k+1)+Ay(i  ,jp1,k))
		Ax_plusy =(0.5d0)*(Ax(ip1,jp2,k+1)+Ax(ip1,jp2,k))
		Ax_minusy=(0.5d0)*(Ax(ip1,j  ,k+1)+Ax(ip1,j  ,k))

		Ay_x=(Ay_plusx-Ay_minusx)/(2.d0*dx)
		Ax_y=(Ax_plusy-Ax_minusy)/(2.d0*dy)

		! Compute delta w.
		delta_w(i,j,k)=f*(Ay_x-Ax_y)

	  end do
	end do
      end do

      ! Enforce the periodic boundary conditions in x and y.
      delta_w(0,:,:)=delta_w(l-1,:,:)
      delta_w(l,:,:)=delta_w(1,:,:)
      delta_w(:,0,:)=delta_w(:,m-1,:) 
      delta_w(:,m,:)=delta_w(:,1,:)
 
      ! Enforce the no slip boundary conditions at z=0 and z=1.
      delta_w(:,:,0)=0.d0
      delta_w(:,:,n-1)=0.d0

     ! ****************************************************************************************      
     ! Check max values of u,v, and w.
     ! Taken from LON code les_initialisation.      
      
      max_u=0.d0
      max_v=0.d0
      max_w=0.d0
      
      do k=0,n-2
        do j=0,m
          do i=0,l
          
          if(abs(delta_u(i,j,k)).gt.max_u)max_u=abs(delta_u(i,j,k))
          if(abs(delta_v(i,j,k)).gt.max_v)max_v=abs(delta_v(i,j,k))
          
          end do
        end do
      end do
      
      do k=0,n-1
        do j=0,m
          do i=0,l
          
          if(abs(delta_w(i,j,k)).gt.max_w)max_w=abs(delta_w(i,j,k))
          
          end do
        end do
      end do
      
      ! Do this to avoid possible division by zero.
            
      if(max_u.eq.0.d0) max_u=1.d0
      if(max_v.eq.0.d0) max_v=1.d0
      if(max_w.eq.0.d0) max_w=1.d0

     !*******************************************************************************************

      ! Now the velocities in the x,y and z direction have been determined.
      ! We add on the base state profile to the u velocity.
      ! Determine u_max from semi-empirical relationship.
      u_max=(1.d0/kappa)*log(Reynolds)+beta

      ! Want the magnitude of the initial velocity to be "close" to u_max.
      ! Value of 0.85 used by LON.
      u_max=(0.85d0)*u_max
  
      do k=0,n-2
	do j=0,m
	  do i=0,l
		! Add the delta u term onto the base state.
		z_val=k*dz+(dz/2.d0)
		u(i,j,k)=(60)*z_val*(1.d0-z_val)+(eps_val/max_u)*delta_u(i,j,k)
	  end do
 	end do
      end do

      ! Initialise the v and w velocities.
      v=(eps_val/max_v)*delta_v
      w=(eps_val/max_w)*delta_w

      ! Create the initial pressure.
      
      do k=0,n-1
        p_oned(k)=0.d0
      end do
      
      do k=0,n
        do j=0,m
          do i=0,l
            x_val= i*dx-(dx/2.d0)
            
            p_hydro(i,j,k)=p_oned(k)
            ! Factor of dpdl used here?
            p(i,j,k)=(dpdl)*x_val+p_oned(k)
            
          end do
        end do
      end do  



      return
      end subroutine get_all_initial_LES
      



