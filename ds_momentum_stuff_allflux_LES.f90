!! @author Prashant Valluri, Lennon O Naraigh, Jean-Christophe Loiseau,
!! James Fannon, Peter Spelt
!! @copyright (c) 2013-2015, Prashant Valluri, Lennon O Naraigh, Jean-Christophe Loiseau,
!! James Fannon, Peter Spelt
!! This program is distributed under the BSD License See LICENSE.txt
!! for details.

!**********************************************************************************************
!
! Subroutine to compute the viscosity term at level n.  
! This subroutine uses flux-conservative differencing.
! Also, it is the master subroutine where the cell-averaged viscosities are computed.
! To compute these cell-averages, the augmented viscosity array is needed (MPI stuff).
!
!**********************************************************************************************
        
        subroutine get_diffusion_all(diff_u,diff_v,diff_w,viscosity,u2,v2,w2,sx,sy,ex,ey,n,dx,dy,dz)
        implicit none
        
        integer :: sx,sy,ex,ey,n
        integer :: i,j,k,ip1,im1,jp1,jm1
        
        double precision :: u2(sx-1:ex+1,sy-1:ey+1,0:n-2),diff_u(sx-1:ex+1,sy-1:ey+1,0:n-2)
        double precision :: v2(sx-1:ex+1,sy-1:ey+1,0:n-2),diff_v(sx-1:ex+1,sy-1:ey+1,0:n-2)
        double precision :: w2(sx-1:ex+1,sy-1:ey+1,0:n-1),diff_w(sx-1:ex+1,sy-1:ey+1,0:n-1)
        double precision :: viscosity(sx-2:ex+2,sy-2:ey+2,0:n)
        double precision :: dx,dy,dz
        double precision :: u_minusz,u_plusz,mu_duxE,mu_duxW,mu_duyN,mu_duyS,mu_duzI,mu_duzO, &
               v_minusz,v_plusz,mu_dvxE,mu_dvxW,mu_dvyN,mu_dvyS,mu_dvzI,mu_dvzO,              &
               mu_dwxE,mu_dwxW,mu_dwyN,mu_dwyS,mu_dwzI,mu_dwzO
        double precision :: mu_plushalf_x_val,mu_minushalf_x_val, &
               mu_plushalf_y_val,mu_minushalf_y_val,mu_plushalf_z_val,mu_minushalf_z_val

               
        double precision :: rho_ugrid,rho_vgrid,rho_wgrid
      
        diff_u=0.d0*u2
        
        do k=0,n-2
          do j=sy,ey
            do i=sx,ex
            
              ip1=i+1
              im1=i-1
              jp1=j+1
              jm1=j-1
            
              if(k.eq.0)then
                u_minusz=-2.d0*u2(i,j,0)+u2(i,j,1)/3.d0
              else
                u_minusz=u2(i,j,k-1)
              end if

              if(k.eq.(n-2))then
                u_plusz=-2.d0*u2(i,j,n-2)+u2(i,j,n-3)/3.d0
              else
                u_plusz=u2(i,j,k+1)
              end if
              
              mu_plushalf_x_val =viscosity(i+1,j+1,k+1)
              mu_minushalf_x_val=viscosity(i,  j+1,k+1)
              
              mu_plushalf_y_val =(viscosity(i,j+1,k+1)+viscosity(i+1,j+1,k+1)+viscosity(i,j+2,k+1)+viscosity(i+1,j+2,k+1))/4.d0
              mu_minushalf_y_val=(viscosity(i,j+1,k+1)+viscosity(i+1,j+1,k+1)+viscosity(i,j  ,k+1)+viscosity(i+1,j  ,k+1))/4.d0
              
              mu_plushalf_z_val =(viscosity(i,j+1,k+1)+viscosity(i+1,j+1,k+1)+viscosity(i,j+1,k+2)+viscosity(i+1,j+1,k+2))/4.d0
              mu_minushalf_z_val=(viscosity(i,j+1,k+1)+viscosity(i+1,j+1,k+1)+viscosity(i,j+1,k  )+viscosity(i+1,j+1,k  ))/4.d0
            
              mu_duxE= mu_plushalf_x_val*((u2(ip1,j,k)-u2(i,j,k))/dx)
              mu_duxW=mu_minushalf_x_val*((u2(i,j,k)-u2(im1,j,k))/dx)

              mu_duyN= mu_plushalf_y_val*((u2(i,jp1,k)-u2(i,j,k))/dy)
              mu_duyS=mu_minushalf_y_val*((u2(i,j,k)-u2(i,jm1,k))/dy)

              mu_duzO= mu_plushalf_z_val*((u_plusz-u2(i,j,k))/dz)
              mu_duzI=mu_minushalf_z_val*((u2(i,j,k)-u_minusz)/dz)
              
              rho_ugrid=1.d0
             
              diff_u(i,j,k)=(((mu_duxE-mu_duxW)/dx)+((mu_duyN-mu_duyS)/dy)+((mu_duzO-mu_duzI)/dz))/rho_ugrid
            
            end do
          end do
        end do
              
        diff_v=0.d0*v2
        
        do k=0,n-2
          do j=sy,ey
            do i=sx,ex
            
              ip1=i+1
              im1=i-1
              jp1=j+1
              jm1=j-1
            
              if(k.eq.0)then
                v_minusz=-2.d0*v2(i,j,0)+v2(i,j,1)/3.d0
              else
                v_minusz=v2(i,j,k-1)
              end if

              if(k.eq.(n-2))then
                v_plusz=-2.d0*v2(i,j,n-2)+v2(i,j,n-3)/3.d0
              else
                v_plusz=v2(i,j,k+1)
              end if
              
              mu_plushalf_x_val= (viscosity(i+1,j+1,k+1)+viscosity(i+1,j,k+1)+viscosity(i+2,j+1,k+1)+viscosity(i+2,j,k+1))/4.d0
              mu_minushalf_x_val=(viscosity(i+1,j+1,k+1)+viscosity(i+1,j,k+1)+viscosity(i,  j+1,k+1)+viscosity(i,  j,k+1))/4.d0
                            
              mu_plushalf_y_val= viscosity(i+1,j+1,k+1)
              mu_minushalf_y_val=viscosity(i+1,j,  k+1)
              
              mu_plushalf_z_val= (viscosity(i+1,j+1,k+1)+viscosity(i+1,j,k+1)+viscosity(i+1,j+1,k+2)+viscosity(i+1,j,k+2))/4.d0
              mu_minushalf_z_val=(viscosity(i+1,j+1,k+1)+viscosity(i+1,j,k+1)+viscosity(i+1,j+1,k  )+viscosity(i+1,j,k  ))/4.d0

              mu_dvxE= mu_plushalf_x_val*((v2(ip1,j,k)-v2(i,j,k))/dx)
              mu_dvxW=mu_minushalf_x_val*((v2(i,j,k)-v2(im1,j,k))/dx)

              mu_dvyN= mu_plushalf_y_val*((v2(i,jp1,k)-v2(i,j,k))/dy)
              mu_dvyS=mu_minushalf_y_val*((v2(i,j,k)-v2(i,jm1,k))/dy)

              mu_dvzO= mu_plushalf_z_val*((v_plusz-v2(i,j,k))/dz)
              mu_dvzI=mu_minushalf_z_val*((v2(i,j,k)-v_minusz)/dz)
              
              rho_vgrid=1.d0

              diff_v(i,j,k)=(((mu_dvxE-mu_dvxW)/dx)+((mu_dvyN-mu_dvyS)/dy)+((mu_dvzO-mu_dvzI)/dz))/rho_vgrid
              
            end do
          end do
        end do
        
        diff_w=0.d0*w2
        do k=1,n-2
          do j=sy,ey
            do i=sx,ex
            
              ip1=i+1
              im1=i-1
              jp1=j+1
              jm1=j-1
            
              mu_plushalf_x_val= (viscosity(i+1,j+1,k)+viscosity(i+1,j+1,k+1)+viscosity(i+2,j+1,k)+viscosity(i+2,j+1,k+1))/4.d0
              mu_minushalf_x_val=(viscosity(i+1,j+1,k)+viscosity(i+1,j+1,k+1)+viscosity(i  ,j+1,k)+viscosity(i  ,j+1,k+1))/4.d0
                            
              mu_plushalf_y_val= (viscosity(i+1,j+1,k)+viscosity(i+1,j+1,k+1)+viscosity(i+1,j+2,k)+viscosity(i+1,j+2,k+1))/4.d0
              mu_minushalf_y_val=(viscosity(i+1,j+1,k)+viscosity(i+1,j+1,k+1)+viscosity(i+1,j  ,k)+viscosity(i+1,j  ,k+1))/4.d0
              
              mu_plushalf_z_val= viscosity(i+1,j+1,k+1)
              mu_minushalf_z_val= viscosity(i+1,j+1,k  )
                                
              mu_dwxE= mu_plushalf_x_val*((w2(ip1,j,k)-w2(i,  j,k))/dx)
              mu_dwxW=mu_minushalf_x_val*((w2(i,  j,k)-w2(im1,j,k))/dx)
     
              mu_dwyN= mu_plushalf_y_val*((w2(i,jp1,k)-w2(i,j,  k))/dy)
              mu_dwyS=mu_minushalf_y_val*((w2(i,j,  k)-w2(i,jm1,k))/dy)
     
              mu_dwzO= mu_plushalf_z_val*((w2(i,j,k+1)-w2(i,j,k  ))/dz)
              mu_dwzI=mu_minushalf_z_val*((w2(i,j,k  )-w2(i,j,k-1))/dz)

              
              rho_wgrid=1.d0

              diff_w(i,j,k)=(((mu_dwxE-mu_dwxW)/dx)+((mu_dwyN-mu_dwyS)/dy)+((mu_dwzO-mu_dwzI)/dz))/rho_wgrid

            end do
          end do
        end do
        
        return
        end subroutine get_diffusion_all
        
!**********************************************************************************************
!
! Subroutine to compute the convective term at level n.  
! There are two parts to the convective term: the ordinary convective term, and a part involving
! gradients of the viscosity (the transposed part of the stress tensor).
! The differencing is fully flux-conservative.
! To compute the cell-averaged viscosities, the augmented viscosity array is needed (MPI stuff).
!
!**********************************************************************************************


       subroutine get_conv_all(conv_u2,conv_v2,conv_w2,viscosity_aug,u2,v2,w2,sx,sy,ex,ey,n,dx,dy,dz)
       implicit none
       
       integer :: n,i,j,k,im1,jm1,jp1
       integer :: sx,ex,sy,ey
       double precision :: dx,dy,dz,dt
       double precision :: conv_u2(sx-1:ex+1,sy-1:ey+1,0:n-2),u2(sx-1:ex+1,sy-1:ey+1,0:n-2)
       double precision :: conv_v2(sx-1:ex+1,sy-1:ey+1,0:n-2),v2(sx-1:ex+1,sy-1:ey+1,0:n-2)
       double precision :: conv_w2(sx-1:ex+1,sy-1:ey+1,0:n-1),w2(sx-1:ex+1,sy-1:ey+1,0:n-1)
       double precision :: viscosity_aug(sx-2:ex+2,sy-2:ey+2,0:n)
       double precision :: ub,vb,tempu,tempv,tempw,temp_conv1,temp_conv2
       double precision :: dudx,dudy,dudz,dvdx,dvdy,dvdz,dwdx,dwdy,dwdz,Xconv,Yconv,Zconv
       double precision :: mu_dudxE,mu_dudxW,mu_dvdxN,mu_dvdxS,mu_dwdxO,mu_dwdxI, &
               mu_dudyE,mu_dudyW,mu_dvdyN,mu_dvdyS,mu_dwdyO,mu_dwdyI, &
               mu_dudzE,mu_dudzW,mu_dvdzN,mu_dvdzS,mu_dwdzO,mu_dwdzI
       double precision :: mu_plushalf_x, mu_minushalf_x, mu_plushalf_y, &
               mu_minushalf_y, mu_plushalf_z, mu_minushalf_z
         
       double precision :: rho_ugrid,rho_vgrid,rho_wgrid
         
! ******************************  X direction 
       conv_u2=0.d0*u2
       
       do k=0, n-2
         do j=sy, ey
           do i=sx, ex
           
           im1=i-1
           
           tempv=(v2(im1,j,k)+v2(i,j,k)+v2(im1,j+1,k)+v2(i,j+1,k))/4.d0
           tempw=(w2(im1,j,k)+w2(i,j,k)+w2(im1,j,k+1)+w2(i,j,k+1))/4.d0

           dudx=0.5d0*(u2(i+1,j,k)-u2(im1,j,k))/dx           
           Xconv=dudx*u2(i,j,k)
           
           ub=u2(i,j+1,k)
           vb=u2(i,j-1,k)
           dudy=0.5d0*(ub-vb)/dy
           
           Yconv=dudy*tempv
           
           if(k==0)then
              dudz= (u2(i,j,k)+u2(i,j,k+1))/(2.d0*dz)
           elseif(k==n-2)then
              dudz=-(u2(i,j,k)+u2(i,j,k-1))/(2.d0*dz)
           else
              ub=u2(i,j,k+1)
              vb=u2(i,j,k-1)
              dudz=0.5d0*(ub-vb)/dz
           endif
           
           Zconv=dudz*tempw
           temp_conv1=Xconv+Yconv+Zconv
           
           mu_plushalf_x=viscosity_aug(i+1,j+1,k+1)
           mu_minushalf_x=viscosity_aug(i,j+1,k+1)
           
           mu_plushalf_y = (viscosity_aug(i+1,j+2,k+1)+ viscosity_aug(i,j+2,k+1)+ viscosity_aug(i+1,j+1,k+1)+ &
               viscosity_aug(i,j+1,k+1))/4.d0
           mu_minushalf_y= (viscosity_aug(i+1,j,k+1)  + viscosity_aug(i,j,k+1)  + viscosity_aug(i+1,j+1,k+1)+ &
               viscosity_aug(i,j+1,k+1))/4.d0
           
           mu_plushalf_z = (viscosity_aug(i+1,j+1,k+2)+ viscosity_aug(i,j+1,k+2)+ viscosity_aug(i+1,j+1,k+1)+ &
               viscosity_aug(i,j+1,k+1))/4.d0
           mu_minushalf_z= (viscosity_aug(i+1,j+1,k)  + viscosity_aug(i,j+1,k)  + viscosity_aug(i+1,j+1,k+1)+ &
               viscosity_aug(i,j+1,k+1))/4.d0
           
           mu_dudxE=mu_plushalf_x* (u2(i+1,j,k)-u2(i,  j,k))/dx
           mu_dudxW=mu_minushalf_x*(u2(i,  j,k)-u2(i-1,j,k))/dx
           
           mu_dvdxN=mu_plushalf_y* (v2(i,j+1,k)-v2(i-1,j+1,k))/dx
           mu_dvdxS=mu_minushalf_y*(v2(i,j,  k)-v2(i-1,j,  k))/dx
           
           mu_dwdxO=mu_plushalf_z* (w2(i,j,k+1)-w2(i-1,j,k+1))/dx
           mu_dwdxI=mu_minushalf_z*(w2(i,j,k  )-w2(i-1,j,k  ))/dx
           
           rho_ugrid=1.d0
           
           temp_conv2=(((mu_dudxE-mu_dudxW)/dx)+((mu_dvdxN-mu_dvdxS)/dy)+((mu_dwdxO-mu_dwdxI)/dz))/rho_ugrid

           conv_u2(i,j,k)=temp_conv1-temp_conv2
           enddo
         enddo
       enddo

! ******************************  Y direction 

       conv_v2=0.d0*v2
       
       do k=0, n-2
         do j=sy, ey
           do i=sx, ex

           jm1=j-1
           tempu=(u2(i,jm1,k)+u2(i,j,k)+u2(i+1,jm1,k)+u2(i+1,j,k))/4.d0
           tempw=(w2(i,jm1,k)+w2(i,j,k)+w2(i,jm1,k+1)+w2(i,j,k+1))/4.d0

           dvdy=0.5d0*(v2(i,j+1,k)-v2(i,jm1,k))/dy
           Yconv=dvdy*v2(i,j,k)

           ub=v2(i+1,j,k)
           vb=v2(i-1,j,k)
           dvdx=0.5d0*(ub-vb)/dx
           Xconv=dvdx*tempu
           
           if(k==0)then
             dvdz=(v2(i,j,k)+v2(i,j,k+1))/(2.d0*dz)
           elseif(k==n-2)then
             dvdz=-(v2(i,j,k)+v2(i,j,k-1))/(2.d0*dz)
           else
             dvdz=0.5d0*(v2(i,j,k+1)-v2(i,j,k-1))/dz
           endif
           
           Zconv=dvdz*tempw
           temp_conv1=Xconv+Yconv+Zconv
           
           mu_plushalf_x = (  viscosity_aug(i+2,j+1,k+1)+   viscosity_aug(i+2,j,k+1) +  viscosity_aug(i+1,j+1,k+1)+  &
               viscosity_aug(i+1,j,k+1))/4.d0
           mu_minushalf_x = (  viscosity_aug(i,j+1,k+1)  +   viscosity_aug(i,j,k+1)   +  viscosity_aug(i+1,j+1,k+1)+  &
               viscosity_aug(i+1,j,k+1))/4.d0
               
           mu_plushalf_y= viscosity_aug(i+1,j+1,k+1)
           mu_minushalf_y=viscosity_aug(i+1,j,  k+1)
           
           mu_plushalf_z = (viscosity_aug(i+1,j+1,k+2) +viscosity_aug(i+1,j,k+2)  +  viscosity_aug(i+1,j+1,k+1)+  &
               viscosity_aug(i+1,j,k+1))/4.d0
           mu_minushalf_z = (viscosity_aug(i+1,j+1,k)   +viscosity_aug(i+1,j,k)    +  viscosity_aug(i+1,j+1,k+1)+  &
               viscosity_aug(i+1,j,k+1))/4.d0
           
           mu_dudyE=mu_plushalf_x* (u2(i+1,j,k)-u2(i+1,j-1,k))/dy
           mu_dudyW=mu_minushalf_x*(u2(i,  j,k)-u2(i,  j-1,k))/dy
           
           mu_dvdyN=mu_plushalf_y* (v2(i,j+1,k)-v2(i,j,  k))/dy
           mu_dvdyS=mu_minushalf_y*(v2(i,j,  k)-v2(i,j-1,k))/dy
           
           mu_dwdyO=mu_plushalf_z* (w2(i,j,k+1)-w2(i,j-1,k+1))/dy
           mu_dwdyI=mu_minushalf_z*(w2(i,j,k  )-w2(i,j-1,k  ))/dy
           
           rho_vgrid=1.d0
           
           temp_conv2=(((mu_dudyE-mu_dudyW)/dx)+((mu_dvdyN-mu_dvdyS)/dy)+((mu_dwdyO-mu_dwdyI)/dz))/rho_vgrid
           
           conv_v2(i,j,k)=temp_conv1-temp_conv2
           enddo
         enddo
       enddo

! ******************************  Z direction  

       conv_w2=0.d0*w2
       
       do k=1, n-2
         do j=sy, ey
           do i=sx, ex
           tempu=(u2(i,j,k-1)+u2(i,j,k)+u2(i+1,j,k-1)+u2(i+1,j,k))/4.d0
           tempv=(v2(i,j,k-1)+v2(i,j,k)+v2(i,j+1,k-1)+v2(i,j+1,k))/4.d0

           ub = w2(i,j,k+1)
           vb = w2(i,j,k-1)
           dwdz=0.5*(ub-vb)/dz
           Zconv=dwdz*w2(i,j,k)

           ub=w2(i+1,j,k)
           vb=w2(i-1,j,k)
           dwdx=0.5d0*(ub-vb)/dx
           Xconv=dwdx*tempu
           
           ub=w2(i,j+1,k)
           vb=w2(i,j-1,k)
           dwdy=0.5d0*(ub-vb)/dy
           Yconv=dwdy*tempv
           
           temp_conv1=Xconv+Yconv+Zconv
           
           mu_plushalf_x=(viscosity_aug(i+2,j+1,k+1)+  viscosity_aug(i+2,j+1,k) +  viscosity_aug(i+1,j+1,k+1)+  &
               viscosity_aug(i+1,j+1,k))/4.d0
           mu_minushalf_x=(viscosity_aug(i,j+1,k+1)  +  viscosity_aug(i,j+1,k)   +  viscosity_aug(i+1,j+1,k+1)+  &
               viscosity_aug(i+1,j+1,k))/4.d0
               
           mu_plushalf_y= (viscosity_aug(i+1,j+2,k+1)+  viscosity_aug(i+1,j+2,k)+  viscosity_aug(i+1,j+1,k+1)+  &
               viscosity_aug(i+1,j+1,k))/4.d0
           mu_minushalf_y= (viscosity_aug(i+1,j,k+1)  +  viscosity_aug(i+1,j,k)  +  viscosity_aug(i+1,j+1,k+1)+  &
               viscosity_aug(i+1,j+1,k))/4.d0
               
           mu_plushalf_z=viscosity_aug(i+1,j+1,k+1)
           mu_minushalf_z=viscosity_aug(i+1,j+1,k)
           
           mu_dudzE=mu_plushalf_x* (u2(i+1,j,k)-u2(i+1,j,k-1))/dz
           mu_dudzW=mu_minushalf_x*(u2(i,  j,k)-u2(i,  j,k-1))/dz
           
           mu_dvdzN=mu_plushalf_y* (v2(i,j+1,k)-v2(i,j+1,k-1))/dz
           mu_dvdzS=mu_minushalf_y*(v2(i,j,  k)-v2(i,j  ,k-1))/dz
           
           mu_dwdzO=mu_plushalf_z* (w2(i,j,k+1)-w2(i,j,k  ))/dz
           mu_dwdzI=mu_minushalf_z*(w2(i,j,k  )-w2(i,j,k-1))/dz
           
           rho_wgrid=1.d0
           
           temp_conv2=(((mu_dudzE-mu_dudzW)/dx)+((mu_dvdzN-mu_dvdzS)/dy)+((mu_dwdzO-mu_dwdzI)/dz))/rho_wgrid
           
           conv_w2(i,j,k)=temp_conv1-temp_conv2
           enddo
         enddo
       enddo

       return
       end subroutine get_conv_all

!**********************************************************************************************
!start luke code       
!subroutine to get cell-centred velocities local to an mpi process

subroutine get_cell_cent_vel(u,v,w,u_c,v_c,w_c,ex,ey,sx,sy,n)
implicit none

integer :: i,j,k,ex,ey,sx,sy,n

double precision :: u(sx-1:ex+1,sy-1:ey+1,0:n-2), u_c(sx-1:ex+1,sy-1:ey+1,0:n)
double precision :: v(sx-1:ex+1,sy-1:ey+1,0:n-2), v_c(sx-1:ex+1,sy-1:ey+1,0:n)
double precision :: w(sx-1:ex+1,sy-1:ey+1,0:n-1), w_c(sx-1:ex+1,sy-1:ey+1,0:n)

!loop over grid and assign cell centred velocities
do k=1,n-1
	do j=sy,ey
		do i=sx,ex

			!averages assigned based on velocity grid
			u_c(i,j,k)=0.5d0*(u(i,j,k-1)+u(i-1,j,k-1))
			v_c(i,j,k)=0.5d0*(v(i,j,k-1)+v(i,j-1,k-1))
			w_c(i,j,k)=0.5d0*(w(i,j,k)+w(i,j,k-1))	

			!cell centred velocities appear undefined at k=0 and k=n so set them equal to values at k=1 and k=n-1
			if(k.eq.1)then
	        		u_c(i,j,k-1)=u_c(i,j,k)
				v_c(i,j,k-1)=v_c(i,j,k)
				w_c(i,j,k-1)=w_c(i,j,k)
	    		elseif(k.eq.n-1)then
				u_c(i,j,k+1)=u_c(i,j,k)
				v_c(i,j,k+1)=v_c(i,j,k)
				w_c(i,j,k+1)=w_c(i,j,k)
	    		endif
		enddo
	enddo
enddo			
return
end subroutine get_cell_cent_vel 

!subroutine to calculate fields necessary for dynamic smagorinsky co-efficient

subroutine get_dynam_smag_fields(s11,s22,s33,s12,s13,s23,u1u1,u2u2,u3u3,u1u2,u1u3,u2u3,abs_s_s11, & 
	   abs_s_s22,abs_s_s33,abs_s_s12,abs_s_s13,abs_s_s23,abs_s,u2,v2,w2,u_c,v_c,w_c,ex,ey,sx,sy, & 
	   n,dx,dy,dz)
implicit none

integer :: sx,ex,sy,ey,n,i,j,k

double precision :: s11(sx-1:ex+1,sy-1:ey+1,0:n)
double precision :: s22(sx-1:ex+1,sy-1:ey+1,0:n)
double precision :: s33(sx-1:ex+1,sy-1:ey+1,0:n)
double precision :: s12(sx-1:ex+1,sy-1:ey+1,0:n)
double precision :: s13(sx-1:ex+1,sy-1:ey+1,0:n)
double precision :: s23(sx-1:ex+1,sy-1:ey+1,0:n)

double precision :: u1u1(sx-1:ex+1,sy-1:ey+1,0:n)
double precision :: u2u2(sx-1:ex+1,sy-1:ey+1,0:n)
double precision :: u3u3(sx-1:ex+1,sy-1:ey+1,0:n)
double precision :: u1u2(sx-1:ex+1,sy-1:ey+1,0:n)
double precision :: u1u3(sx-1:ex+1,sy-1:ey+1,0:n)
double precision :: u2u3(sx-1:ex+1,sy-1:ey+1,0:n)

double precision :: abs_s_s11(sx-1:ex+1,sy-1:ey+1,0:n) 
double precision :: abs_s_s22(sx-1:ex+1,sy-1:ey+1,0:n)
double precision :: abs_s_s33(sx-1:ex+1,sy-1:ey+1,0:n)
double precision :: abs_s_s12(sx-1:ex+1,sy-1:ey+1,0:n)
double precision :: abs_s_s13(sx-1:ex+1,sy-1:ey+1,0:n)
double precision :: abs_s_s23(sx-1:ex+1,sy-1:ey+1,0:n)

double precision :: abs_s(sx-1:ex+1,sy-1:ey+1,0:n)

double precision :: u2(sx-2:ex+2,sy-2:ey+2,0:n-2) 
double precision :: v2(sx-2:ex+2,sy-2:ey+2,0:n-2) 
double precision :: w2(sx-2:ex+2,sy-2:ey+2,0:n-1)
double precision :: val1,val2,val3,val4,val5,val6

double precision :: u_c(sx-1:ex+1,sy-1:ey+1,0:n)
double precision :: v_c(sx-1:ex+1,sy-1:ey+1,0:n)
double precision :: w_c(sx-1:ex+1,sy-1:ey+1,0:n)

double precision :: u_i_minusz,u_im1_minusz,u_i_plusz,u_im1_plusz,v_j_minusz, &
		  v_jm1_minusz,v_j_plusz,v_jm1_plusz,u_star_plusy,u_star_minusy, &
		  u_star_plusz,u_star_minusz,v_star_plusx,v_star_minusx, &
		  v_star_plusz,v_star_minusz,w_star_plusx,w_star_minusx, &
		  w_star_plusy,w_star_minusy
double precision :: du_dx,du_dy,du_dz,dv_dx,dv_dy,dv_dz,dw_dx,dw_dy,dw_dz
double precision :: dx,dy,dz

       do k=1,n-1
         do j=sy,ey
           do i=sx,ex
		   ! Compute the various derivatives used in the rate of strain tensor.
	           ! Hence we must use the augmented velocity fields as inputs.
		   if(k.eq.1)then
		      u_i_minusz=-2.d0*u2(i,j-1,0)+u2(i,j-1,1)/3.d0
                      u_im1_minusz=-2.d0*u2(i-1,j-1,0)+u2(i-1,j-1,1)/3.d0
		      u_i_plusz=u2(i,j-1,1)
		      u_im1_plusz=u2(i-1,j-1,1)
		      v_j_minusz=-2.d0*v2(i-1,j,0)+v2(i-1,j,1)/3.d0
		      v_jm1_minusz=-2.d0*v2(i-1,j-1,0)+v2(i-1,j-1,1)/3.d0
		      v_j_plusz=v2(i-1,j,1)
		      v_jm1_plusz=v2(i-1,j-1,1)	
		   elseif(k.eq.n-1)then
		      u_i_minusz=u2(i,j-1,n-3)
		      u_im1_minusz=u2(i-1,j-1,n-3)
		      u_i_plusz=-2.d0*u2(i,j-1,n-2)+u2(i,j-1,n-2)/3.d0
		      u_im1_plusz=-2.d0*u2(i-1,j-1,n-2)+u2(i-1,j-1,n-3)/3.d0
		      v_j_minusz=v2(i-1,j,n-3)
		      v_jm1_minusz=v2(i-1,j-1,n-3)
		      v_j_plusz=-2.d0*v2(i-1,j,n-2)+v2(i-1,j,n-3)/3.d0
		      v_jm1_plusz=-2.d0*v2(i-1,j-1,n-2)+v2(i-1,j-1,n-3)/3.d0
		   else
                      u_i_minusz=u2(i,j-1,k-2)
		      u_im1_minusz=u2(i-1,j-1,k-2)
		      u_i_plusz=u2(i,j-1,k)
		      u_im1_plusz=u2(i-1,j-1,k)
		      v_j_minusz=v2(i-1,j,k-2)
		      v_jm1_minusz=v2(i-1,j-1,k-2)
		      v_j_plusz=v2(i-1,j,k)
		      v_jm1_plusz=v2(i-1,j-1,k)
		   end if
			
		   u_star_plusy=(0.5d0)*(u2(i,j,k-1)+u2(i-1,j,k-1))
	           u_star_minusy=(0.5d0)*(u2(i,j-2,k-1)+u2(i-1,j-2,k-1))
		   u_star_plusz=(0.5d0)*(u_i_plusz+u_im1_plusz)
		   u_star_minusz=(0.5d0)*(u_i_minusz+u_im1_minusz)
		   
		   v_star_plusx=(0.5d0)*(v2(i,j,k-1)+v2(i,j-1,k-1))
		   v_star_minusx=(0.5d0)*(v2(i-2,j,k-1)+v2(i-2,j-1,k-1))
		   v_star_plusz=(0.5d0)*(v_j_plusz+v_jm1_plusz)
		   v_star_minusz=(0.5d0)*(v_j_minusz+v_jm1_minusz)

		   ! Note that there are no problems in the z direction for
 		   ! the w grid. Thus we can compute the derivatives as follows.
		   w_star_plusx=(0.5d0)*(w2(i,j-1,k)+w2(i,j-1,k-1))
		   w_star_minusx=(0.5d0)*(w2(i-2,j-1,k)+w2(i-2,j-1,k-1))
		   w_star_plusy=(0.5d0)*(w2(i-1,j,k)+w2(i-1,j,k-1))
		   w_star_minusy=(0.5d0)*(w2(i-1,j-2,k)+w2(i-1,j-2,k-1))

		   du_dx=(u2(i,j-1,k-1)-u2(i-1,j-1,k-1))/dx
     		   du_dy=(u_star_plusy-u_star_minusy)/(2.d0*dy)
		   du_dz=(u_star_plusz-u_star_minusz)/(2.d0*dz)

		   dv_dx=(v_star_plusx-v_star_minusx)/(2.d0*dx)
		   dv_dy=(v2(i-1,j,k-1)-v2(i-1,j-1,k-1))/dy
		   dv_dz=(v_star_plusz-v_star_minusz)/(2.d0*dz)

		   dw_dx=(w_star_plusx-w_star_minusx)/(2.d0*dx)
		   dw_dy=(w_star_plusy-w_star_minusy)/(2.d0*dy)
                   dw_dz=(w2(i-1,j-1,k)-w2(i-1,j-1,k-1))/(dz)


		   !assign individual terms in rate-of-strain tensor	
		   s11(i,j,k)=du_dx
		   s22(i,j,k)=dv_dy
		   s33(i,j,k)=dw_dz	
		   s12(i,j,k)=0.5d0*(du_dy+dv_dx)
		   s13(i,j,k)=0.5d0*(du_dz+dw_dx) 
		   s23(i,j,k)=0.5d0*(dv_dz+dw_dy)
		
		   !assign terms in uiuj tensor		
                   u1u1(i,j,k)=u_c(i,j,k)*u_c(i,j,k)
		   u2u2(i,j,k)=v_c(i,j,k)*v_c(i,j,k)
		   u3u3(i,j,k)=w_c(i,j,k)*w_c(i,j,k)
		   u1u2(i,j,k)=u_c(i,j,k)*v_c(i,j,k)		 			
		   u1u3(i,j,k)=u_c(i,j,k)*w_c(i,j,k)	
		   u2u3(i,j,k)=v_c(i,j,k)*w_c(i,j,k)

		   val1=2.d0*((du_dx)**2)
		   val2=0.5d0*((du_dy)**2 + 2.d0*(du_dy)*(dv_dx) + (dv_dx)**2)
   		   val3=0.5d0*((du_dz)**2 + 2.d0*(du_dz)*(dw_dx) + (dw_dx)**2)
		   val4=2.d0*((dv_dy)**2)
		   val5=0.5d0*((dv_dz)**2 + 2.d0*(dv_dz)*(dw_dy) + (dw_dy)**2)
		   val6=2.d0*((dw_dz)**2)

		   abs_s(i,j,k)=sqrt(val1 + 2.d0*val2 + 2.d0*val3 + val4 + 2.d0*val5 + val6)

		   abs_s_s11(i,j,k)=abs_s(i,j,k)*s11(i,j,k)	
		   abs_s_s22(i,j,k)=abs_s(i,j,k)*s22(i,j,k)
		   abs_s_s33(i,j,k)=abs_s(i,j,k)*s33(i,j,k)
		   abs_s_s12(i,j,k)=abs_s(i,j,k)*s12(i,j,k)
		   abs_s_s13(i,j,k)=abs_s(i,j,k)*s13(i,j,k)
		   abs_s_s23(i,j,k)=abs_s(i,j,k)*s23(i,j,k)

		   !derivatives undefined at boundaries, so identify points k=1(n-1) with k=0(n)	
		   if(k.eq.1)then
		      s11(i,j,k-1)=s11(i,j,k)
		      s22(i,j,k-1)=s22(i,j,k)
		      s33(i,j,k-1)=s33(i,j,k)
		      s12(i,j,k-1)=s12(i,j,k)
		      s13(i,j,k-1)=s13(i,j,k)
		      s23(i,j,k-1)=s23(i,j,k)

		      u1u1(i,j,k-1)=u1u1(i,j,k)
		      u2u2(i,j,k-1)=u2u2(i,j,k)
		      u3u3(i,j,k-1)=u3u3(i,j,k)
		      u1u2(i,j,k-1)=u1u2(i,j,k)
		      u1u3(i,j,k-1)=u1u3(i,j,k)
		      u2u3(i,j,k-1)=u2u3(i,j,k)

	              abs_s(i,j,k-1)=abs_s(i,j,k)

		      abs_s_s11(i,j,k-1)=abs_s_s11(i,j,k)
		      abs_s_s22(i,j,k-1)=abs_s_s22(i,j,k) 
		      abs_s_s33(i,j,k-1)=abs_s_s33(i,j,k) 
		      abs_s_s12(i,j,k-1)=abs_s_s12(i,j,k) 
		      abs_s_s13(i,j,k-1)=abs_s_s13(i,j,k) 
		      abs_s_s23(i,j,k-1)=abs_s_s23(i,j,k) 
     	
		   elseif(k.eq.n-1)then
		      s11(i,j,k+1)=s11(i,j,k)
		      s22(i,j,k+1)=s22(i,j,k)
		      s33(i,j,k+1)=s33(i,j,k)
		      s12(i,j,k+1)=s12(i,j,k)
		      s13(i,j,k+1)=s13(i,j,k)
		      s23(i,j,k+1)=s23(i,j,k)

		      u1u1(i,j,k+1)=u1u1(i,j,k)
		      u2u2(i,j,k+1)=u2u2(i,j,k)
		      u3u3(i,j,k+1)=u3u3(i,j,k)
		      u1u2(i,j,k+1)=u1u2(i,j,k)
		      u1u3(i,j,k+1)=u1u3(i,j,k)
		      u2u3(i,j,k+1)=u2u3(i,j,k)

	              abs_s(i,j,k+1)=abs_s(i,j,k)

		      abs_s_s11(i,j,k+1)=abs_s_s11(i,j,k)
		      abs_s_s22(i,j,k+1)=abs_s_s22(i,j,k) 
		      abs_s_s33(i,j,k+1)=abs_s_s33(i,j,k) 
		      abs_s_s12(i,j,k+1)=abs_s_s12(i,j,k) 
		      abs_s_s13(i,j,k+1)=abs_s_s13(i,j,k) 
		      abs_s_s23(i,j,k+1)=abs_s_s23(i,j,k) 
		   endif

		   

           enddo
	 enddo
       enddo
return
end subroutine get_dynam_smag_fields 		

!subroutine to doubly filter fields through nearest-neighbour averaging

subroutine filter_field(u,u_f,ex,ey,sx,sy,n)
implicit none

integer :: i,j,k,ex,ey,sx,sy,n
double precision :: u(sx-1:ex+1,sy-1:ey+1,0:n)
double precision :: u_f(sx-1:ex+1,sy-1:ey+1,0:n) 

do k=1,n-1
   do j=sy,ey
      do i=sx,ex
	    !simple average over adjacent cells
	    u_f(i,j,k)=(u(i+1,j,k)+u(i-1,j,k)+u(i,j+1,k)+u(i,j-1,k)+u(i,j,k+1)+u(i,j,k-1))/6.d0
	    
	    !Averaging slightly different at z boundary due to no-slip BCs	 
	    if(k.eq.1)then
	        u_f(i,j,k-1)=(u(i+1,j,k-1)+u(i-1,j,k-1)+u(i,j+1,k-1)+u(i,j-1,k-1)+u(i,j,k))/5.d0
	    elseif(k.eq.n-1)then
		u_f(i,j,k+1)=(u(i+1,j,k+1)+u(i-1,j,k+1)+u(i,j+1,k+1)+u(i,j-1,k+1)+u(i,j,k))/5.d0
	    endif
      enddo
   enddo
enddo
return

end subroutine filter_field

!subroutine to calculate dynamic smagorinsky coefficient 

subroutine get_dynam_visc(viscosity,s11,s22,s33,s12,s13,s23,u1u1,u2u2,u3u3,u1u2,u1u3,u2u3,abs_s_s11, & 
	   abs_s_s22,abs_s_s33,abs_s_s12,abs_s_s13,abs_s_s23,abs_s,abs_s_u,u_c,v_c,w_c,ex,ey,sx,sy,n,dx,dy,dz,Re)		
implicit none

integer :: sx,ex,sy,ey,n,i,j,k
double precision :: dx,dy,dz

!declare fields(filtered when passed as arguments)
double precision :: s11(sx-1:ex+1,sy-1:ey+1,0:n)
double precision :: s22(sx-1:ex+1,sy-1:ey+1,0:n)
double precision :: s33(sx-1:ex+1,sy-1:ey+1,0:n)
double precision :: s12(sx-1:ex+1,sy-1:ey+1,0:n)
double precision :: s13(sx-1:ex+1,sy-1:ey+1,0:n)
double precision :: s23(sx-1:ex+1,sy-1:ey+1,0:n)

double precision :: u1u1(sx-1:ex+1,sy-1:ey+1,0:n)
double precision :: u2u2(sx-1:ex+1,sy-1:ey+1,0:n)
double precision :: u3u3(sx-1:ex+1,sy-1:ey+1,0:n)
double precision :: u1u2(sx-1:ex+1,sy-1:ey+1,0:n)
double precision :: u1u3(sx-1:ex+1,sy-1:ey+1,0:n)
double precision :: u2u3(sx-1:ex+1,sy-1:ey+1,0:n)

double precision :: abs_s_s11(sx-1:ex+1,sy-1:ey+1,0:n) 
double precision :: abs_s_s22(sx-1:ex+1,sy-1:ey+1,0:n)
double precision :: abs_s_s33(sx-1:ex+1,sy-1:ey+1,0:n)
double precision :: abs_s_s12(sx-1:ex+1,sy-1:ey+1,0:n)
double precision :: abs_s_s13(sx-1:ex+1,sy-1:ey+1,0:n)
double precision :: abs_s_s23(sx-1:ex+1,sy-1:ey+1,0:n)

double precision :: abs_s(sx-1:ex+1,sy-1:ey+1,0:n)

double precision :: u_c(sx-1:ex+1,sy-1:ey+1,0:n) 
double precision :: v_c(sx-1:ex+1,sy-1:ey+1,0:n)
double precision :: w_c(sx-1:ex+1,sy-1:ey+1,0:n)

!also require unfiltered abs_s
double precision :: abs_s_u(sx-1:ex+1,sy-1:ey+1,0:n)

double precision :: viscosity(sx-1:ex+1,sy-1:ey+1,0:n) 	

double precision :: C_smag, vis_les, Re
double precision :: sum_L_ij_M_ij, sum_M_ij_sq !it's not exactly these expressions i'm calcing, give/take factor of delta**2 

double precision :: L_11, L_22, L_33, L_12, L_13, L_23
double precision :: M_11, M_22, M_33, M_12, M_13, M_23
double precision :: delta_sq

delta_sq=4.d0*((dx*dy*dz)**(2.d0/3.d0))

 do k=1,n-1
       do j=sy,ey
            do i=sx,ex
		
		L_11=u1u1(i,j,k)-u_c(i,j,k)*u_c(i,j,k)
		L_22=u2u2(i,j,k)-v_c(i,j,k)*v_c(i,j,k)
		L_33=u3u3(i,j,k)-w_c(i,j,k)*w_c(i,j,k)
		L_12=u1u2(i,j,k)-u_c(i,j,k)*v_c(i,j,k)
		L_13=u1u3(i,j,k)-u_c(i,j,k)*w_c(i,j,k)
		L_23=u2u3(i,j,k)-v_c(i,j,k)*w_c(i,j,k)
		
		M_11=abs_s_s11(i,j,k)-4.d0*abs_s(i,j,k)*s11(i,j,k)
		M_22=abs_s_s22(i,j,k)-4.d0*abs_s(i,j,k)*s22(i,j,k)
		M_33=abs_s_s33(i,j,k)-4.d0*abs_s(i,j,k)*s33(i,j,k)
		M_12=abs_s_s12(i,j,k)-4.d0*abs_s(i,j,k)*s12(i,j,k)
		M_13=abs_s_s13(i,j,k)-4.d0*abs_s(i,j,k)*s13(i,j,k)
		M_23=abs_s_s23(i,j,k)-4.d0*abs_s(i,j,k)*s23(i,j,k)

		sum_L_ij_M_ij = L_11*M_11 + L_22*M_22 + L_33*M_33 + 2.d0*L_12*M_12 + 2.d0*L_13*M_13 + 2.d0*L_23*M_23

		sum_M_ij_sq = M_11*M_11 + M_22*M_22 + M_33*M_33 + 2.d0*M_12*M_12 + 2.d0*M_13*M_13 + 2.d0*M_23*M_23

		C_smag=0.5d0*(1/delta_sq)*((sum_L_ij_M_ij)/(sum_M_ij_sq))
		vis_les=(delta_sq)*((C_smag)**2.d0)*abs_s_u(i,j,k)
		viscosity(i,j,k)=(1.d0/Re)+vis_les	

		!identify k=1(n-1) with k=0(n)
		if(k.eq.1)then
		      viscosity(i,j,k-1)=viscosity(i,j,k)
		elseif(k.eq.n-1)then
                      viscosity(i,j,k+1)=viscosity(i,j,k)
		endif	    


		

            enddo
       enddo
 enddo

return
end subroutine get_dynam_visc

       subroutine get_viscosity_les(viscosity,u2,v2,w2,ex,ey,sx,sy,n,dx,dy,dz,Re)
       implicit none
       
       integer :: sx,ex,sy,ey,n,i,j,k
       double precision :: viscosity(sx-1:ex+1,sy-1:ey+1,0:n) 
       double precision :: u2(sx-2:ex+2,sy-2:ey+2,0:n-2) 
       double precision :: v2(sx-2:ex+2,sy-2:ey+2,0:n-2) 
       double precision :: w2(sx-2:ex+2,sy-2:ey+2,0:n-1)            
       double precision :: Re,z_val
       double precision :: dx,dy,dz
       double precision :: delta_cls,C_smag
       double precision :: u_i_minusz,u_im1_minusz,u_i_plusz,u_im1_plusz,v_j_minusz, &
		  v_jm1_minusz,v_j_plusz,v_jm1_plusz,u_star_plusy,u_star_minusy, &
		  u_star_plusz,u_star_minusz,v_star_plusx,v_star_minusx, &
		  v_star_plusz,v_star_minusz,w_star_plusx,w_star_minusx, &
		  w_star_plusy,w_star_minusy
       double precision :: du_dx,du_dy,du_dz,dv_dx,dv_dy,dv_dz,dw_dx,dw_dy,dw_dz
       double precision :: val1,val2,val3,val4,val5,val6
       double precision :: sij,sij_sq,phi_w,A,vis_les
                            
       ! Implementation of eddy viscosity for LES simulation.
       ! Define some constants
       delta_cls=2.d0*((dx*dy*dz)**(1d0/3d0))
       C_smag=0.1d0
       A=25.d0

       do k=1,n-1
         do j=sy,ey
           do i=sx,ex
		   ! Compute the various derivatives used in the rate of strain tensor.
	           ! Hence we must use the augmented velocity fields as inputs.
		   if(k.eq.1)then
		      u_i_minusz=-2.d0*u2(i,j-1,0)+u2(i,j-1,1)/3.d0
                      u_im1_minusz=-2.d0*u2(i-1,j-1,0)+u2(i-1,j-1,1)/3.d0
		      u_i_plusz=u2(i,j-1,1)
		      u_im1_plusz=u2(i-1,j-1,1)
		      v_j_minusz=-2.d0*v2(i-1,j,0)+v2(i-1,j,1)/3.d0
		      v_jm1_minusz=-2.d0*v2(i-1,j-1,0)+v2(i-1,j-1,1)/3.d0
		      v_j_plusz=v2(i-1,j,1)
		      v_jm1_plusz=v2(i-1,j-1,1)	
		   elseif(k.eq.n-1)then
		      u_i_minusz=u2(i,j-1,n-3)
		      u_im1_minusz=u2(i-1,j-1,n-3)
		      u_i_plusz=-2.d0*u2(i,j-1,n-2)+u2(i,j-1,n-2)/3.d0
		      u_im1_plusz=-2.d0*u2(i-1,j-1,n-2)+u2(i-1,j-1,n-3)/3.d0
		      v_j_minusz=v2(i-1,j,n-3)
		      v_jm1_minusz=v2(i-1,j-1,n-3)
		      v_j_plusz=-2.d0*v2(i-1,j,n-2)+v2(i-1,j,n-3)/3.d0
		      v_jm1_plusz=-2.d0*v2(i-1,j-1,n-2)+v2(i-1,j-1,n-3)/3.d0
		   else
                      u_i_minusz=u2(i,j-1,k-2)
		      u_im1_minusz=u2(i-1,j-1,k-2)
		      u_i_plusz=u2(i,j-1,k)
		      u_im1_plusz=u2(i-1,j-1,k)
		      v_j_minusz=v2(i-1,j,k-2)
		      v_jm1_minusz=v2(i-1,j-1,k-2)
		      v_j_plusz=v2(i-1,j,k)
		      v_jm1_plusz=v2(i-1,j-1,k)
		   end if
			
		   u_star_plusy=(0.5d0)*(u2(i,j,k-1)+u2(i-1,j,k-1))
	           u_star_minusy=(0.5d0)*(u2(i,j-2,k-1)+u2(i-1,j-2,k-1))
		   u_star_plusz=(0.5d0)*(u_i_plusz+u_im1_plusz)
		   u_star_minusz=(0.5d0)*(u_i_minusz+u_im1_minusz)
		   
		   v_star_plusx=(0.5d0)*(v2(i,j,k-1)+v2(i,j-1,k-1))
		   v_star_minusx=(0.5d0)*(v2(i-2,j,k-1)+v2(i-2,j-1,k-1))
		   v_star_plusz=(0.5d0)*(v_j_plusz+v_jm1_plusz)
		   v_star_minusz=(0.5d0)*(v_j_minusz+v_jm1_minusz)

		   ! Note that there are no problems in the z direction for
 		   ! the w grid. Thus we can compute the derivatives as follows.
		   w_star_plusx=(0.5d0)*(w2(i,j-1,k)+w2(i,j-1,k-1))
		   w_star_minusx=(0.5d0)*(w2(i-2,j-1,k)+w2(i-2,j-1,k-1))
		   w_star_plusy=(0.5d0)*(w2(i-1,j,k)+w2(i-1,j,k-1))
		   w_star_minusy=(0.5d0)*(w2(i-1,j-2,k)+w2(i-1,j-2,k-1))

		   du_dx=(u2(i,j-1,k-1)-u2(i-1,j-1,k-1))/dx
     		   du_dy=(u_star_plusy-u_star_minusy)/(2.d0*dy)
		   du_dz=(u_star_plusz-u_star_minusz)/(2.d0*dz)

		   dv_dx=(v_star_plusx-v_star_minusx)/(2.d0*dx)
		   dv_dy=(v2(i-1,j,k-1)-v2(i-1,j-1,k-1))/dy
		   dv_dz=(v_star_plusz-v_star_minusz)/(2.d0*dz)

		   dw_dx=(w_star_plusx-w_star_minusx)/(2.d0*dx)
		   dw_dy=(w_star_plusy-w_star_minusy)/(2.d0*dy)
                   dw_dz=(w2(i-1,j-1,k)-w2(i-1,j-1,k-1))/(dz)

		   ! Individual terms in rate of strain tensor.
		   val1=2.d0*((du_dx)**2)
		   val2=0.5d0*((du_dy)**2 + 2.d0*(du_dy)*(dv_dx) + (dv_dx)**2)
   		   val3=0.5d0*((du_dz)**2 + 2.d0*(du_dz)*(dw_dx) + (dw_dx)**2)
		   val4=2.d0*((dv_dy)**2)
		   val5=0.5d0*((dv_dz)**2 + 2.d0*(dv_dz)*(dw_dy) + (dw_dy)**2)
		   val6=2.d0*((dw_dz)**2)

		   sij_sq=(val1 + 2.d0*val2 + 2.d0*val3 + val4 + 2.d0*val5 + val6)
		   sij=sqrt(sij_sq)
  
		   ! Near wall modelling term (p and q set to 1 and omitted for simplicity).
		   z_val=k*dz-(dz/2.d0)

		   if (z_val.lt.0.5d0)then
			phi_w=1.d0-dexp((-z_val*(Re/A)))
		   else
		        phi_w=1.d0-dexp(-(1.d0-z_val)*(Re/A))
		   endif

		  ! Obtain the eddy viscosity
		  vis_les=((C_smag*delta_cls*phi_w)**2.d0)*sij
		  viscosity(i,j,k)=(1.d0/Re)+vis_les	

		  if(k.eq.1)then
		      viscosity(i,j,k-1)=viscosity(i,j,k)
		  elseif(k.eq.n-1)then
                      viscosity(i,j,k+1)=viscosity(i,j,k)
		  endif	   
	
           end do
         end do
       end do

       return

       end subroutine get_viscosity_les
